from threading import Thread, Timer
from time import sleep

from src import log
from src.hardware.Asm import ASM
from src.hardware.Hardware import HARDWARE
from src.so.Kernel import Kernel
from src.so.Program import Program

##
##  MAIN
##
if __name__ == '__main__':
    log.setupLogger()
    log.logger.info('Starting emulator')

    ## setup our hardware and set memory size to 20 "cells"
    HARDWARE.setup(20)

    ## new create the Operative System Kernel
    kernel = Kernel()

    ##  create a program
    prg = Program("test.exe", [ASM.CPU(2), ASM.IO(), ASM.CPU(3), ASM.IO(), ASM.CPU(3)])

    # execute the program
    threadDeCarga = Thread(target=kernel.execute, args=(prg,))

    ## start
    threadDeFuncionamiento = Timer(1, HARDWARE.switchOn)

    threadDeFuncionamiento.start()
    threadDeCarga.start()

    ###################
    prg1 = Program("prg1.exe", [ASM.CPU(2), ASM.IO(), ASM.CPU(3)])
    prg2 = Program("prg2.exe", [ASM.CPU(4), ASM.IO(), ASM.CPU(1)])
    prg3 = Program("prg3.exe", [ASM.CPU(3)])

    batch   = [prg1, prg2, prg3]

    # execute the program
    threadDeCarga2 = Thread(target=kernel.executeBatch, args=(batch,))

    ## start
    threadDeFuncionamiento2 = Timer(1, HARDWARE.switchOn)

    sleep(12) #puesto para que no se pise con los thread anteriores
    threadDeFuncionamiento2.start()
    threadDeCarga2.start()
