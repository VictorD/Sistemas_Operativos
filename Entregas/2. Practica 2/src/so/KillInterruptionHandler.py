from src.log import logger
from src.so.AbstractInterruptionHandler import AbstractInterruptionHandler

class KillInterruptionHandler(AbstractInterruptionHandler):

    def execute(self, irq):
        logger.info(" Program Finished ")
        self.kernel.loadNextProgram()
