from src import log
from src.hardware.Hardware import *
from src.hardware.Irq import KILL_INTERRUPTION_TYPE
from src.so.KillInterruptionHandler import KillInterruptionHandler

# emulates the core of an Operative System
class Kernel():

    def __init__(self):
        ## setup interruption handlers
        killHandler = KillInterruptionHandler(self)
        HARDWARE.interruptVector.register(KILL_INTERRUPTION_TYPE, killHandler)
        self._programList = []

    def load_program(self, program):
        # loads the program in main memory
        progSize = len(program.instructions)
        for index in range(0, progSize):
            inst = program.instructions[index]
            HARDWARE.memory.put(index, inst)

    ## emulates a "system call" for programs execution
    def execute(self, program):
        self.load_program(program)
        log.logger.info("\n Executing program: {name}".format(name=program.name))
        log.logger.info(HARDWARE)

        # set CPU program counter at program's first intruction
        HARDWARE.cpu.pc = 0

    # Executes all programs in a batch in secuential order.
    def executeBatch(self, batch):
        self._programList = batch
        self.execute(self._programList.pop())

    def __repr__(self):
        return "Kernel "

    def loadNextProgram(self):
        HARDWARE.cpu.pc = -1
        if self._programList:
            self.execute(self._programList.pop())
        else:
            HARDWARE.switchOff()

