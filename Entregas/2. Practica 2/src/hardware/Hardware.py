from src.hardware.Clock import Clock
from src.hardware.InterruptVector import InterruptVector
from src.hardware.Memory import Memory
from src.hardware.Cpu import Cpu
from src.log import logger

## emulates the Hardware that were the Operative System run
class Hardware():

    ## Setup our hardware
    def setup(self, memorySize):
        ## add the components to the "motherboard"
        self._interruptVector = InterruptVector()
        self._memory = Memory(memorySize)
        self._clock = Clock()

        ## "wire" the components each others
        self._cpu = Cpu(self._memory, self._interruptVector)
        self._clock.addSubscriber(self._cpu)

    @property
    def cpu(self):
        return self._cpu

    @property
    def clock(self):
        return self._clock

    @property
    def interruptVector(self):
        return self._interruptVector

    @property
    def memory(self):
        return self._memory

    def __repr__(self):
        return "HARDWARE state {cpu}\n{mem}".format(cpu=self._cpu, mem=self._memory)

    def switchOn(self):
        logger.info(" ---- SWITCH ON ---- ")
        self.clock.start()

    def switchOff(self):
        self.clock.stop()
        logger.info(" ---- SWITCH OFF ---- ")

### HARDWARE is a global variable
### can be access from any
HARDWARE = Hardware()
