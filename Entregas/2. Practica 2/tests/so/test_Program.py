from unittest import TestCase
from src.so.Program import Program
from src.hardware.Asm import *

class TestProgram(TestCase):

    def setUp(self):
        self.prg = Program("test.exe", [ASM.CPU(2), ASM.IO(), ASM.CPU(3), ASM.IO(), ASM.CPU(3)])


    def test_when_A_Program_Is_created_There_is_only_one_EXIT_instruction_and_Its_The_last(self):
        EvaluationForExistence = self.hasOnly1Exit    (self.prg)
        EvaluationForLast      = self.exitIsTheLastOne(self.prg)

        self.assertTrue(EvaluationForExistence)
        self.assertTrue(EvaluationForLast)

    def hasOnly1Exit(self, prg):
        prgInstructions =  prg.instructions
        return  any(instruction == INSTRUCTION_EXIT for instruction in prgInstructions)

    def exitIsTheLastOne(self, prg):
        prgInstructions = prg.instructions
        last = prgInstructions[-1]
        return  ASM.isEXIT(last)

    def tearDown(self):
        pass
