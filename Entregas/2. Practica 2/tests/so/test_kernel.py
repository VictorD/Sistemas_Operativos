from unittest import TestCase
from unittest.mock import patch
from src.hardware.Asm import ASM
from src.hardware.Hardware import HARDWARE
from src.so.Kernel import Kernel
from src.so.Program import Program
import src.log #No Borrar, si se quita rompe el @Patch

class TestKernel(TestCase):

    def setUp(self):
        self.prg    = Program("test.exe", [ASM.CPU(1)])
        self.prg2   = Program("test.exe", [ASM.CPU(1), ASM.IO()])
        HARDWARE.setup(20)
        self.kernel = Kernel()

    @patch('src.log.logger.info')
    def test_Execute_An_Program_With_Two_Instructions_Without_SwitchOn_Only_Load_In_Memory(self, mock_info):

        #excerice
        self.kernel.execute(self.prg)

        #assert
        self.assertEqual(self.prg.instructions[0], HARDWARE.memory.get(0))
        self.assertEqual(self.prg.instructions[1], HARDWARE.memory.get(1))
        self.assertEqual("", HARDWARE.memory.get(2))
        self.assertEqual(2, mock_info.call_count)

    @patch('src.log.logger.info')
    def test_Execute_An_Program_With_Two_Instructions(self, mock_info):

        #excerice
        self.kernel.execute(self.prg)
        HARDWARE.switchOn()

        #assert
        self.assertEqual(10, mock_info.call_count)


    @patch('src.log.logger.info')
    def test_ExecuteBatch_With_A_Batch_With_Two_Programs(self,mock_info):
        # setUp
        batch   = [self.prg,self.prg2]

        # excerice
        self.kernel.executeBatch(batch)
        HARDWARE.switchOn()

        # assert
        self.assertEqual(19, mock_info.call_count)

    def tearDown(self):
        pass