from unittest import TestCase
from unittest.mock import MagicMock
from src.hardware.Cpu import Cpu
from src.hardware.InterruptVector import InterruptVector
from src.hardware.Memory import Memory
from src.log import *
from src.so.Program import Program

class TestCpu(TestCase):

    def setUp(self):
        memory = Memory(20)
        prg = Program("test.exe", ["INSTRUCCION MOCK"])
        memory.put(2, prg.instructions[0])

        self.cpuSUT = Cpu(memory, InterruptVector())

    def test_when_A_Cpu_Is_Created_Its_PC_Is_Minus_One_and_Doesnt_has_an_ir_asigned(self):
        pc = self.cpuSUT.pc
        ir = self.cpuSUT._ir

        # check that the values are the expected
        self.assertEqual(pc, -1)
        self.assertIsNone(ir)

    def test_should_a_tick_be_called_when_the_PC_is_pointing_to_an_instruction_Then_The_IR_Should_be_the_Fetched_instruction_and_The_PC_should_be_pointing_to_the_next(self):

        # set the PC to the desired Instruction
        self.cpuSUT.pc = 2

        pc = self.cpuSUT.pc
        ir = self.cpuSUT._ir

        # check that the values are as expected before the tick
        self.assertEqual (pc, 2)
        self.assertIsNone(ir)

        # execute the tick

        self.cpuSUT.tick(1)

        # check that the values are the expected

        pc = self.cpuSUT.pc
        ir = self.cpuSUT._ir

        self.assertEqual(pc, 3)
        self.assertEqual(ir, "INSTRUCCION MOCK")

    def test_should_a_tick_be_called_when_the_PC_is_Idle_the_logger_is_called_one_time(self):
        logger.info = MagicMock()
        self.cpuSUT.tick(1)
        self.assertEqual(1, logger.info.call_count)


    def tearDown(self):
        pass