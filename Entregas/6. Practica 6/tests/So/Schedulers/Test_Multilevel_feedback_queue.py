from unittest import TestCase

from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.Hardware.Irq import TIMEOUT_INTERRUPTION_TYPE
from src.Hardware.Timer.Timer import Timer
from src.So.ContextSwitcher.ContextSwitch import ContextSwitch
from src.So.InterruptionHandlers.TimeOutMFQInterruptionHandler import TimeOutMFQInterruptionHandler
from src.So.Kernel import Kernel
from src.So.Program import Program
from src.So.Scheduler.SchedulerMultilevelFeedBackQueue import SchedulerMultilevelFeedBackQueue
from src.So.Switch.MultilevelFeedbackQueue.SwitchInMultilevelFeedbackQueue import SwitchInMultilevelFeedbackQueue
from src.So.Switch.WithTimer.SwitchOutWithTimer import SwitchOutWithTimer


class TestMultilevelFeedbackQueue(TestCase):

    def setUp(self):
        HARDWARE.setup(1000)

        self.kernel = Kernel()
        HARDWARE.timer = Timer(HARDWARE.cpu)
        HARDWARE.clock._subscribers[0] = HARDWARE.timer
        self.kernel.scheduler = SchedulerMultilevelFeedBackQueue()
        timeOutHandler = TimeOutMFQInterruptionHandler(self.kernel)
        HARDWARE.interruptVector.register(TIMEOUT_INTERRUPTION_TYPE, timeOutHandler)
        ContextSwitch().setSwitchOut(SwitchOutWithTimer())
        ContextSwitch().setSwitchIn(SwitchInMultilevelFeedbackQueue())

        self.program = Program("It's a Virus!.exe", [ASM.CPU(100)])
        HARDWARE.harddisk.newDirectory("C:/virus")
        HARDWARE.harddisk.saveProgram(self.program, "C:/virus")

        self.program1 = Program("Really? another virus!.exe", [ASM.CPU(100)])
        HARDWARE.harddisk.newDirectory("C:/virus/fake")
        HARDWARE.harddisk.saveProgram(self.program1, "C:/virus/fake")

        self.program2 = Program("Flash.exe", [ASM.CPU(100)])
        HARDWARE.harddisk.newDirectory("C:/bios")
        HARDWARE.harddisk.saveProgram(self.program2, "C:/bios")

        self.program3 = Program("troyanWar.exe", [ASM.CPU(7)])
        HARDWARE.harddisk.newDirectory("C:/system/private/risk")
        HARDWARE.harddisk.saveProgram(self.program3, "C:/system/private/risk")

        self.program4 = Program("troyanWar2.exe", [ASM.CPU(7)])
        HARDWARE.harddisk.newDirectory("C:/system/private/risk2")
        HARDWARE.harddisk.saveProgram(self.program4, "C:/system/private/risk2")

        self.program5 = Program("troyanWar3.exe", [ASM.CPU(7)])
        HARDWARE.harddisk.newDirectory("C:/system/private/risk3")
        HARDWARE.harddisk.saveProgram(self.program5, "C:/system/private/risk3")



    def test_When_a_process_starts_executing_then_it_first_enters_queue_one(self):
        # Setup
        self.kernel.execute("C:/virus/It's a Virus!.exe")
        self.kernel.execute("C:/virus/fake/Really? another virus!.exe")

        ## the first one goes to cpu directly. The second one goes to the ready queue
        HARDWARE.clock.do_ticks(2)

        # We check that the ready queue is as expected, with program on the readyQueue
        # And program1 currently running
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue1())
        self.assertEqual(self.kernel.scheduler.readyQueue1[0].direction, "C:/virus/fake/Really? another virus!.exe")
        self.assertEqual(self.kernel.pcbTable.pcbRunning.direction, "C:/virus/It's a Virus!.exe")


    def test_should_a_process_in_the_first_queue_not_finish_or_go_to_IO_after_a_round_robin_cicle_it_goes_to_the_second_queue(self):
        # Setup
        self.kernel.execute("C:/virus/It's a Virus!.exe")
        self.kernel.execute("C:/virus/fake/Really? another virus!.exe")

        # we set up a program  it does 9 ticks which is how long the round robin should wait before the time out
        # once it returns, it should go to the second Queue.
        HARDWARE.clock.do_ticks(9)

        # We check that the ready queue is as expected, with program1 on the readyQueue2
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue1())
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue2())
        self.assertEqual(self.kernel.scheduler.readyQueue2[0].direction, "C:/virus/It's a Virus!.exe")
        self.assertEqual(self.kernel.pcbTable.pcbRunning.direction, "C:/virus/fake/Really? another virus!.exe")

    def test_should_the_scheduler_be_asked_For_the_next_process_it_gets_its_next_from_the_first_queue(self):
        # Setup
        self.kernel.execute("C:/virus/It's a Virus!.exe")
        self.kernel.execute("C:/virus/fake/Really? another virus!.exe")

        # we set up a program  it does 9 ticks which is how long the round robin should wait before the time out
        # the first program goes to the second queue, and when the scheduler gets the next program, it picks the one that is left in the queue one
        HARDWARE.clock.do_ticks(9)

        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue1())
        self.assertEqual(self.kernel.scheduler.readyQueue2[0].direction, "C:/virus/It's a Virus!.exe")
        self.assertEqual(self.kernel.pcbTable.pcbRunning.direction, "C:/virus/fake/Really? another virus!.exe")


    def test_should_the_scheduler_be_asked_For_the_next_process_and_there_are_none_in_the_first_queue_it_gets_the_next_one_From_the_second_queue(self):
        # Setup
        self.kernel.execute("C:/virus/It's a Virus!.exe")
        self.kernel.execute("C:/virus/fake/Really? another virus!.exe")

        # we set up a program  it does 9 ticks which is how long the round robin should wait before the time out
        # the first program goes to the second queue, and when the scheduler gets the next program, it picks the one that was left in the second queue
        HARDWARE.clock.do_ticks(9)
        self.assertTrue (self.kernel.scheduler.isEmptyReadyQueue1())
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue2())
        self.assertEqual(self.kernel.scheduler.readyQueue2[0].direction, "C:/virus/It's a Virus!.exe")

        HARDWARE.clock.do_ticks(9)

        # We do another tick and check that the currently running pcb is the one expected
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue1())
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue2())
        self.assertEqual(self.kernel.pcbTable.pcbRunning.direction, "C:/virus/It's a Virus!.exe")
        self.assertEqual(self.kernel.scheduler.readyQueue2[0].direction, "C:/virus/fake/Really? another virus!.exe")


    def test_should_a_process_in_the_second_queue_not_finish_or_go_to_IO_after_a_round_robin_cicle_it_goes_to_the_third_queue(self):
        # Setup
        self.kernel.execute("C:/bios/Flash.exe")
        self.kernel.execute("C:/virus/fake/Really? another virus!.exe")

        # we set up a program  it does 9 ticks which is how long the round robin should wait before the time out
        # the first program goes to the second queue, and when the scheduler gets the next program, it picks the one that was left in the second queue
        HARDWARE.clock.do_ticks(9)
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue1())
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue2())
        self.assertEqual(self.kernel.scheduler.readyQueue2[0].direction, "C:/bios/Flash.exe")

        # we set up a program  it does 17 ticks which is how long the round robin should wait before the time out
        # the first program goes to the third queue, and when the scheduler gets the next program, it picks the one that was left in the third queue
        HARDWARE.clock.do_ticks(9)
        self.assertEqual(self.kernel.pcbTable.pcbRunning.direction, "C:/bios/Flash.exe")
        self.assertEqual(self.kernel.scheduler.readyQueue2[0].direction, "C:/virus/fake/Really? another virus!.exe")

        HARDWARE.clock.do_ticks(17)
        self.assertEqual(self.kernel.pcbTable.pcbRunning.direction, "C:/virus/fake/Really? another virus!.exe")

        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue1())
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue2())
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue3())
        self.assertEqual(self.kernel.scheduler.readyQueue3[0].direction, "C:/bios/Flash.exe")


    def test_should_the_scheduler_be_asked_For_the_next_process_and_there_are_none_in_the_first_queue_or_the_Second_queue_it_gets_the_next_one_From_the_third_queue(self):

        # Setup
        self.kernel.execute("C:/bios/Flash.exe")
        self.kernel.execute("C:/virus/fake/Really? another virus!.exe")

        # we set up a program  it does 9 ticks which is how long the round robin should wait before the time out
        # the first program goes to the second queue, and when the scheduler gets the next program, it picks the one that was left in the second queue
        HARDWARE.clock.do_ticks(9)
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue1())
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue2())
        self.assertEqual(self.kernel.scheduler.readyQueue2[0].direction, "C:/bios/Flash.exe")

        # we set up a program  it does 16 ticks which is how long the round robin should wait before the time out
        # the first program goes to the third queue, and when the scheduler gets the next program, it picks the one that was left in the third queue
        HARDWARE.clock.do_ticks(9)
        HARDWARE.clock.do_ticks(17)
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue1())
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue2())
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue3())


        HARDWARE.clock.do_ticks(17)

        self.assertEqual(self.kernel.scheduler.readyQueue3[0].direction, "C:/virus/fake/Really? another virus!.exe")
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue1())
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue2())
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue3())
        self.assertEqual(self.kernel.pcbTable.pcbRunning.direction, "C:/bios/Flash.exe")

        ## As the third queue runs until a new pcb comes, it will continue running without worrying about a timeout
        HARDWARE.clock.do_ticks(20)

        self.assertEqual(self.kernel.pcbTable.pcbRunning.direction, "C:/bios/Flash.exe")



    def test_after_the_scheduler_asks_for_the_next_process_and_gets_one_from_the_first_queue_equal_to_its_aging_factor_the_first_process_in_the_queue_two_and_queue_three_promotes_to_queue_one(self):
        # Setup
        self.kernel.scheduler._agingFactor= 4

        # Setup
        self.kernel.execute("C:/bios/Flash.exe")

        # we set up a program  it does 24 ticks which is how many ticks it needs to go to the third row
        HARDWARE.clock.do_ticks(9)
        self.kernel.execute("C:/virus/It's a Virus!.exe")
        self.assertEqual(1, HARDWARE.timer._timeOutCounter)
        self.assertEqual(16, HARDWARE.timer.quantum)
        HARDWARE.clock.do_ticks(16)


        self.assertTrue  (self.kernel.scheduler.isEmptyReadyQueue1())
        self.assertTrue  (self.kernel.scheduler.isEmptyReadyQueue2())
        self.assertFalse (self.kernel.scheduler.isEmptyReadyQueue3())
        self.assertEqual(self.kernel.scheduler._ageCounter, 1)

        self.assertEqual(self.kernel.pcbTable.pcbRunning.direction, "C:/virus/It's a Virus!.exe")
        self.assertEqual(self.kernel.scheduler.readyQueue3[0].direction, "C:/bios/Flash.exe")
        self.assertEqual(HARDWARE.timer._timeOutCounter, 1 )


        #we add another program, and make it do 8 ticks so it goes to the second row

        self.kernel.execute("C:/system/private/risk/troyanWar.exe")
        HARDWARE.clock.do_ticks(8)

        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue1())
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue2())
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue3())

        self.assertEqual(self.kernel.scheduler.readyQueue2[0].direction, "C:/virus/It's a Virus!.exe")
        self.assertEqual(self.kernel.scheduler.readyQueue3[0].direction, "C:/bios/Flash.exe")


        self.assertEqual(self.kernel.scheduler._ageCounter, 2)
        self.assertEqual(self.kernel.pcbTable.pcbRunning.direction, "C:/system/private/risk/troyanWar.exe")
        self.assertEqual(HARDWARE.timer._timeOutCounter, 1)


        # # we add another 2 programs, and make it do 8 ticks so both of them finish and the agefactor is meet, promoting the programs in queue 2 and 3 to one and resetting the age counter.
        self.kernel.execute("C:/system/private/risk2/troyanWar2.exe")
        self.kernel.execute("C:/system/private/risk3/troyanWar3.exe")

        self.assertEqual(self.kernel.scheduler._ageCounter, 2)
        self.assertEqual(HARDWARE.timer._timeOutCounter, 1)
        self.assertEqual(HARDWARE.cpu.pc, 1)

        # se hace 7 ticks por que los troyan wars solo tienen 8 instrucciones y ya corrio 1
        HARDWARE.clock.do_ticks(7)

        self.assertEqual(self.kernel.scheduler._ageCounter, 3)
        self.assertEqual(self.kernel.pcbTable.pcbRunning.direction, "C:/system/private/risk2/troyanWar2.exe")
        self.assertEqual(HARDWARE.timer._timeOutCounter, 0)
        self.assertEqual(HARDWARE.cpu.pc, 0)

        # se hace 8 ticks por que los troyan wars solo tienen 8 instrucciones
        HARDWARE.clock.do_ticks(8)

        self.assertEqual(self.kernel.scheduler._ageCounter, 0)

        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue1())
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue2())
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue3())
        self.assertEqual(len(self.kernel.scheduler.readyQueue1),2)
        self.assertEqual(self.kernel.scheduler.readyQueue1[0].direction,"C:/virus/It's a Virus!.exe")
        self.assertEqual(self.kernel.scheduler.readyQueue1[1].direction, "C:/bios/Flash.exe")


    def test_should_a_process_be_handled_to_the_scheduler_when_its_Running_a_process_from_the_third_queue_then_it_changes_to_that_one(self):
        # Setup
        self.kernel.execute("C:/bios/Flash.exe")
        self.kernel.execute("C:/virus/fake/Really? another virus!.exe")

        # we set up a program  it does 9 ticks which is how long the round robin should wait before the time out
        # the first program goes to the second queue, and when the scheduler gets the next program, it picks the one that was left in the second queue
        HARDWARE.clock.do_ticks(9)
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue1())
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue2())
        self.assertEqual(self.kernel.scheduler.readyQueue2[0].direction, "C:/bios/Flash.exe")

        # we set up a program  it does 16 ticks which is how long the round robin should wait before the time out
        # the first program goes to the third queue, and when the scheduler gets the next program, it picks the one that was left in the third queue
        HARDWARE.clock.do_ticks(9)
        HARDWARE.clock.do_ticks(17)
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue1())
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue2())
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue3())

        HARDWARE.clock.do_ticks(17)

        self.assertEqual(self.kernel.scheduler.readyQueue3[0].direction, "C:/virus/fake/Really? another virus!.exe")
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue1())
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue2())
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue3())
        self.assertEqual(self.kernel.pcbTable.pcbRunning.direction, "C:/bios/Flash.exe")

        ## As the third queue runs until a new pcb comes, it will continue running without worrying about a timeout
        HARDWARE.clock.do_ticks(10)

        self.assertEqual(self.kernel.pcbTable.pcbRunning.direction, "C:/bios/Flash.exe")

        self.kernel.execute("C:/system/private/risk/troyanWar.exe")
        HARDWARE.clock.do_ticks(1)
        self.assertEqual(self.kernel.pcbTable.pcbRunning.direction, "C:/system/private/risk/troyanWar.exe")
        self.assertEqual(self.kernel.scheduler.readyQueue1[0].direction, "C:/bios/Flash.exe")


