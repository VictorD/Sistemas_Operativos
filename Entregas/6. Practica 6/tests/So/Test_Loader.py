
from unittest import TestCase
from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.So.Pcb.Pcb import PCB
from src.So.Program import Program
from src.So.Loader.Loader import Loader

class TestLoader(TestCase):

    def setUp(self):
        HARDWARE.setup(40)
        self.loaderSUT      = Loader()
        self.programTest1   = Program("Test.dog", [ASM.CPU(2), ASM.IO()])
        HARDWARE.harddisk.newDirectory("C:/perrugrama")
        HARDWARE.harddisk.saveProgram(self.programTest1, "C:/perrugrama")
        self.pcbProgram1 = PCB(0, "C:/perrugrama/Test.dog")
        self.programTest2   = Program("It's not a Virus.dog", [ASM.CPU(7)])
        HARDWARE.harddisk.newDirectory("C:/It's Dog Time!")
        HARDWARE.harddisk.saveProgram(self.programTest2, "C:/It's Dog Time!")
        self.pcbProgram2 = PCB(1, "C:/It's Dog Time!/It's not a Virus.dog")

    def test_the_first_load_should_address_to_the_dir_zero(self):
        # Setup
        addresBeforLoad = self.loaderSUT.addressToAssign

        # Exercise
        self.loaderSUT.loadProgram(self.pcbProgram1)

        # Test
        self.assertEqual(0, addresBeforLoad)
        self.assertEqual(4, self.loaderSUT.addressToAssign)

    def test_after_the_first_load_the_address_should_point_to_the_next_empty_cell(self):
        # Setup Part 1
        addresBeforLoad = self.loaderSUT.addressToAssign

        # Exercise Part 1
        self.loaderSUT.loadProgram(self.pcbProgram1)

        # Test Part 1
        self.assertEqual(0, addresBeforLoad)
        self.assertEqual(['CPU', 'CPU', 'IO', 'EXIT'], HARDWARE.memory._cells[0:4])
        self.assertEqual(4, self.loaderSUT.addressToAssign)

        # Setup Part 2
        addresBeforLoad = self.loaderSUT.addressToAssign

        # Exercise Part 2
        self.loaderSUT.loadProgram(self.pcbProgram2)

        # Test Part 2
        self.assertEqual(4, addresBeforLoad)
        self.assertEqual(['CPU', 'CPU', 'CPU', 'CPU', 'CPU', 'CPU', 'CPU', 'EXIT'], HARDWARE.memory._cells[4:12])
        self.assertEqual(12, self.loaderSUT.addressToAssign)

    def tearDown(self):
        HARDWARE.memory._cells.clear()
        self.loaderSUT._addressToAssign = 0
