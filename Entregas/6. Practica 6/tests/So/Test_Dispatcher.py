from unittest import TestCase

from src.Hardware.Hardware import HARDWARE
from src.So.Dispatcher.Dispatcher import Dispatcher
from src.So.Pcb.Pcb import PCB


class TestDispatcher(TestCase):

    def setUp(self):
        HARDWARE.setup(60)
        HARDWARE.cpu.pc             = 5
        HARDWARE.mmu.limit          = 10
        HARDWARE.mmu.baseDir        = 3
        self.dispatcherSUT          = Dispatcher()
        self.pcbToSave              = PCB(1, "C:/perrtanic/dogioh.dog")
        self.pcbToSave.baseAddress  = 3
        self.pcbToSave.size         = 7

        self.pcbToLoad              = PCB(2, "C:/to.dog")
        self.pcbToLoad.baseAddress  = 1
        self.pcbToLoad.size         = 8
        self.pcbToLoad.pc           = 1

    def test_When_the_state_Of_a_running_PCB_is_saved_the_cpu_goes_idle(self):
        #Exersice
        self.dispatcherSUT.save(self.pcbToSave)

        #Test
        self.assertEqual(5, self.pcbToSave.pc)
        self.assertEqual(3, self.pcbToSave.baseAddress)
        self.assertEqual(7, self.pcbToSave.size)
        self.assertEqual(-1, HARDWARE.cpu.pc)

    def test_when_a_pcb_is_loaded_the_state_of_PCB_The_CPU_and_MMU_Have_new_Values(self):
        # Exersice
        self.dispatcherSUT.load(self.pcbToLoad)

        # Test
        self.assertEqual(1, HARDWARE.cpu.pc)
        self.assertEqual(1, HARDWARE.mmu.baseDir)
        self.assertEqual(8, HARDWARE.mmu.limit)