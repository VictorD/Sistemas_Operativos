from unittest import TestCase
from src.So.MemoryTable import MemoryTable
from src.So.Page import Page
from src.So.PageTable import PageTable
from src.So.victimStrategy.LRUStrategy import LRUStrategy


class TestLRUStrategy(TestCase):

    def setUp(self):
        self.LRUStrategySut = LRUStrategy()
        self.memoryTable    = MemoryTable()
        self.pageTablePid0  = self.setupPageTableOfPid0()
        self.pageTablePid1  = self.setupPageTableOfPid1()
        self.pageTablePid2  = self.setupPageTableOfPid2()
        self.pageTablePid3  = self.setupPageTableOfPid3()
        self.memoryTable.loadPCBAndPageTable(0, self.pageTablePid0)
        self.memoryTable.loadPCBAndPageTable(1, self.pageTablePid1)
        self.memoryTable.loadPCBAndPageTable(2, self.pageTablePid2)

    def test_getVictim(self):
        # Setup
        # Exercise
        victimLRU = self.LRUStrategySut.getVictim(self.memoryTable)
        # Test
        self.assertEqual(0, victimLRU[0])
        self.assertEqual(0, victimLRU[1]._frame)

    def test_getVictim2(self):
        # Setup
        self.memoryTable.loadPCBAndPageTable(3, self.pageTablePid3)
        # Exercise
        victimLRU = self.LRUStrategySut.getVictim(self.memoryTable)
        # Test
        self.assertEqual(3, victimLRU[0])
        self.assertEqual(12, victimLRU[1]._frame)

    ##[-----Setup Methods-----]##
    def setupPageTableOfPid2(self):
        pageTablePid2 = PageTable()
        page1Pid2 = Page()
        page1Pid2._number = 0
        pageTablePid2.assocPages([page1Pid2])
        return pageTablePid2

    def setupPageTableOfPid1(self):
        pageTablePid1       = PageTable()
        page1Pid1           = Page()
        page1Pid1._number   = 0
        page2Pid1           = Page()
        page2Pid1._number   = 1
        pageTablePid1.assocPages([page1Pid1, page2Pid1])
        pageTablePid1.assocFrames([2])
        pageTablePid1.setDirty(0, 1)
        pageTablePid1.setEntryTimeStamp(0, 14)
        return pageTablePid1

    def setupPageTableOfPid0(self):
        pageTablePid0       = PageTable()
        page1Pid0          = Page()
        page1Pid0._number  = 0
        page2Pid0          = Page()
        page2Pid0._number  = 1
        page3Pid0          = Page()
        page3Pid0._number  = 2
        page4Pid0          = Page()
        page4Pid0._number  = 3
        pageTablePid0.assocPages([page1Pid0, page2Pid0, page3Pid0, page4Pid0])
        pageTablePid0.assocFrames([4, 0])
        pageTablePid0.setDirty(0, 5)
        pageTablePid0.setDirty(1, 9)
        pageTablePid0.setEntryTimeStamp(0, 15)
        pageTablePid0.setEntryTimeStamp(1, 10)
        return pageTablePid0

    def setupPageTableOfPid3(self):
        pageTablePid3 = PageTable()
        page1Pid3 = Page()
        page1Pid3._number = 0
        pageTablePid3.assocPages([page1Pid3])
        pageTablePid3.assocFrames([12])
        pageTablePid3.setDirty(0, 0)
        pageTablePid3.setEntryTimeStamp(0, 1)
        return pageTablePid3