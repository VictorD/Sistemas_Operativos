from unittest import TestCase
from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.Hardware.Irq import OUT_OF_MEMORY_INTERRUPTION_TYPE
from src.OutOfMemoryException import OutOfMemoryException
from src.So.InterruptionHandlers.OutOfMemoryInterruptionHandler import OutOfMemoryInterruptionHandler
from src.So.Kernel import Kernel
from src.So.Loader.LoaderPaging import LoaderPaging
from src.So.MemoryManager.MemoryManagerPaging import MemoryManagerPaging
from src.So.Pcb.Pcb import PCB
from src.So.Program import Program


class TestLoaderPaging(TestCase):

    def setUp(self):
        HARDWARE.setup(40)
        self.memoryManager = MemoryManagerPaging(4,40)
        self.loaderSUT      = LoaderPaging(self.memoryManager)
        self.programTest1   = Program("Test.dog", [ASM.CPU(12)])
        HARDWARE.harddisk.newDirectory("C:/perrugrama")
        HARDWARE.harddisk.saveProgram(self.programTest1, "C:/perrugrama")
        self.pcbProgram1 = PCB(0, "C:/perrugrama/Test.dog")

        self.programTest2   = Program("It's not a Virus.dog", [ASM.CPU(7)])
        HARDWARE.harddisk.newDirectory("C:/It's Dog Time!")
        HARDWARE.harddisk.saveProgram(self.programTest2, "C:/It's Dog Time!")
        self.pcbProgram2 = PCB(1, "C:/It's Dog Time!/It's not a Virus.dog")

        self.programTest3 = Program("loader.dog", [ASM.CPU(5)])
        HARDWARE.harddisk.newDirectory("C:/MemoryManger")
        HARDWARE.harddisk.saveProgram(self.programTest3, "C:/MemoryManger")
        self.pcbProgram3 = PCB(2, "C:/MemoryManger/loader.dog")

    def test_when_a_program_with_4_pages_is_loaded_then_the_loader_asks_the_memmory_manager_for_4_free_frames_creates_a_page_table_for_these_pages_and_saves_them_in_the_memory_manager(self):
        # Setup
        # Exercise
        pageTableOfPidZero = self.loaderSUT.createPageTable(len(self.programTest1.instructions), self.pcbProgram1.pid)
        # Test
        self.assertEqual(24, self.loaderSUT._memoryManager._freeMemory)
        self.assertEqual(pageTableOfPidZero, self.loaderSUT._memoryManager.getPageTableOf(0))
        self.assertEqual(6, len(self.loaderSUT._memoryManager._freeFrames))
        self.assertListEqual([4, 5, 6, 7, 8, 9], self.loaderSUT._memoryManager._freeFrames)
        self.assertEqual(4, len(pageTableOfPidZero._table))
        self.assertEqual(0, pageTableOfPidZero.frameOf(0))
        self.assertEqual(1, pageTableOfPidZero.frameOf(1))
        self.assertEqual(2, pageTableOfPidZero.frameOf(2))
        self.assertEqual(3, pageTableOfPidZero.frameOf(3))

    def test_when_the_loader_is_asked_to_load_a_page_for_a_program_in_a_given_frame_it_fetchs_from_the_program_those_instructions_indexed_in_the_page_And_Writtes_them_on_the_base_direcction_of_the_Frame(self):
        # Setup
        # Exercise
        self.loaderSUT.write(self.programTest1.instructions[0:4], 1)
        # Test
        self.assertListEqual(['', '', '', ''], HARDWARE.memory._cells[0:4])
        self.assertListEqual(['CPU', 'CPU', 'CPU', 'CPU'], HARDWARE.memory._cells[4:8])

    def test_when_a_program_with_6_instructions_loads_2_pages_of_frameSize_4_these_instructions_are_loaded_in_the_frame(self):
        # Setup
        pageTableOfPidTwo = self.loaderSUT.createPageTable(len(self.programTest3.instructions), self.pcbProgram3.pid)
        # Exercise
        self.loaderSUT.load(self.programTest3.instructions, pageTableOfPidTwo)
        # Test
        self.assertListEqual(['CPU', 'CPU', 'CPU', 'CPU', 'CPU', 'EXIT', '', ''], HARDWARE.memory._cells[0:8])

    def test_when_two_pages_Are_loaded_in_memory_then_their_pages_are_fully_load_in_the_Corresponding_Frames_which_where_Given_to_those_pages(self):
        # Setup
        self.memoryManager._freeFrames  = [4, 3]
        self.memoryManager._freeMemory  = 8

        # Exercise
        self.loaderSUT.loadProgram(self.pcbProgram2)

        # Test
        pageTableOfPidOne = self.loaderSUT._memoryManager.getPageTableOf(1)
        self.assertEqual(0, self.loaderSUT._memoryManager._freeMemory)
        self.assertListEqual([], self.loaderSUT._memoryManager._freeFrames)
        self.assertEqual(2, len(pageTableOfPidOne._table))
        self.assertEqual(4, pageTableOfPidOne.frameOf(0))
        self.assertEqual(3, pageTableOfPidOne.frameOf(1))
        self.assertListEqual(['', '', '', '','', '', '', '','', '', '', ''], HARDWARE.memory._cells[0:12])
        self.assertListEqual(['CPU', 'CPU', 'CPU', 'CPU'], HARDWARE.memory._cells[16:20])
        self.assertListEqual(['CPU', 'CPU', 'CPU', 'EXIT'], HARDWARE.memory._cells[12:16])

    def test_when_the_loader_is_asked_to_load_a_program_and_the_memory_Manager_dont_have_free_frames_raise_an_exeption(self):
        # Setup
        outOfMemoryHandler = OutOfMemoryInterruptionHandler(Kernel())
        HARDWARE.interruptVector.register(OUT_OF_MEMORY_INTERRUPTION_TYPE, outOfMemoryHandler)
        self.memoryManager._freeFrames = []
        self.memoryManager._freeMemory = 0

        # Exercise
        self.assertRaises(OutOfMemoryException, self.loaderSUT.loadProgram, self.pcbProgram2)
