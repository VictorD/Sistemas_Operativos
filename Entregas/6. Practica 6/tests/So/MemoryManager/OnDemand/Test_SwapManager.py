from unittest import TestCase

from src.Hardware.Hardware import HARDWARE
from src.Hardware.Memory import Memory
from src.So.MemoryManager.Swap.SwapManager import SwapManager


class TestSwapManager(TestCase):

    def setUp(self):
        HARDWARE.setup(50)
        HARDWARE.harddisk._swap = Memory(12)
        self.aPageNumber = 3
        self.aPid = 2
        self.someInstructions = ['CPU', 'CPU', 'CPU', 'CPU']
        self.swapManager = SwapManager(4, 12)

    def test_Given_a_PageNumber_a_pid_and_Some_instructions_The_swapManager_asigns_the_page_To_a_free_swapFrame_and_loads_the_instructions_on_the_disk(
            self):
        self.assertFalse(self.swapManager.isInSwap(self.aPageNumber, self.aPid))
        self.assertNotEqual(self.someInstructions, HARDWARE.harddisk._swap._cells[0:4])

        self.swapManager.swapIN(self.aPageNumber, self.aPid, self.someInstructions)

        self.assertTrue(self.swapManager.isInSwap(self.aPageNumber, self.aPid))
        self.assertEqual(self.someInstructions, HARDWARE.harddisk._swap._cells[0:4])

    def test_Given_a_PageNumber_and_a_pid_The_swapManager_takes_the_page_of_its_swapTable_Frees_a_swapFrame_and_retrieves_the_instructions_of_the_page_from_the_disk(
            self):
        self.swapManager.swapIN(self.aPageNumber, self.aPid, self.someInstructions)
        instructionsToReturn = self.swapManager.swapOUT(self.aPageNumber, self.aPid)

        self.assertFalse(self.swapManager.isInSwap(self.aPageNumber, self.aPid))
        self.assertEqual(self.someInstructions, instructionsToReturn)
        self.assertEqual(len(self.swapManager._freeSwapFrames), 3)
