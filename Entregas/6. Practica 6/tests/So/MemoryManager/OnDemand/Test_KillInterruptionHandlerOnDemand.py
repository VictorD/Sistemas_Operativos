from unittest import TestCase

from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.So.InterruptionHandlers.KillInterruptionHandlerOnDemand import KillInterruptionHandlerOnDemand
from src.So.Kernel import Kernel
from src.So.Loader.LoaderOnDemand import LoaderOnDemand
from src.So.MemoryManager.MemoryManagerOnDemand import MemoryManagerOnDemand
from src.So.Pcb.PcbState import PcbState
from src.So.Program import Program


class TestKillInterruptionHandlerOnDemand(TestCase):

    def setUp(self):
        HARDWARE.setup(50)
        self.kernel                 = Kernel()
        self.killSut                = KillInterruptionHandlerOnDemand(self.kernel)
        self.kernel._memoryManager  = MemoryManagerOnDemand(5, 50)
        self.kernel._loader         = LoaderOnDemand(self.kernel._memoryManager)
        self.program1               = Program("Really? another virus! Remastered.exe", [ASM.CPU(5)])
        HARDWARE.harddisk.newDirectory("C:/fake")
        HARDWARE.harddisk.saveProgram(self.program1, "C:/fake")

    def test_when_a_process_terminates_Their_page_table_reset_the_frames_and_swaps_flags(self):
        # Setup
        self.kernel.execute("C:/fake/Really? another virus! Remastered.exe")
        pageTableOfPid  = self.kernel._memoryManager.getPageTableOf(0)
        pageTableOfPid.setFrameToPage(0,1)
        swapManager     = self.kernel._memoryManager._swapManager
        swapManager.swapIN(1,0, ['CPU','CPU','CPU','CPU','CPU'])
        pageTableOfPid.setFrameInSwap(1)

        # Exercise
        self.killSut.execute([])

        # Test
        self.assertFalse(pageTableOfPid.hasFrame(0))
        self.assertFalse(pageTableOfPid.isInSwap(1))
        self.assertDictEqual({},swapManager._pageSwapTables._swaptable)