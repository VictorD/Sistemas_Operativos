from unittest import TestCase
from src.So.Page import Page
from src.So.PageTable import PageTable


class TestPageFrameTable(TestCase):

    def setUp(self):
        self.pfTableSut = PageTable()
        self.page1 = Page()
        self.page1._number = 0
        self.page2 = Page()
        self.page2._number = 1
        self.page3 = Page()
        self.page3._number = 2
        self.page4 = Page()
        self.page4._number = 3

    def test_when_4associate_4_new_Pages_those_pages_dont_have_frame_assigned(self):
        # Setup
        # Exercise
        self.pfTableSut.assocPages([self.page1, self.page2, self.page3, self.page4])

        # Test
        self.assertIsNone(self.pfTableSut.frameOf(0))
        self.assertIsNone(self.pfTableSut.frameOf(1))
        self.assertIsNone(self.pfTableSut.frameOf(2))
        self.assertIsNone(self.pfTableSut.frameOf(3))

    def test_when_4_Frames_are_associated_in_a_table_of_4_pages_and_those_pages_dont_have_assigned_frames_these_are_associated(self):
        # Setup
        self.pfTableSut.assocPages([self.page1, self.page2, self.page3, self.page4])

        # Exercise
        self.pfTableSut.assocFrames([4, 0, 2, 3])

        # Test
        self.assertEqual(4, self.pfTableSut.frameOf(0))
        self.assertEqual(0, self.pfTableSut.frameOf(1))
        self.assertEqual(2, self.pfTableSut.frameOf(2))
        self.assertEqual(3, self.pfTableSut.frameOf(3))

    def test_when_2_Frames_are_associated_in_a_table_of_4_pages_and_those_pages_dont_have_assigned_frames_two_of_these_are_associated(
            self):
        # Setup
        self.pfTableSut.assocPages([self.page1, self.page2, self.page3, self.page4])

        # Exercise
        self.pfTableSut.setFrameToPage(0, 4)
        self.pfTableSut.setFrameToPage(1, 0)

        # Test
        self.assertEqual(4, self.pfTableSut.frameOf(0))
        self.assertEqual(0, self.pfTableSut.frameOf(1))
        self.assertIsNone(self.pfTableSut.frameOf(2))
        self.assertIsNone(self.pfTableSut.frameOf(3))

    def test_when_clear_the_table_all_pages_dont_have_assigned_frames(self):
        # Setup
        self.pfTableSut.assocPages([self.page1, self.page2, self.page3, self.page4])
        self.pfTableSut.assocFrames([4, 0])

        # Exercise
        self.pfTableSut.clearFrames()

        # Test
        self.assertIsNone(self.pfTableSut.frameOf(0))
        self.assertIsNone(self.pfTableSut.frameOf(1))
        self.assertIsNone(self.pfTableSut.frameOf(2))
        self.assertIsNone(self.pfTableSut.frameOf(3))

    def test_when_ask_for_all_frames_and_some_Pages_dont_have_frames_only_return_the_assignded_frames(self):
        # Setup
        self.pfTableSut.assocPages([self.page1, self.page2, self.page3, self.page4])
        self.pfTableSut.assocFrames([4, 0])

        # Exercise
        frames  = self.pfTableSut.getAllFrames()

        # Test
        self.assertEqual(2, len(frames))
        self.assertListEqual([4, 0], frames)