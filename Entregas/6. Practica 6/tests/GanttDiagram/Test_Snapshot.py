from unittest import TestCase
from src.GanttDiagram.Snapshot import Snapshot
from src.So.Pcb.Pcb import PCB
from src.So.Pcb.PcbState import PcbState
from src.So.Pcb.PcbTable import PcbTable


class TestSystemSnapshot(TestCase):

    def setUp(self):
        self.aPCBTable  = PcbTable()
        pcb1            = PCB(0, "lanada misma")
        pcb1.running()
        pcb2            = PCB(1, "lanada misma reforged")
        pcb2.ready()
        pcb3            = PCB(2, "collie")
        pcb3.ready()
        self.aPCBTable._pcbTable.setdefault(0, pcb1)
        self.aPCBTable._pcbTable.setdefault(1, pcb2)
        self.aPCBTable._pcbTable.setdefault(2, pcb3)

    def test_when_a_system_Snapshot_has_made_its_done_correctly(self):
        # Setup

        # Exercise
        snapshot    = Snapshot.takeSnapshot(0, self.aPCBTable)

        # Test
        self.assertEqual(3, len(snapshot._pcbList))
        self.assertEqual(0, snapshot._pcbList[0]._pid)
        self.assertEqual(PcbState.RUNNING, snapshot._pcbList[0]._state)
        self.assertEqual(1, snapshot._pcbList[1]._pid)
        self.assertEqual(PcbState.READY, snapshot._pcbList[1]._state)
        self.assertEqual(2, snapshot._pcbList[2]._pid)
        self.assertEqual(PcbState.READY, snapshot._pcbList[2]._state)

    def test_if_a_quantityOfReadyState_is_done_and_there_are_two_ready_state_return_two(self):
        # Setup
        snapshot    = Snapshot.takeSnapshot(0, self.aPCBTable)
        # Exercise
        result      = snapshot.quantityOfReadyState()

        # Test
        self.assertEqual(2, result)