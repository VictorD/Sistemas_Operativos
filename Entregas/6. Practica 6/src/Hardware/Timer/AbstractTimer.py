from abc import abstractmethod


class AbstractTimer():

    @abstractmethod
    def tick(self, tickNbr):
        pass

    @abstractmethod
    def resetTimeOutCounterIfNeed(self):
        pass

    @abstractmethod
    def setOff(self):
        pass

    @abstractmethod
    def setOn(self):
        pass