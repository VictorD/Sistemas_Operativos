from src.Hardware.HardDisk.Directory import Directory
from src.Hardware.Memory import Memory


class HardDisk():

    def __init__(self):
            self._root = Directory("C:/")
            self._swap = Memory(200)

    @property
    def root(self):
        return self._root

    def readProgram(self, aDirection):
        split = aDirection.split("/")
        return self.root.fetchProgram(split)

    def readInstructionsOfProgram(self,aDirection,init,end):
        split = aDirection.split("/")
        program = self.root.fetchProgram(split)
        return program.data[init:end]

    def saveProgram(self,aProgram,aDirection):
        split = aDirection.split("/")
        self.root.saveProgram(aProgram,split)

    def newDirectory(self,aDirection):
        split = aDirection.split("/")
        self.root.newRute(split)