from src import log
from src.Hardware.HardDisk.HardDisk import HardDisk
from src.Hardware.MMU.MMU import MMU
from src.Hardware.Timer.AbstractTimer import AbstractTimer
from src.Hardware.Clock import Clock
from src.Hardware.Cpu import Cpu

from src.Hardware.InterruptVector import InterruptVector
from src.Hardware.Irq import IRQ, IO_OUT_INTERRUPTION_TYPE
from src.Hardware.Memory import Memory


## emulates the Hardware that were the Operative System run
class Hardware():

    ## Setup our hardware
    def setup(self, memorySize):
        ## add the components to the "motherboard"
        self._harddisk          = HardDisk()
        self._memory            = Memory(memorySize)
        self._interruptVector   = InterruptVector()
        self._clock             = Clock()
        self._ioDevice          = PrinterIODevice()
        self._mmu               = MMU(self._memory)
        self._cpu               = Cpu(self._mmu, self._interruptVector)
        self._timer             = AbstractTimer()
        self._clock.addSubscriber(self._timer)
        self._clock.addSubscriber(self._ioDevice)
        self._clock.addSubscriber(self._cpu)


    def switchOn(self):
        log.logger.info(" ---- SWITCH ON ---- ")
        return self.clock.start()

    def switchOff(self):
        self.clock.stop()
        HARDWARE.cpu.pc = -1
        log.logger.info(" ---- SWITCH OFF ---- ")

    @property
    def cpu(self):
        return self._cpu

    @property
    def harddisk(self):
        return self._harddisk

    @property
    def clock(self):
        return self._clock

    @property
    def interruptVector(self):
        return self._interruptVector

    @property
    def memory(self):
        return self._memory

    @property
    def mmu(self):
        return self._mmu

    @property
    def ioDevice(self):
        return self._ioDevice

    @property
    def timer(self):
        return self._timer

    @timer.setter
    def timer(self, aTimer):
        self._timer = aTimer

    def __repr__(self):
        return "HARDWARE state {cpu}\n{mem}".format(cpu=self._cpu, mem=self._memory)

### HARDWARE is a global variable
### can be access from any
HARDWARE = Hardware()

## emulates an Input/output device of the Hardware

class AbstractIODevice():

    def __init__(self, deviceId, deviceTime):
        self._deviceId = deviceId
        self._deviceTime = deviceTime
        self._busy = False

    @property
    def deviceId(self):
        return self._deviceId

    @property
    def is_busy(self):
        return self._busy

    @property
    def is_idle(self):
        return not self._busy

    ## executes an I/O instruction
    def execute(self, operation):
        if (self._busy):
            raise Exception("Device {id} is busy, can't  execute operation: {op}".format(id = self.deviceId, op = operation))
        else:
            self._busy = True
            self._ticksCount = 0
            self._operation = operation

    def tick(self, tickNbr):
        if (self._busy):
            self._ticksCount += 1
            if (self._ticksCount > self._deviceTime):
                ## operation execution has finished
                self._busy = False
                ioOutIRQ = IRQ(IO_OUT_INTERRUPTION_TYPE, self._deviceId)
                HARDWARE.interruptVector.handle(ioOutIRQ)
            else:
                log.logger.info("device {deviceId} - Busy: {ticksCount} of {deviceTime}".format(deviceId = self.deviceId, ticksCount = self._ticksCount, deviceTime = self._deviceTime))

    def shuttingDownPC(self):
        self._operation = None
        self._busy = False


## Emulates a Printer
class PrinterIODevice(AbstractIODevice):
    def __init__(self):
        super(PrinterIODevice, self).__init__("Printer", 3)

