from enum import Enum

class PCBLevelPriority(Enum):

    HIGH    = 1
    LOW     = 0

    def __repr__(self):
        return f'{self.name} LEVEL PRIORITY'

    def __lt__(self, other):
        return self.value < other.value

    def __le__(self, other):
        return self.value <= other.value

    def __gt__(self, other):
        return self.value > other.value

    def __ge__(self, other):
        return self.value >= other.value
