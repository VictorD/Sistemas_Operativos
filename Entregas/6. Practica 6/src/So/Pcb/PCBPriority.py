from enum import Enum

from src.So.Pcb.PCBLevelPriority import PCBLevelPriority


class PCBPriority(Enum):

    LOWEST  = 5
    LOW     = 4
    MEDIUM  = 3
    HIGH    = 2
    HIGHEST = 1

    def priorityLevel(self):
        if (self.value == 1) or (self.value == 2):
            return PCBLevelPriority.HIGH
        else:
            return PCBLevelPriority.LOW

    def __repr__(self):
        return f'{self.name} PRIORITY'

    def __lt__(self, other):
        return self.value > other.value

    def __le__(self, other):
        return self.value >= other.value

    def __gt__(self, other):
        return self.value < other.value

    def __ge__(self, other):
        return self.value <= other.value