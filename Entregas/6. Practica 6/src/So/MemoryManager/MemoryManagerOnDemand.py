from src.Hardware.Hardware import HARDWARE
from src.So.MemoryManager.Swap.SwapManager import SwapManager
from src.So.MemoryTable import MemoryTable
from src.So.victimStrategy.FIFOStrategy import FIFOStrategy

##Se separo en dos Memory Manager para evitar que se haga comportamiento innecesario
class MemoryManagerOnDemand():

    def __init__(self, frameSize, memorySize):
        self._frameSize                 = frameSize
        self._freeFrames                = []
        self._pageTables                = MemoryTable()
        self._freeMemory                = memorySize
        self._selectionOfVictimStrategy = FIFOStrategy()
        self.setupFrames(memorySize)
        self._swapManager               = SwapManager(frameSize, memorySize)

    def setupFrames(self, memorySize):
        self._freeFrames.extend(list(range(0, int(memorySize / self._frameSize))))

    def savePCBAndPageTable(self, aPid, pcbPagTable):
        self._pageTables.loadPCBAndPageTable(aPid, pcbPagTable)

    def getPageTableOf(self, aPidPCB):
        return self._pageTables.pageTableOf(aPidPCB)

    def releaseFrames(self, aPid):
        """Libera los frames asociados a las paginas del proceso con el id aPid, actualiza la tabla de dicho proceso limpiandola de frames y pone los frames liberados como frames disponibles"""
        framesFreed         = self._pageTables.releaseAndReturnsFrames(aPid)
        self._freeMemory   += len(framesFreed) * self._frameSize
        self._freeFrames.extend(framesFreed)

    def hasMemoryFor(self, quantityOfInstructions):
        return self._freeMemory >= quantityOfInstructions

    def hasFreeFrame(self):
        return self._freeFrames != []

    def getVictim(self):
        victim = self._selectionOfVictimStrategy.getVictim(self._pageTables)
        return victim

    def getFreeFrame(self):
        frameToUse = None
        if self.hasFreeFrame():
          frameToUse         = self._freeFrames.pop(0)
          self._freeMemory  -= self._frameSize
        else:
          tupleVictimPidPtRow   = self.getVictim()                                      # Obtiene la victima
          victimPid             = tupleVictimPidPtRow[0]
          victimPage            = tupleVictimPidPtRow[1]._page
          frameToUse            = tupleVictimPidPtRow[1]._frame
          instructionsToSwap    = self.getInstructionsOfFrame(frameToUse)               # Trae las Instrucciones de Memoria
          self._swapManager.swapIN(victimPage._number, victimPid, instructionsToSwap)   # Lleva la victima a Swap
          self._pageTables.releaseFrameOf(victimPid, victimPage._number)                # Libera los frame de la tabla de la victima
          self._pageTables.setSwapFrame(victimPid, victimPage._number)                  # Setea que la pagina esta en swap
        return frameToUse

    def getInstructionsOfFrame(self, aFrame):
        firstInstruction= aFrame * self._frameSize
        endInstruction  = firstInstruction + self._frameSize
        instructions    = HARDWARE.memory._cells[firstInstruction:endInstruction]
        return instructions

    def setStrategyVictim(self, aStrategyVictim):
        self._selectionOfVictimStrategy = aStrategyVictim

    def releaseSwapFrames(self, aPid):
        """Actualiza la tabla de dicho proceso limpiandola de swapFrames"""
        self._pageTables.releaseSwapFrames(aPid)    # Limpia los flags de swap de la tabla
        self._swapManager.releaseSwapFrames(aPid)   # Libera los swapframes.