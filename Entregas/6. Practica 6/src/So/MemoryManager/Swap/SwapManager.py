from src.OutOfMemoryException import OutOfMemoryException
from src.So.Loader.LoaderSwap import LoaderSwap
from src.So.MemoryManager.Swap.SwapTable import SwapTable


class SwapManager():

    def __init__(self, frameSize, memorySize):
        self._frameSize = frameSize
        self._freeSwapFrames = []
        self._pageSwapTables = SwapTable()
        self.setupswapFrames(memorySize)

    def setupswapFrames(self, memorySize):
        self._freeSwapFrames.extend(list(range(0, int(memorySize / self._frameSize))))

    def swapIN(self, aPageNumber, aPid, someInstructions):
        swapFrame = self.getFreeFrame()
        self._pageSwapTables.swapIn(aPageNumber,aPid,swapFrame)
        LoaderSwap().writeInSwap(someInstructions, swapFrame, self._frameSize)

    def swapOUT(self, aPageNumber, aPid):
        frame           = self._pageSwapTables.swapOut(aPageNumber, aPid)
        someInstructions= LoaderSwap().readFromSwap(frame, self._frameSize)
        self.freeFrame(frame)
        return someInstructions

    def isInSwap(self, aPageNumber, aPid):
        return self._pageSwapTables.isInSwap(aPid,aPageNumber)

    def getFreeFrame(self):
        if self._freeSwapFrames == []:
            raise OutOfMemoryException("Memoria y Swap Saturados! No Hay Mas Espacio!")

        return self._freeSwapFrames.pop(0)

    def freeFrame(self, aFrame):
        self._freeSwapFrames.append(aFrame)

    def releaseSwapFrames(self, aPid):
        if self._pageSwapTables.hasTable(aPid):
            self._freeSwapFrames.extend(self._pageSwapTables.deleteSwapFramesOf(aPid))