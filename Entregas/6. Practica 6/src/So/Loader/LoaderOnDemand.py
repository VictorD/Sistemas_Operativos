from src.Hardware.Hardware import HARDWARE
from src.So.Loader.AbstractLoaderPaging import AbstractLoaderPaging


class LoaderOnDemand(AbstractLoaderPaging):

    def __init__(self, memoryManager):
        super().__init__(memoryManager)

    def loadProgram(self, aPCB):
        aProgram = HARDWARE.harddisk.readProgram(aPCB.direction)
        sizeOfProgram = len(aProgram.data)
        self.createPageTable(sizeOfProgram, aPCB.pid)
        aPCB.size = sizeOfProgram


    def loadPage(self, aPCB, aPageNumber, frame):
        instructionsToLoad = []
        pageTableOfPCB     = self._memoryManager.getPageTableOf(aPCB.pid)   # Me traigo la tabla del pcb para actualizar los datos

        # Pregunto si la pagina fue a swap
        if pageTableOfPCB.isInSwap(aPageNumber):
              instructionsToLoad  = self._memoryManager._swapManager.swapOUT(aPageNumber, aPCB.pid) # Me traigo las instrucciones de Swap
              pageTableOfPCB.clearSwapOfPage(aPageNumber)                                           # Limpio la tabla de los datos Swap
        else:
              aPageToLoad         = pageTableOfPCB._table[aPageNumber]._page
              initInstruction     = aPageToLoad.firstInstruction()    # Calcula la posicion de la primera instrucion
              endInstruction      = aPageToLoad.endInstruction()      # Calcula la posicion de la ultima instrucion

              # Trae las instrucciones del disco
              instructionsToLoad  = HARDWARE.harddisk.readInstructionsOfProgram(aPCB._direction, initInstruction, endInstruction)

        pageTableOfPCB.setFrameToPage(aPageNumber, frame)   # Asocio el frame a la pagina
        pageTableOfPCB.setEntryTimeStamp(aPageNumber, HARDWARE.clock._tickNbr)
        self.write(instructionsToLoad, frame)               # Escribo en el frame
