from src.Hardware.Hardware import HARDWARE

# Have the responsibility of load the program on the memory
class Loader():

    def __init__(self):
        self._addressToAssign   = 0

    def loadProgram(self, aPCB):

        aProgram            = HARDWARE.harddisk.readProgram(aPCB.direction)
        sizeOfProgram       = len(aProgram.data)
        initAddressMemory   = self._addressToAssign
        aPCB.baseAddress    = initAddressMemory
        aPCB.size           = sizeOfProgram
        endAddressMemory    = initAddressMemory + sizeOfProgram

        # loads the program in main memory
        for index in range(initAddressMemory, endAddressMemory):
            inst = aProgram.data[index - initAddressMemory]
            HARDWARE.memory.put(index, inst)

        self._addressToAssign = endAddressMemory

    @property
    def addressToAssign(self):
        return self._addressToAssign
