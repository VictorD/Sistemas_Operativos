from src.Hardware.Hardware import HARDWARE
from src.So.PageCreator import PageCreator
from src.So.PageTable import PageTable


class AbstractLoaderPaging():

    def __init__(self, memoryManager):
        self._memoryManager = memoryManager

    # Crea las paginas necesarias para alojar las instruccioens en memoria
    def paginatePcb(self, sizeOfProgram):
        return PageCreator.createPages(sizeOfProgram, self._memoryManager._frameSize)

    # Crea la tabla de paginas para el proceso aPcbPid y lo carga a la memory manager
    def createPageTable(self, sizeOfProgram, aPcbPid):
        pagesCreated    = self.paginatePcb(sizeOfProgram)
        paginationTable = PageTable.createWith(pagesCreated)
        self._memoryManager.savePCBAndPageTable(aPcbPid, paginationTable)
        return paginationTable

    # Dado un Pcb carga el programa en el sistema operativo.
    def loadProgram(self, aPCB):
        raise NotImplementedError("Must be Overriden in the Class")

    def write(self, instructions, aFrame):
        addresToWrite = aFrame * self._memoryManager._frameSize  # Se Calcula la posicion de memoria inicial

        for index in range(0, len(instructions)):
            HARDWARE.memory.put(addresToWrite, instructions[index])
            addresToWrite += 1
