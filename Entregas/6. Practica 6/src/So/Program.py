from src.Hardware.Asm import ASM, INSTRUCTION_EXIT


## emulates a compiled program
class Program():

    def __init__(self, name, instructions):
        self._name = name
        self._instructions = self.expand(instructions)

    def addInstr(self, instruction):
        self.instructions.append(instruction)

    def expand(self, instructions):
        expanded = []
        for i in instructions:
            if isinstance(i, list):
                ## is a list of instructions
                expanded.extend(i)
            else:
                ## a single instr (a String)
                expanded.append(i)

        ## now test if last instruction is EXIT
        ## if not... add an EXIT as final instruction
        last = expanded[-1]
        if not ASM.isEXIT(last):
            expanded.append(INSTRUCTION_EXIT)

        return expanded

    # ----- Getters & Setters ----- #
    @property
    def name(self):
        return self._name

    @property
    def instructions(self):
        return self._instructions

    def __repr__(self):
        return "Program({name}, {instructions})".format(name=self.name, instructions=self.instructions)
