from src.So.InterruptionHandlers.KillInterruptionHandlerPaging import KillInterruptionHandlerPaging


class KillInterruptionHandlerOnDemand(KillInterruptionHandlerPaging):

    def execute(self, irq):
        pidOfRunningPcb = self.kernel.pcbTable.pcbRunning.pid

        self.kernel._memoryManager.releaseSwapFrames(pidOfRunningPcb)   # Limpio La tabla swap del Pcb de swapframes
                                                                        # asociados y los pongo como libres.
        # TODO: ¿Limpiar los timestamp tambien?, el super ya limpia los frames. Habria que modificar el metodo nada mas.
        super().execute(irq)