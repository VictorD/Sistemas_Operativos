from src.So.Scheduler.AbstractScheduler import AbstractScheduler
from src.So.Scheduler.SchedulerFIFO import SchedulerFIFO
from src.So.Scheduler.SchedulerRoundRobin import SchedulerRoundRobin


class SchedulerMultilevelQueue(AbstractScheduler):


    def __init__(self, aQuantum):
        self._readyQueue    = [SchedulerRoundRobin(aQuantum), SchedulerFIFO()]

    def add(self, aPcb):
        if aPcb.hasHighLevelPriority():
            self._readyQueue[0].add(aPcb)
        else:
            self._readyQueue[1].add(aPcb)

    def getNext(self):
        if not self.isEmptyForegroundQueue():
            return self._readyQueue[0].getNext()
        else:
            return self._readyQueue[1].getNext()

    def cleanAll(self):
        self._readyQueue[0].cleanAll()
        self._readyQueue[1].cleanAll()

    def isEmptyReadyQueue(self):
        return self.isEmptyForegroundQueue() and self.isEmptyBackgroundQueue()

    def isEmptyForegroundQueue(self):
        return self.foregroundQueue == []

    def isEmptyBackgroundQueue(self):
        return self.backgroundQueue == []

    # ----- Getters & Setters ----- #
    @property
    def foregroundQueue(self):
        return self._readyQueue[0].readyQueue

    @property
    def backgroundQueue(self):
        return self._readyQueue[1].readyQueue

    @property
    def readyQueue(self):
        return [self.foregroundQueue, self.backgroundQueue]