from src.Hardware.Hardware import HARDWARE
from src.So.Scheduler.AbstractScheduler import AbstractScheduler


class SchedulerMultilevelFeedBackQueue(AbstractScheduler):

    def __init__(self):
        super().__init__()
        HARDWARE.timer.quantum = 8
        self.setupQueue()
        self._agingFactor = 10
        self._ageCounter = 0
        self._currentlyRunningQueue = 1
        self.setCurrentlyRunning1()


    def add(self, aPcb):
        self.readyQueue1.append(aPcb)

    def addAndDemote(self, aPcb):
        if self._currentlyRunningQueue == 1:
            self.readyQueue2.append(aPcb)
        else:
            self.readyQueue3.append(aPcb)

    def getNext(self):
        if not self.isEmptyReadyQueue1():
            return self.getNextFromReady1()
        elif not self.isEmptyReadyQueue2():
            return self.getNextFromReady2()
        else:
            return self.getNextFromReady3()


    def cleanAll(self):
        self.setupQueue()

    def isEmptyReadyQueue(self):
        return self.isEmptyReadyQueue1() and self.isEmptyReadyQueue2() and self.isEmptyReadyQueue3()

    @property
    def readyQueue(self):
        return [self.readyQueue1, self.readyQueue2, self.readyQueue3]

    @property
    def readyQueue1(self):
        return self._readyQueue1

    @property
    def readyQueue2(self):
        return self._readyQueue2

    @property
    def readyQueue3(self):
        return self._readyQueue3

    @property
    def ageCounter(self):
        return self._ageCounter

    @property
    def currentlyRunningQueue(self):
        return self._currentlyRunningQueue

    def age(self):
        self._ageCounter = 0
        if not self.isEmptyReadyQueue2():
            self.readyQueue1.append(self.readyQueue2.pop(0))
        if not self.isEmptyReadyQueue3():
            self.readyQueue1.append(self.readyQueue3.pop(0))

    def setupQueue(self):
        self._readyQueue1 = []
        self._readyQueue2 = []
        self._readyQueue3 = []

    def isEmptyReadyQueue1(self):
        return self.readyQueue1 == []

    def isEmptyReadyQueue2(self):
        return self.readyQueue2 == []

    def isEmptyReadyQueue3(self):
        return self.readyQueue3 == []


    def setCurrentlyRunning1(self):
        HARDWARE.timer.resetTimeOutCounterAndPutOnIfNeed()
        HARDWARE.timer.quantum = 8


    def getNextFromReady1(self):
        self.setCurrentlyRunning1()
        self._currentlyRunningQueue = 1
        self._ageCounter = self.ageCounter+1
        pcbtoReturn = self.readyQueue1.pop(0)

        if self.ageCounter == self._agingFactor:
            self.age()

        return pcbtoReturn


    def getNextFromReady2(self):
        HARDWARE.timer.resetTimeOutCounterAndPutOnIfNeed()
        HARDWARE.timer.quantum = 16
        self._currentlyRunningQueue = 2
        return self.readyQueue2.pop(0)

    def getNextFromReady3(self):
        HARDWARE.timer.putOffIfNeed()
        self._currentlyRunningQueue = 3
        return self.readyQueue3.pop(0)