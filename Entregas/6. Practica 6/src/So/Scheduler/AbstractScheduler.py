from abc import abstractmethod


class AbstractScheduler():

    _readyQueue = None

    @abstractmethod
    def add(self, aPcb):
        raise NotImplementedError

    @abstractmethod
    def getNext(self):
        raise NotImplementedError

    @abstractmethod
    def cleanAll(self):
        raise NotImplementedError

    @abstractmethod
    def isEmptyReadyQueue(self):
        raise NotImplementedError

    def isExpropiative(self):
        return False

    @property
    def readyQueue(self):
        return self._readyQueue
