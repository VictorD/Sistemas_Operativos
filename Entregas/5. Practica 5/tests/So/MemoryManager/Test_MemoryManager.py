from unittest import TestCase
from src.Hardware.Hardware import HARDWARE
from src.So.MemoryManager.MemoryManager import MemoryManager
from src.So.Page import Page
from src.So.PageTable import PageTable


class TestMemoryManager(TestCase):

    def setUp(self):
        HARDWARE.setup(50)
        self.mermoryManagerSut  = MemoryManager(5, 50)
        self.page1              = Page.newPage(5, 0)
        self.page2              = Page.newPage(5, 1)


    def test_a_Memory_manager_with_a_Empty_Memory_of_size_50_and_a_frame_Size_of_5_has_10_frames_all_empty(self):
        # Test
        self.assertEqual(5, self.mermoryManagerSut._frameSize)
        self.assertEqual(50, self.mermoryManagerSut._freeMemory)
        self.assertEqual(10, len(self.mermoryManagerSut._freeFrames))
        self.assertListEqual(list(range(0, 10)), self.mermoryManagerSut._freeFrames)
        self.assertTrue(self.mermoryManagerSut._pageTables._table == {})


    def test_when_the_memory_manager_is_asked_for_a_frame_it_returns_the_first_one_From_its_empty_Frames_list_and_is_no_longer_in_the_free_list (self):
        # Setup
        # Exercise
        frame       = self.mermoryManagerSut.getFreeFrames(1)
        # Test
        self.assertListEqual([0], frame)
        self.assertEqual(45, self.mermoryManagerSut._freeMemory)
        self.assertFalse(0 in self.mermoryManagerSut._freeFrames)


    def test_when_the_memory_manager_is_asked_for_the_page_table_of_a_pid_it_returns_The_corresponding_page_table(self):
        # Setup
        pageTablePid0   = PageTable()
        pageTablePid0.setPage(self.page1)
        pageTablePid0.setPage(self.page2)
        pageTablePid0.assocFrames([3,4])
        self.mermoryManagerSut.savePCBAndPageTable(0, pageTablePid0)
        # Test
        self.assertEqual(pageTablePid0, self.mermoryManagerSut.getPageTableOf(0))

    def test_when_the_memory_manager_is_given_a_pid_and_Asked_to_release_the_page_table_the_frames_are_Free_Again(self):
        # Setup
        pageTablePid0   = PageTable()
        pageTablePid0.setPage(self.page1)
        pageTablePid0.setPage(self.page2)
        pageTablePid0.assocFrames([3,4])
        self.mermoryManagerSut.savePCBAndPageTable(0, pageTablePid0)
        self.mermoryManagerSut._freeFrames.remove(3)
        self.mermoryManagerSut._freeFrames.remove(4)
        self.mermoryManagerSut._freeMemory  = 40
        # Exercise
        self.mermoryManagerSut.releaseFrames(0)
        # Test
        self.assertEqual(50, self.mermoryManagerSut._freeMemory)
        self.assertEqual(10, len(self.mermoryManagerSut._freeFrames))
        self.assertTrue(3 in self.mermoryManagerSut._freeFrames)
        self.assertTrue(4 in self.mermoryManagerSut._freeFrames)
        self.assertIsNone(pageTablePid0.frameOf(0))
        self.assertIsNone(pageTablePid0.frameOf(1))

