from unittest import TestCase

from src.So.PageCreator import PageCreator


class TestPageCreator(TestCase):

    def test_If_create_pages_of_size_4_with_a_list_of_9_instructions_got_a_list_of_3_pages(self):
        # Setup
        # Exercise
        pages   = PageCreator.createPages(10, 4)
        # Test
        self.assertEqual(3, len(pages))
        self.assertEqual(0, pages[0]._number)
        self.assertEqual(4, pages[0]._size)
        self.assertEqual(1, pages[1]._number)
        self.assertEqual(4, pages[1]._size)
        self.assertEqual(2, pages[2]._number)
        self.assertEqual(4, pages[2]._size)

    def test_If_create_pages_of_size_4_with_a_list_of_8_instructions_got_a_list_of_2_pages(self):
        # Setup
        # Exercise
        pages   = PageCreator.createPages(8, 4)
        # Test
        self.assertEqual(2, len(pages))
        self.assertEqual(0, pages[0]._number)
        self.assertEqual(1, pages[1]._number)
