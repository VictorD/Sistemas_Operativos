from unittest import TestCase
from src.Hardware.Hardware import HARDWARE
from src.Hardware.MMU.MMUPagination import MMUPagination
from src.So.Dispatcher.DispatcherPaging import DispatcherPaging
from src.So.MemoryManager.MemoryManager import MemoryManager
from src.So.Page import Page
from src.So.PageTable import PageTable
from src.So.Pcb.Pcb import PCB


class TestDispatcher(TestCase):

    def setUp(self):
        HARDWARE.setup(60)
        HARDWARE.cpu.pc                     = 5
        HARDWARE._mmu                       = MMUPagination(5, HARDWARE.memory)
        HARDWARE.mmu.limit                  = 10
        self.memoryManager                  = MemoryManager(5, 60)
        self.dispatcherSUT                  = DispatcherPaging(self.memoryManager)
        self.dispatcherSUT._memoryManager   = self.memoryManager
        self.pcbToSave                      = PCB(1, "C:/perrtanic/dogioh.dog")
        self.pcbToSave.baseAddress          = 3
        self.pcbToSave.size                 = 7

        self.pcbToLoad              = PCB(2, "C:/to.dog")
        self.pcbToLoad.baseAddress  = 1
        self.pcbToLoad.size         = 8
        self.pcbToLoad.pc           = 1

    def test_When_the_state_Of_a_running_PCB_is_saved_the_cpu_goes_idle(self):
        #Exersice
        self.dispatcherSUT.save(self.pcbToSave)

        #Test
        self.assertEqual(5, self.pcbToSave.pc)
        self.assertEqual(7, self.pcbToSave.size)
        self.assertEqual(-1, HARDWARE.cpu.pc)

    def test_when_a_pcb_is_loaded_the_state_of_PCB_The_CPU_and_MMU_Have_new_Values(self):
        # Setup
        aPaginationTable = PageTable()
        aPaginationTable.assocPages([Page.newPage(78, 0)])
        self.memoryManager.savePCBAndPageTable(2, aPaginationTable)

        # Exersice
        self.dispatcherSUT.load(self.pcbToLoad)

        # Test
        pageTableOfMMU  = HARDWARE.mmu._currentProgramPaginationTable
        self.assertEqual(1, HARDWARE.cpu.pc)
        self.assertEqual(8, HARDWARE.mmu.limit)
        self.assertNotEqual(aPaginationTable, pageTableOfMMU)
        self.assertEqual(aPaginationTable.getAllPages()[0]._number, pageTableOfMMU.getAllPages()[0]._number)
        self.assertEqual(8, HARDWARE.mmu.limit)