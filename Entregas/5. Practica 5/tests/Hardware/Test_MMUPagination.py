from unittest import TestCase
from src.Hardware.Hardware import HARDWARE
from src.Hardware.MMU.MMUPagination import MMUPagination
from src.So.Page import Page
from src.So.PageTable import PageTable


class TestMMUPagination(TestCase):

    def setUp(self):
        HARDWARE.setup(50)
        self.mmuSut                                 = MMUPagination(4, HARDWARE.memory)
        self.pft                                    = PageTable()
        self.mmuSut._currentProgramPaginationTable  = self.pft
        self.page1 = Page()
        self.page1._number = 0
        self.page2 = Page()
        self.page2._number = 1
        self.page3 = Page()
        self.page3._number = 2
        self.page4 = Page()
        self.page4._number = 3

    def test_A_MMU_with_a_frame_size_of_4_if_fetch_with_a_PC_equals_to_5_got_5(self):
        # Setup
        self.pft.setPage(self.page1)
        self.pft.setPage(self.page2)
        self.pft.assocFrames([0, 1])

        # Exercise
        addressToRead   = self.mmuSut.calculateAddress(5)

        # Test
        self.assertEqual(5, addressToRead)

    def test_A_MMU_with_a_frame_size_of_5_and_a_page_table_with_the_frame_1_if_fetch_with_a_PC_equals_to_0_got_5(self):
        # Setup
        self.pft.setPage(self.page1)
        self.pft.assocFrames([1])
        self.mmuSut._frameSize  = 5

        # Exercise
        addressToRead       = self.mmuSut.calculateAddress(0)

        # Test
        self.assertEqual(5, addressToRead)

    def test_A_MMU_with_a_pagination_table_of_4_pages_with_the_assigned_frames_are_5_2_1_3_and_frame_size_of_4_if_fetch_every_pc_got_the_correct_direction(self):
        # Setup
        self.pft.setPage(self.page1)
        self.pft.setPage(self.page2)
        self.pft.setPage(self.page3)
        self.pft.setPage(self.page4)
        self.pft.assocFrames([5, 2, 1, 3])

        # Test
        self.assertEqual(20, self.mmuSut.calculateAddress(0))
        self.assertEqual(21, self.mmuSut.calculateAddress(1))
        self.assertEqual(22, self.mmuSut.calculateAddress(2))
        self.assertEqual(23, self.mmuSut.calculateAddress(3))
        self.assertEqual(8, self.mmuSut.calculateAddress(4))
        self.assertEqual(9, self.mmuSut.calculateAddress(5))
        self.assertEqual(10, self.mmuSut.calculateAddress(6))
        self.assertEqual(11, self.mmuSut.calculateAddress(7))
        self.assertEqual(4, self.mmuSut.calculateAddress(8))
        self.assertEqual(5, self.mmuSut.calculateAddress(9))
        self.assertEqual(6, self.mmuSut.calculateAddress(10))
        self.assertEqual(7, self.mmuSut.calculateAddress(11))
        self.assertEqual(12, self.mmuSut.calculateAddress(12))
        self.assertEqual(13, self.mmuSut.calculateAddress(13))
        self.assertEqual(14, self.mmuSut.calculateAddress(14))
        self.assertEqual(15, self.mmuSut.calculateAddress(15))
