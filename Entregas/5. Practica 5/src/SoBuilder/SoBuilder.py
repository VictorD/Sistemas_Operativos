from src.Hardware.Hardware import HARDWARE
from src.Hardware.Irq import TIMEOUT_INTERRUPTION_TYPE, OUT_OF_MEMORY_INTERRUPTION_TYPE, KILL_INTERRUPTION_TYPE
from src.Hardware.MMU.MMUPagination import MMUPagination
from src.Hardware.Timer.Timer import Timer
from src.So.ContextSwitcher.ContextSwitch import ContextSwitch
from src.So.Dispatcher.DispatcherPaging import DispatcherPaging
from src.So.InterruptionHandlers.KillInterruptionHandlerPaging import KillInterruptionHandlerPaging
from src.So.InterruptionHandlers.OutOfMemoryInterruptionHandler import OutOfMemoryInterruptionHandler
from src.So.InterruptionHandlers.TimeOutMFQInterruptionHandler import TimeOutMFQInterruptionHandler
from src.So.Kernel import Kernel
from src.So.LoaderPaging import LoaderPaging
from src.So.MemoryManager.MemoryManager import MemoryManager
from src.So.Scheduler.SchedulerMultilevelFeedBackQueue import SchedulerMultilevelFeedBackQueue
from src.So.Scheduler.SchedulerMultilevelQueue import SchedulerMultilevelQueue
from src.So.Scheduler.SchedulerPriorityExp import SchedulerPriorityExp
from src.So.Scheduler.SchedulerPriorityNoExp import SchedulerPriorityNoExp
from src.So.Scheduler.SchedulerRoundRobin import SchedulerRoundRobin
from src.So.Switch.MultilevelFeedbackQueue.SwitchInMultilevelFeedbackQueue import SwitchInMultilevelFeedbackQueue
from src.So.Switch.MultilevelQueue.SwitchInMultilevelQueue import SwitchInMultilevelQueue
from src.So.Switch.MultilevelQueue.SwitchOutMultilevelQueue import SwitchOutMultilevelQueue
from src.So.Switch.SwitchInPriorityExpropiative import SwitchInPriorityExpropiative
from src.So.Switch.WithTimer.SwitchInWithTimer import SwitchInWithTimer
from src.So.Switch.WithTimer.SwitchOutWithTimer import SwitchOutWithTimer


class SoBuilder():

    @classmethod
    def createKernel(cls, aTypeOfScheduler, aTypeOfMemoryManagment, aQuantum = 0, aFrameSize = 0):
        schedulersTypes     = { "FIFO"                      : cls.withFIFOScheduler,
                                "ROUNDROBIN"                : cls.withRoundRobinScheduler,
                                "PRIORITYNOEXPROPIATIVE"    : cls.withPriorityNoExpropiativeScheduler,
                                "PRIORITYEXPROPIATIVE"      : cls.withPriorityExpropiativeScheduler,
                                "MULTILEVELQUEUE"           : cls.withMultilevelQueueScheduler,
                                "MULTILEVELFEEDBACKQUEUE"   : cls.withMultilevelFeedBackQueueScheduler}
        memoryManagmentTypes= { "SIMPLE"                    : cls.withOutPaging,
                                "PAGING"                    : cls.withPaging}

        newKernel = Kernel()
        return memoryManagmentTypes[aTypeOfMemoryManagment.value](schedulersTypes[aTypeOfScheduler.value](newKernel, aQuantum), aFrameSize)


    # ----- SCHEDULERS BUILDS METHODS ----- #
    @classmethod
    def withFIFOScheduler(cls, akernel, aQuantum):
        return akernel

    @classmethod
    def withRoundRobinScheduler(cls, akernel, aQuantum):
        HARDWARE.timer                  = Timer(HARDWARE.cpu, aQuantum)
        HARDWARE.clock._subscribers[0]  = HARDWARE.timer
        akernel.scheduler               = SchedulerRoundRobin(aQuantum)
        HARDWARE.timer.setOn()
        ContextSwitch().setSwitchOut(SwitchOutWithTimer())
        ContextSwitch().setSwitchIn(SwitchInWithTimer())
        return akernel

    @classmethod
    def withPriorityNoExpropiativeScheduler(cls, akernel, aQuantum):
        akernel.scheduler = SchedulerPriorityNoExp()
        return akernel

    @classmethod
    def withPriorityExpropiativeScheduler(cls, akernel, aQuantum):
        akernel.scheduler = SchedulerPriorityExp()
        ContextSwitch().setSwitchIn(SwitchInPriorityExpropiative())
        return akernel

    @classmethod
    def withMultilevelQueueScheduler(cls, akernel, aQuantum):
        akernel.scheduler               = SchedulerMultilevelQueue(aQuantum)
        HARDWARE.timer                  = Timer(HARDWARE.cpu, aQuantum)
        HARDWARE.clock._subscribers[0]  = HARDWARE.timer
        ContextSwitch().setSwitchOut(SwitchOutMultilevelQueue())
        ContextSwitch().setSwitchIn(SwitchInMultilevelQueue())
        return akernel

    @classmethod
    def withMultilevelFeedBackQueueScheduler(cls, akernel, aQuantum):
        HARDWARE.timer                  = Timer(HARDWARE.cpu)
        HARDWARE.clock._subscribers[0]  = HARDWARE.timer

        akernel.scheduler               = SchedulerMultilevelFeedBackQueue()

        timeOutHandler                  = TimeOutMFQInterruptionHandler(akernel)
        HARDWARE.interruptVector.register(TIMEOUT_INTERRUPTION_TYPE, timeOutHandler)

        ContextSwitch().setSwitchOut(SwitchOutWithTimer())
        ContextSwitch().setSwitchIn(SwitchInMultilevelFeedbackQueue())
        return akernel

    # ----- SCHEDULERS BUILDS METHODS ----- #
    @classmethod
    def withOutPaging(cls, akernel, aFrameSize):
        return akernel

    @classmethod
    def withPaging(cls, akernel, aFrameSize):
        akernel._memoryManager  = MemoryManager(aFrameSize, len(HARDWARE.memory._cells))
        akernel._loader         = LoaderPaging(akernel._memoryManager)
        akernel._dispatcher     = DispatcherPaging(akernel._memoryManager)
        HARDWARE._mmu           = MMUPagination(aFrameSize, HARDWARE.memory)
        HARDWARE.cpu._mmu       = HARDWARE.mmu
        outOfMemoryHandler      = OutOfMemoryInterruptionHandler(akernel)
        HARDWARE.interruptVector.register(OUT_OF_MEMORY_INTERRUPTION_TYPE, outOfMemoryHandler)
        killHandler = KillInterruptionHandlerPaging(akernel)
        HARDWARE.interruptVector.register(KILL_INTERRUPTION_TYPE, killHandler)
        return akernel