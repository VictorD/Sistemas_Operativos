

##  These are the instructions supported on our CPU
INSTRUCTION_IO = 'IO'
INSTRUCTION_CPU = 'CPU'
INSTRUCTION_EXIT = 'EXIT'
INSTRUCTION_SHUTDOWN = 'SD'


## Helper for emulated machine code
class ASM():

    @classmethod
    def EXIT(self, times):
        return [INSTRUCTION_EXIT] * times

    @classmethod
    def IO(self):
        return INSTRUCTION_IO

    @classmethod
    def SD(self):
        return INSTRUCTION_SHUTDOWN

    @classmethod
    def CPU(self, times):
        return [INSTRUCTION_CPU] * times

    @classmethod
    def isEXIT(self, instruction):
        return INSTRUCTION_EXIT == instruction

    @classmethod
    def isSD(self, instruction):
        return INSTRUCTION_SHUTDOWN == instruction

    @classmethod
    def isIO(self, instruction):
        return INSTRUCTION_IO == instruction
