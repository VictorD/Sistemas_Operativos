from src.Hardware.Directory import Directory


class HardDisk():

    def __init__(self):
            self._root = Directory("C:/")

    @property
    def root(self):
        return self._root

    def fetchProgram(self,aDirection):
        split = aDirection.split("/")
        return self.root.fetchProgram(split)

    def saveProgram(self,aProgram,aDirection):
        split = aDirection.split("/")
        self.root.saveProgram(aProgram,split)

    def newDirectory(self,aDirection):
        split = aDirection.split("/")
        self.root.newRute(split)