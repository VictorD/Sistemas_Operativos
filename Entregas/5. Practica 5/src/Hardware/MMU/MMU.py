from builtins import Exception
from src.Hardware.MMU.AbstractMMU import AbstractMMU

## emulates the Memory Management Unit (MMU)
class MMU(AbstractMMU):

    def __init__(self, memory):
        super().__init__(memory)
        self._baseDir = 0

    @property
    def baseDir(self):
        return self._baseDir

    @baseDir.setter
    def baseDir(self, baseDir):
        self._baseDir = baseDir

    def fetch(self,  logicalAddress):
        if (logicalAddress >= self._limit):
            raise Exception("Invalid Address,  {logicalAddress} is eq or higher than process limit: {limit}".format(limit = self._limit, logicalAddress = logicalAddress))
        else:
            physicalAddress = logicalAddress + self._baseDir
            return self._memory.get(physicalAddress)

    def __repr__(self):
        return "MMU Simple"