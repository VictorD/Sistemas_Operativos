from abc import abstractmethod


class AbstractMMU():

    def __init__(self, memory):
        self._memory = memory
        self._limit = 999

    @property
    def limit(self):
        return self._limit

    @limit.setter
    def limit(self, limit):
        self._limit = limit

    @abstractmethod
    def fetch(self, logicalAddress):
        raise NotImplementedError("To be implemented")
