from src.GanttDiagram.GanttDiagram import GanttDiagram
from src.Hardware.Asm import ASM
from src.Hardware.Hardware import *
from src.So.Pcb.PCBPriority import PCBPriority
from src.So.Program import Program
from src import log
from src.SoBuilder.MemoryType import MemoryType
from src.SoBuilder.SchedulerType import SchedulerType
from src.SoBuilder.SoBuilder import SoBuilder

##
##  MAIN 
##
if __name__ == '__main__':
    log.setupLogger()
    log.logger.info('Starting emulator')

    ## setup our hardware and set memory size to 25 "cells"
    HARDWARE.setup(25)
    # HARDWARE.setup(125)

    ## new create the Operative System Kernel
    # kernel          = SoBuilder.createKernel(SchedulerType.FIFO, MemoryType.SIMPLE)
    # kernel          = SoBuilder.createKernel(SchedulerType.PRIORITYNOEXPROPIATIVE, MemoryType.SIMPLE)
    # kernel          = SoBuilder.createKernel(SchedulerType.ROUNDROBIN, MemoryType.SIMPLE, 3)
    # kernel          = SoBuilder.createKernel(SchedulerType.PRIORITYEXPROPIATIVE, MemoryType.SIMPLE)
    # kernel          = SoBuilder.createKernel(SchedulerType.MULTILEVELQUEUE, MemoryType.SIMPLE, 3)
    # kernel          = SoBuilder.createKernel(SchedulerType.MULTILEVELFEEDBACKQUEUE, MemoryType.SIMPLE)

    kernel          = SoBuilder.createKernel(SchedulerType.FIFO, MemoryType.PAGING, aFrameSize=5)
    # kernel          = SoBuilder.createKernel(SchedulerType.PRIORITYNOEXPROPIATIVE, MemoryType.PAGING, aFrameSize=5)
    # kernel          = SoBuilder.createKernel(SchedulerType.ROUNDROBIN, MemoryType.PAGING, 3, 5)
    # kernel          = SoBuilder.createKernel(SchedulerType.PRIORITYEXPROPIATIVE, MemoryType.PAGING, aFrameSize=5)
    # kernel          = SoBuilder.createKernel(SchedulerType.MULTILEVELQUEUE, MemoryType.PAGING, 3, 5)
    # kernel          = SoBuilder.createKernel(SchedulerType.MULTILEVELFEEDBACKQUEUE, MemoryType.PAGING, aFrameSize=5)
    diagramaDeGantt = GanttDiagram.setGanttChart(kernel)

    ##  create a program
    prg1 = Program("prg1.dog", [ASM.CPU(2), ASM.IO(), ASM.CPU(3), ASM.IO(), ASM.CPU(2)])
    prg2 = Program("prg2.dog", [ASM.CPU(4), ASM.IO(), ASM.CPU(1)])
    # prg1 = Program("prg1.dog", [ASM.CPU(10), ASM.IO(), ASM.CPU(3), ASM.IO(), ASM.CPU(2)])
    # prg2 = Program("prg2.dog", [ASM.CPU(24), ASM.IO(), ASM.CPU(1)])
    prg3 = Program("prg3.dog", [ASM.CPU(3)])
    prg4 = Program("Shutdown.dog",[ASM.SD()])

    ## Create Directories in hard disk and save the programs there
    HARDWARE.harddisk.newDirectory("C:/programitasPerrunos")
    HARDWARE.harddisk.newDirectory("C:/programitasPerrunos/huesoSabueso")
    HARDWARE.harddisk.newDirectory("C:/programitasPerrunos/woofwoof")
    HARDWARE.harddisk.saveProgram(prg1,"C:/programitasPerrunos")
    HARDWARE.harddisk.saveProgram(prg2, "C:/programitasPerrunos")
    HARDWARE.harddisk.saveProgram(prg3, "C:/programitasPerrunos/huesoSabueso")
    HARDWARE.harddisk.saveProgram(prg4, "C:/programitasPerrunos/woofwoof")

    # execute all programs "concurrently"
    kernel.execute("C:/programitasPerrunos/prg1.dog", PCBPriority.HIGH)
    kernel.execute("C:/programitasPerrunos/prg2.dog", PCBPriority.LOW)
    kernel.execute("C:/programitasPerrunos/huesoSabueso/prg3.dog", PCBPriority.HIGHEST)
    # kernel.execute("C:/programitasPerrunos/woofwoof/Shutdown.dog", PCBPriority.LOWEST)

    ## start
    # HARDWARE.switchOn()
    HARDWARE.clock.do_ticks(22)
    # HARDWARE.clock.do_ticks(60)
    diagramaDeGantt.printResult()