
class MemoryTable():

    def __init__(self):
        self._table = {}

    def loadPCBAndPageTable (self, aPid, pcbPagTable):
        self._table[aPid]   = pcbPagTable

    def pageTableOf(self, aPidPCB):
        return self._table[aPidPCB]

    # Libera todos los frames usados por el Pid pasado y los devuelve.
    def releaseAndReturnsFrames(self, aPid):
        tableOfAPid = self._table[aPid]
        frames      = tableOfAPid.getAllFrames()
        tableOfAPid.clearFrames()
        return frames