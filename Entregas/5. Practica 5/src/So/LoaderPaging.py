from src.Hardware.Hardware import HARDWARE
from src.Hardware.Irq import IRQ, OUT_OF_MEMORY_INTERRUPTION_TYPE
from src.So.Loader import Loader
from src.So.PageCreator import PageCreator
from src.So.PageTable import PageTable


class LoaderPaging(Loader):

    def __init__(self, memoryManager):
        super().__init__()
        self._memoryManager = memoryManager

    def loadProgram(self, aPCB):

        # Fetch cambialo por read
        aProgram        = HARDWARE.harddisk.fetchProgram(aPCB.direction)
        sizeOfProgram   = len(aProgram.instructions)

        if self._memoryManager.hasMemoryFor(sizeOfProgram):
            paginationTable = self.createPageTable(sizeOfProgram, aPCB.pid) # pagina, pide los frames, crea la tabla y ya la carga en el memorymanager

            self.load(aProgram.instructions, paginationTable)

            aPCB.size = sizeOfProgram
            # terminar de cargar el programa
        else:
            outOfMemoryIRQ = IRQ(OUT_OF_MEMORY_INTERRUPTION_TYPE)
            HARDWARE.cpu._interruptVector.handle(outOfMemoryIRQ)

    def load(self, aInstructions, aPageTable):
        # Por cada pagina va a cargar las instrucciones que necesita en memoria segun el frame que le toca
        pageList = aPageTable.getAllPages()

        for aPage in pageList:
            # Se queda con las instrucciones
            initInstruction = aPage.firstInstruction()      # Calcula la posicion de la primera instrucion
            endInstruction  =  aPage.endInstruction()      # Calcula la posicion de la ultima instrucion

            self.write(aInstructions[initInstruction:endInstruction], aPageTable.frameOf(aPage._number))


    def write(self, instructions, aFrame):
        addresToWirte   = aFrame * self._memoryManager._frameSize # Se Calcula la posicion de memoria inicial

        for index in range(0, len(instructions)):
            HARDWARE.memory.put(addresToWirte, instructions[index])
            addresToWirte   +=1

    # Crea las paginas necesarias para alojar las instruccioens en memoria
    def paginatePcb(self, sizeOfProgram):
        return PageCreator.createPages(sizeOfProgram, self._memoryManager._frameSize)

    def createPageTable(self, sizeOfProgram, aPcbPid):
        pagesCreated    = self.paginatePcb(sizeOfProgram)
        usedFrames      = self._memoryManager.getFreeFrames(len(pagesCreated))
        paginationTable = PageTable.createWith(pagesCreated, usedFrames)

        self._memoryManager.savePCBAndPageTable(aPcbPid, paginationTable)
        return paginationTable