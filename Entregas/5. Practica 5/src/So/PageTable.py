from src.So.PTRow import PTRow


class PageTable():

    def __init__(self):
        self._table     = {}

    # Setea la pagina en una fila de la tabla cuya posicion corresponde al numero de la pagina
    def setPage(self, aKey):
        self._table[aKey._number] = PTRow.newRow(aKey)

    # asocia una frame a una pagina.
    def setFrameToPage(self, aKey, aFrame):
        self._table[aKey].registryFrame(aFrame)

    # retorna el frame correspondiente al numero de pagina. Puede retornar None si no hay frame asociado
    def frameOf(self, pageNumber):
        return self._table[pageNumber]._frame

    # Setea un listado de paginas a la tabla de paginas
    def assocPages(self, pages):
        for each in pages:
            self.setPage(each)

    # Asocia una lista de frames a la tabla. Asocia de la manera:
    # 1er frame de la lista al 1er page de la tabla
    # 2do frame de la lista al 2do page de la tabla
    # etc
    def assocFrames(self, frames):
        index = 0
        for each in frames:
            self.setFrameToPage(index, each)
            index += 1

    # Devuelve todos los frames que hay en la tabla.
    def getAllFrames(self):
        return list(filter(lambda x: not x is None, map(lambda x: x._frame, self._table.values())))

    # Limpia de frames la tabla.
    def clearFrames(self):
        for k in self._table.keys():
            self._table[k]._frame   = None

    @classmethod
    def createWith(cls, listOfPages, listOfFrames):
        newPageTable    = PageTable()
        newPageTable.assocPages(listOfPages)
        newPageTable.assocFrames(listOfFrames)
        return newPageTable

    def getAllPages(self):
        return  list(map(lambda x: x._page, self._table.values()))