
class PTRow():

    def __init__(self, page):
        self._page = page
        self._frame= None

    @classmethod
    def newRow(cls, page):
        newRow  = PTRow(page)
        return newRow

    def registryFrame(self, aFrame):
        self._frame = aFrame