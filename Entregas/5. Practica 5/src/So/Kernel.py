#!/usr/bin/env python
from src import log
from src.Hardware.Hardware import HARDWARE
from src.Hardware.Irq import KILL_INTERRUPTION_TYPE, IO_IN_INTERRUPTION_TYPE, IO_OUT_INTERRUPTION_TYPE, \
    NEW_INTERRUPTION_TYPE, IRQ, SHUTDOWN_INTERRUPTION_TYPE, TIMEOUT_INTERRUPTION_TYPE, OUT_OF_MEMORY_INTERRUPTION_TYPE
from src.Hardware.MMU.MMUPagination import MMUPagination
from src.Hardware.Timer.Timer import Timer
from src.So.ContextSwitcher.ContextSwitch import ContextSwitch
from src.So.Dispatcher.Dispatcher import Dispatcher
from src.So.Dispatcher.DispatcherPaging import DispatcherPaging
from src.So.InterruptionHandlers.OutOfMemoryInterruptionHandler import OutOfMemoryInterruptionHandler
from src.So.InterruptionHandlers.ShutDownInterruptionHandler import ShutDownInterruptionHandler
from src.So.InterruptionHandlers.TimeOutInterruptionHandler import TimeOutInterruptionHandler
from src.So.InterruptionHandlers.TimeOutMFQInterruptionHandler import TimeOutMFQInterruptionHandler
from src.So.IoDeviceController import IoDeviceController
from src.So.InterruptionHandlers.IoInInterruptionHandler import IoInInterruptionHandler
from src.So.InterruptionHandlers.IoOutInterruptionHandler import IoOutInterruptionHandler
from src.So.InterruptionHandlers.KillInterruptionHandler import KillInterruptionHandler
from src.So.Loader import Loader
from src.So.InterruptionHandlers.NewInterruptionHandler import NewInterruptionHandler
from src.So.LoaderPaging import LoaderPaging
from src.So.MemoryManager.MemoryManager import MemoryManager
from src.So.Pcb.PCBPriority import PCBPriority
from src.So.Pcb.PcbTable import PcbTable
from src.So.Scheduler.SchedulerFIFO import SchedulerFIFO
from src.So.Scheduler.SchedulerMultilevelFeedBackQueue import SchedulerMultilevelFeedBackQueue
from src.So.Scheduler.SchedulerMultilevelQueue import SchedulerMultilevelQueue
from src.So.Scheduler.SchedulerPriorityExp import SchedulerPriorityExp
from src.So.Scheduler.SchedulerPriorityNoExp import SchedulerPriorityNoExp
from src.So.Scheduler.SchedulerRoundRobin import SchedulerRoundRobin
from src.So.Switch.MultilevelFeedbackQueue.SwitchInMultilevelFeedbackQueue import SwitchInMultilevelFeedbackQueue
from src.So.Switch.MultilevelQueue.SwitchInMultilevelQueue import SwitchInMultilevelQueue
from src.So.Switch.MultilevelQueue.SwitchOutMultilevelQueue import SwitchOutMultilevelQueue
from src.So.Switch.SwitchInPriorityExpropiative import SwitchInPriorityExpropiative
from src.So.Switch.WithTimer.SwitchInWithTimer import SwitchInWithTimer
from src.So.Switch.WithTimer.SwitchOutWithTimer import SwitchOutWithTimer


# emulates the core of an Operative System
class Kernel():

    def __init__(self):
        ## setup interruption handlers
        killHandler = KillInterruptionHandler(self)
        HARDWARE.interruptVector.register(KILL_INTERRUPTION_TYPE, killHandler)

        ioInHandler = IoInInterruptionHandler(self)
        HARDWARE.interruptVector.register(IO_IN_INTERRUPTION_TYPE, ioInHandler)

        ioOutHandler = IoOutInterruptionHandler(self)
        HARDWARE.interruptVector.register(IO_OUT_INTERRUPTION_TYPE, ioOutHandler)

        newHandler = NewInterruptionHandler(self)
        HARDWARE.interruptVector.register(NEW_INTERRUPTION_TYPE, newHandler)

        shutDownHandler = ShutDownInterruptionHandler(self)
        HARDWARE.interruptVector.register(SHUTDOWN_INTERRUPTION_TYPE, shutDownHandler)

        timeOutHandler = TimeOutInterruptionHandler(self)
        HARDWARE.interruptVector.register(TIMEOUT_INTERRUPTION_TYPE, timeOutHandler)

        ## controls the Hardware's I/O Device
        self._ioDeviceController = IoDeviceController(HARDWARE.ioDevice)

        # Setup of pcbtable
        self._pcbtable = PcbTable()

        # Setup of Scheduler
        self._scheduler = SchedulerFIFO()

        # Setup of dispatcher
        self._dispatcher = Dispatcher()

        # Setup of loader
        self._loader = Loader()

        # Setup of Memory Manager
        self._memoryManager = None

    ## emulates a "system call" for programs execution
    def execute(self, aDirection, aPriority=PCBPriority.MEDIUM):
        newIRQ = IRQ(NEW_INTERRUPTION_TYPE, [aDirection, aPriority])
        HARDWARE.cpu._interruptVector.handle(newIRQ)
        log.logger.info("\nExecuting program in: " + aDirection)
        log.logger.info(HARDWARE)

    # Check if the Cpu is Idle
    def isCPUIdle(self):
        return HARDWARE.cpu.pc == -1

    # Cleans all devices and data types
    def cleanAll(self):
        HARDWARE.memory.cleanAll()
        log.logger.info("MEMORY CLEANED")

        self.pcbTable.cleanAll()
        log.logger.info("PCB TABLE CLEANED")

        self.scheduler.cleanAll()
        log.logger.info("SCHEDULER CLEANED")

        self.ioDeviceController.cleanAll()
        log.logger.info("IO DEVICE CONTROLLERS CLEANED")

        HARDWARE.timer.setOff()

    # ShutDowns the machine
    def shutDown(self):
        HARDWARE.switchOff()

    # ----- Getters & Setters ----- #
    @property
    def ioDeviceController(self):
        return self._ioDeviceController

    @property
    def pcbTable(self):
        return self._pcbtable

    @property
    def scheduler(self):
        return self._scheduler

    @property
    def dispatcher(self):
        return self._dispatcher

    @property
    def loader(self):
        return self._loader

    @scheduler.setter
    def scheduler(self, aScheduler):
        self._scheduler = aScheduler
    # ----- End of Getters & Setters ----- #

    def __repr__(self):
        return "Kernel "
