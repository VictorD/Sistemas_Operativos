

# Have the responsibility of load the program on the memory
from src.Hardware.Hardware import HARDWARE


class Loader():

    def __init__(self):
        self._addressToAssign   = 0

    def loadProgram(self, aPCB):

        #Fetch cambialo por read
        aProgram= HARDWARE.harddisk.fetchProgram(aPCB.direction)

        #el directorio solo deberia guardar la lista de instrucciones
        sizeOfProgram   = len(aProgram.instructions)
        initAddressMemory = self._addressToAssign

        aPCB.baseAddress = initAddressMemory
        aPCB.size = sizeOfProgram

        endAddressMemory = initAddressMemory + sizeOfProgram

        # loads the program in main memory
        for index in range(initAddressMemory, endAddressMemory):
            inst = aProgram.instructions[index - initAddressMemory]
            HARDWARE.memory.put(index, inst)

        self._addressToAssign = endAddressMemory

    @property
    def addressToAssign(self):
        return self._addressToAssign
