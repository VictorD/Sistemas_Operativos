from src.So.InterruptionHandlers.AbstractInterruptionHandler import AbstractInterruptionHandler


class NewInterruptionHandler(AbstractInterruptionHandler):

    def execute(self, irq):
        directionOfProgramToLoad= irq.parameters[0]
        priorityOfProgramToLoad = irq.parameters[1]

        pcbCreated = self.kernel.pcbTable.createNewPcb(directionOfProgramToLoad, priorityOfProgramToLoad)

        self.kernel.loader.loadProgram(pcbCreated)

        self.contextSwitchInToCPU(pcbCreated)
