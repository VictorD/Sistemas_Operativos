from src.OutOfMemoryException import OutOfMemoryException
from src.So.InterruptionHandlers.AbstractInterruptionHandler import AbstractInterruptionHandler


class OutOfMemoryInterruptionHandler(AbstractInterruptionHandler):

    def __init__(self, kernel):
        super().__init__(kernel)

    def execute(self, irq):
        raise OutOfMemoryException("Out Of Memory!")