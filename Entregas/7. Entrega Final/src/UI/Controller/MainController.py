import sys
from PyQt5 import QtWidgets
from src.GanttDiagram.GanttDiagram import GanttDiagram
from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.So.Program import Program
from src.So.Pcb.PCBPriority import PCBPriority
from src.So.Segment import Segment
from src.SoBuilder.SoBuilder import SoBuilder
from src.UI.Controller.StateMonitorController import StateMonitorController
from src.UI.View.MainWindowView import MainWindowView


class MainController():

    def __init__(self):
        self.memoryValid        = False
        self.memoryTypeValid    = True
        self.schedulerTypeValid = True

        app             = QtWidgets.QApplication(sys.argv)
        mainWindow      = QtWidgets.QDialog()
        self.__view     = MainWindowView(mainWindow, self)
        self.validateMemorySize()
        mainWindow.show()
        sys.exit(app.exec_())

    def buildAndStart(self):
        memorySize      = int(self.__view.MemorySizeEdit.text())
        aQuantum        = int(self.__view.QuantumSpinBox.text())
        aFrameSize      = int(self.__view.FrameSizeSpinBox.text())
        schedulerType   = self.__view.SchedulerComboBox.itemData(self.__view.SchedulerComboBox.currentIndex())
        memoryType      = self.__view.MemoryComboBox.itemData(self.__view.MemoryComboBox.currentIndex())
        victimType      = self.__view.VictimComboBox.itemData(self.__view.VictimComboBox.currentIndex())

        HARDWARE.setup(memorySize)
        kernel     = SoBuilder.createKernel(schedulerType, memoryType, aQuantum, aFrameSize, victimType)
        ganttChart = GanttDiagram.setGanttChart(kernel)
        self.SetPrograms(kernel)
        self.__view.hide()
        StateMonitorController(kernel, ganttChart)

    def validateMemorySize(self):
        errorLabel      = self.__view.MemorySizeErrorlabel
        valueInString   = self.__view.MemorySizeEdit.text()
        value           = int(0 if valueInString == "" else valueInString)
        checkSize       = divmod(value, 8)
        self.memoryValid = True
        if checkSize[1] != 0 or checkSize == (0,0):
            errorLabel.setHidden(False)
            self.memoryValid = False
        elif not errorLabel.isHidden():
            errorLabel.setHidden(True)
        self.validateContinueButton()

    def controllerValidateMemorySelected(self):
        spinBox         = self.__view.FrameSizeSpinBox
        comboBox        = self.__view.VictimComboBox
        memorySelected  = self.__view.MemoryComboBox.currentText()
        self.memoryTypeValid = False
        if memorySelected == "Paging":
            spinBox.setEnabled(True)
            if comboBox.isEnabled():
                comboBox.setDisabled(True)
            self.validateFrameSize()
        elif memorySelected == "On Demand":
            spinBox.setEnabled(True)
            comboBox.setEnabled(True)
            self.validateFrameSize()
        elif spinBox.isEnabled() or comboBox.isEnabled():
            self.memoryTypeValid = True
            spinBox.setDisabled(True)
            comboBox.setDisabled(True)
            self.__view.FrameSizeErrorLabel.setHidden(True)
        self.validateContinueButton()

    def validateFrameSize(self):
        errorLabel          = self.__view.FrameSizeErrorLabel
        memorySizeSelected  = int(0 if self.__view.MemorySizeEdit.text() == "" else self.__view.MemorySizeEdit.text())
        frameSizeSelected   = int(self.__view.FrameSizeSpinBox.value())
        checkFrameValue     = (0, 0) if frameSizeSelected == 0 else divmod(memorySizeSelected, frameSizeSelected)
        self.memoryTypeValid= True
        if checkFrameValue[1] != 0 or memorySizeSelected == 0:
            errorLabel.setHidden(False)
            self.memoryTypeValid = False
        elif not errorLabel.isHidden():
            errorLabel.setHidden(True)
        self.validateContinueButton()

    def validateSchedulerType(self):
        schedulerSelected   = self.__view.SchedulerComboBox.currentText()
        if schedulerSelected == "Round Robin":
            self.__view.QuantumSpinBox.setEnabled(True)
            self.validateQuantum()
        else:
            self.schedulerTypeValid = True
            self.__view.QuantumSpinBox.setEnabled(False)
            self.__view.QuantumErrorLabel.setHidden(True)
        self.validateContinueButton()

    def validateQuantum(self):
        errorLabel  = self.__view.QuantumErrorLabel
        quantum     = int(self.__view.QuantumSpinBox.value())
        self.schedulerTypeValid = True
        if quantum == 0:
            errorLabel.setHidden(False)
            self.schedulerTypeValid = False
        elif not errorLabel.isHidden():
            errorLabel.setHidden(True)
        self.validateContinueButton()

    def validateContinueButton(self):
        if self.memoryValid and self.memoryTypeValid and self.schedulerTypeValid and (self.__view.UserRadioButton.isChecked() or (self.__view.UserLineEdit.text() == "pepita" and self.__view.PasswordLineEdit.text() == "vuelavuela")):
            self.__view.ContinuePushButton.setEnabled(True)
        elif self.__view.ContinuePushButton.isEnabled():
            self.__view.ContinuePushButton.setEnabled(False)

    def SetPrograms(self, akernel):
        HARDWARE.harddisk.newDirectory("C:/programitasPerrunos")
        HARDWARE.harddisk.newDirectory("C:/programitasPerrunos/huesoSabueso")
        HARDWARE.harddisk.newDirectory("C:/programitasPerrunos/woofwoof")
        HARDWARE.harddisk.newDirectory("C:/system")
        HARDWARE.harddisk.newDirectory("C:/windows/fake system")
        HARDWARE.harddisk.newDirectory("C:/last Chance/seriusly")
        HARDWARE.harddisk.newDirectory("C:/Dogginator")
        HARDWARE.harddisk.newDirectory("C:/log/system log")

        ##  create a program

        segment1prg1 = Segment([ASM.CPU(2), ASM.IO(), ASM.CPU(3), ASM.IO(), ASM.CPU(2), ASM.EXIT(1)])
        prg1 = Program("El Loco del SO.dog", [segment1prg1])

        segment1prg2 = Segment([ASM.VARIABLE(1)], "WRITE")
        segment2prg2 = Segment([ASM.CPU(4), ASM.IO(), ASM.CPU(1), ASM.SETVARIABLE('0'), ASM.EXIT(1)])
        prg2 = Program("Batch.dog", [segment1prg2, segment2prg2])

        segment1prg3 = Segment([ASM.VARIABLE(3)], "WRITE")
        segment2prg3 = Segment([ASM.CPU(3), ASM.SETVARIABLE('0'), ASM.SETVARIABLE('1'), ASM.SETVARIABLE('2'), ASM.EXIT(1)])
        prg3 = Program("Kernel.dog", [segment1prg3, segment2prg3])

        segment1prg4 = Segment([ASM.CPU(15), ASM.IO(), ASM.EXIT(1)])
        prg4 = Program("Microphone.dog", [segment1prg4])

        segment1prg5 = Segment([ASM.VARIABLE(1)], "WRITE")
        segment2prg5 = Segment([ASM.CPU(2), ASM.IO(), ASM.SETVARIABLE('0'), ASM.EXIT(1)])
        prg5 = Program("Do Nothing.dog", [segment1prg5, segment2prg5])

        segment1prg6 = Segment([ASM.CPU(1), ASM.CPU(1), ASM.CPU(1), ASM.CPU(1)])
        prg6 = Program("Antivirus.dog", [segment1prg6])
        segment1prg7 = Segment([ASM.IO(), ASM.CPU(1)])
        prg7 = Program("Stress Me.dog", [segment1prg7])
        segment1prg8 = Segment([ASM.IO(), ASM.CPU(1)])
        prg8 = Program("Shutdown.dog", [segment1prg8])

        ## Create Directories in hard disk and save the programs there
        HARDWARE.harddisk.saveProgram(prg1, "C:/programitasPerrunos")
        HARDWARE.harddisk.saveProgram(prg2, "C:/programitasPerrunos/huesoSabueso")
        HARDWARE.harddisk.saveProgram(prg3, "C:/programitasPerrunos/woofwoof")
        HARDWARE.harddisk.saveProgram(prg4, "C:/system")
        HARDWARE.harddisk.saveProgram(prg5, "C:/windows/fake system")
        HARDWARE.harddisk.saveProgram(prg6, "C:/last Chance/seriusly")
        HARDWARE.harddisk.saveProgram(prg7, "C:/Dogginator")
        HARDWARE.harddisk.saveProgram(prg8, "C:/log/system log")

        ## register new user and log in
        HARDWARE.harddisk.userManager.registerUser("pepita", "vuelavuela")
        self.logUser()

        # execute all programs "concurrently"
        akernel.execute("C:/programitasPerrunos/El Loco del SO.dog", PCBPriority.HIGH)
        akernel.execute("C:/programitasPerrunos/huesoSabueso/Batch.dog", PCBPriority.HIGHEST)
        akernel.execute("C:/programitasPerrunos/woofwoof/Kernel.dog", PCBPriority.LOW)
        akernel.execute("C:/system/Microphone.dog", PCBPriority.MEDIUM)
        akernel.execute("C:/windows/fake system/Do Nothing.dog", PCBPriority.LOWEST)
        akernel.execute("C:/last Chance/seriusly/Antivirus.dog", PCBPriority.HIGH)
        akernel.execute("C:/Dogginator/Stress Me.dog", PCBPriority.MEDIUM)
        akernel.execute("C:/log/system log/Shutdown.dog", PCBPriority.LOW)

    def logUser(self):
        if not self.__view.UserRadioButton.isChecked():
            HARDWARE.harddisk.userManager.logInUser(self.__view.UserLineEdit.text(), self.__view.PasswordLineEdit.text())

if __name__ == "__main__":

    mainController = MainController()