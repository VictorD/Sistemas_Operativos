from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QListWidgetItem
from src.Hardware.Hardware import HARDWARE
from src.So.MemoryManager.MemoryManagerOnDemand import MemoryManagerOnDemand
from src.So.MemoryManager.MemoryManagerPaging import MemoryManagerPaging
from src.So.Pcb.Pcb import PCB
from src.So.Scheduler.SchedulerFIFO import SchedulerFIFO
from src.So.Scheduler.SchedulerRoundRobin import SchedulerRoundRobin
from src.SoStateSnapshot import SoStateSnapshot
from src.StatisticsCollector import STATISTICSCOLLECTOR
from src.UI.Controller.GanttChartController import GanttChartController
from src.UI.Controller.IOQueueController import IOQueueController
from src.UI.Controller.ListedPCBReadyQueueController import ListedPCBReadyQueueController
from src.UI.Controller.MMUPageTableController import MMUPageTableController
from src.UI.Controller.PCBListController import PCBListController
from src.UI.Controller.ProcessInspectorController import ProcessInspectorController
from src.UI.View.StateMonitorView import StateMonitorView

class StateMonitorController():

    def __init__(self, aKernel, aGantChart):
        ###Variables###
        #[Sistema Real]#
        self.currentGantChart   = aGantChart
        self.currentKernel      = aKernel

        self.tickCounter = 0

        #[Estado a Mostrar]#
        self.gantChartToSee     = self.currentGantChart
        self.kernelToSee        = self.currentKernel
        self.statisticsToSee    = STATISTICSCOLLECTOR
        self.hardwareToSee      = HARDWARE

        self.window             = QtWidgets.QDialog()
        self.__view             = StateMonitorView(self.window, self)

        ###Seteo de estados de UI###
        self.soStates   = []
        self.soStates.append(SoStateSnapshot.takeSnapshot(self.currentKernel, self.currentGantChart, STATISTICSCOLLECTOR)) # guarda el estado inicial del currentKernel como estado 0
        self.setSchedulerType()
        self.setPCBList()
        self.setRunningPCB()
        self.setStatisticsSO()
        self.setSlideStates()
        self.setMMUTableButton()
        self.setLoggedUser()

        ###Mostrar###
        self.window.show()

    def setPCBList(self):
        pcbList = self.kernelToSee.scheduler.readyQueue

        for eachPCB in pcbList:
            item = QListWidgetItem()
            item.setText(repr(eachPCB))
            item.setData(Qt.ItemIsSelectable, eachPCB)
            self.__view.ReadyQueueListWidget.addItem(item)

    def setRunningPCB(self):
        self.__view.PCBRunningLabel.setText(repr(self.kernelToSee.pcbTable.pcbRunning))
        self.enableSeeRunningPCBButton()

    def setSchedulerType(self):
        currentKernel   = self.kernelToSee.scheduler
        self.__view.SchedulerTypSelectedLabel.setText(repr(currentKernel))
        if currentKernel.__class__ == SchedulerFIFO or currentKernel.__class__ == SchedulerRoundRobin:
            self.__view.ListPushButton.setHidden(True)
        else:
            self.__view.SeePCBPushButton.setHidden(True)
            self.__view.ListPushButton.setEnabled(True)


    def openPCBInspectorForRunningPCB(self):
        self.openPCBInspector(self.kernelToSee.pcbTable.pcbRunning)

    def openPCBInspector(self, aPCB):
        ProcessInspectorController(aPCB, self.kernelToSee.memoryManager)

    def openPCBListWindow(self):
        PCBListController(self.kernelToSee.pcbTable, self.kernelToSee.memoryManager)

    def tick(self):

        HARDWARE.clock.tick(self.tickCounter)
        self.soStates.append(SoStateSnapshot.takeSnapshot(self.currentKernel, self.currentGantChart, STATISTICSCOLLECTOR))

        self.gantChartToSee = self.currentGantChart
        self.kernelToSee    = self.currentKernel
        self.statisticsToSee= STATISTICSCOLLECTOR
        self.tickCounter += 1
        self.__view.KernelStateSlider.setMaximum(self.tickCounter)
        self.__view.TicksExecutedQuantityLabel.setText(repr(self.tickCounter))
        self.__view.KernelStateSlider.setSliderPosition(self.tickCounter)

    def doTicks(self):
        ticksToDo=self.__view.TicksSpinBox.value()
        for each in range(0,ticksToDo):
            self.tick()

    def setStatisticsSO(self):
        if self.kernelToSee._memoryManager.__class__ != MemoryManagerOnDemand:
            self.__view.PageFaultValueLabel.hide()
            self.__view.PageFaultLabel.setDisabled(True)
            self.__view.SwapInValueLabel.hide()
            self.__view.SwapInLabel.setDisabled(True)
            self.__view.SwapOutValueLabel.hide()
            self.__view.SwapOutLabel.setDisabled(True)
            self.__view.VictimValueLabel.hide()
            self.__view.VictimLabel.setDisabled(True)
        else:
            self.__view.PageFaultValueLabel.setText(repr(self.statisticsToSee.pageFaultCounter))
            self.__view.SwapInValueLabel.setText(repr(self.statisticsToSee.swapInCounter))
            self.__view.SwapOutValueLabel.setText(repr(self.statisticsToSee.swapOutCounter))
            self.__view.VictimValueLabel.setText(repr(self.statisticsToSee.victimCounter))

    def update(self):
        self.__view.ReadyQueueListWidget.clear()
        self.__view.SeePCBPushButton.setDisabled(True)

        self.setRunningPCB()
        self.setStatisticsSO()
        self.setPCBList()

    def setSlideStates(self):
        self.__view.KernelStateSlider.setMinimum(0)
        self.__view.KernelStateSlider.setMaximum(len(self.soStates)-1)
        self.__view.KernelStateSlider.setTickInterval(1)
        self.__view.KernelStateSlider.setValue(0)
        self.__view.KernelStateSlider.valueChanged.connect(self.setStateAndUpdate)


    def setStateAndUpdate(self):
        index                   = self.__view.KernelStateSlider.value()
        self.__view.TicksExecutedQuantityLabel.setText(repr(index))
        stateToLoad             = self.soStates[index]
        ###Setea el estado del SO al momento del tick seleccionado y actualiza
        self.gantChartToSee     = stateToLoad.gantchart
        self.kernelToSee        = stateToLoad.kernel
        self.statisticsToSee    = stateToLoad.statisticsCollector
        self.hardwareToSee      = stateToLoad.hardware
        self.update()

    def enableDisableSeePcbButton(self):
        if self.__view.ReadyQueueListWidget.currentItem().data(1).__class__ == PCB:
            self.__view.SeePCBPushButton.setEnabled(True)

    def enableSeeRunningPCBButton(self):
        if self.kernelToSee.pcbTable.pcbRunning.__class__ == PCB:
            self.__view.SeeRunningPushButton.setEnabled(True)

    def openGanttChart(self):
        GanttChartController(self.gantChartToSee)

    def openIOListWindow(self):
        IOQueueController(self.kernelToSee.ioDeviceController, self.kernelToSee.memoryManager)

    def setMMUTableButton(self):
        if self.kernelToSee._memoryManager.__class__ == MemoryManagerOnDemand or self.kernelToSee._memoryManager.__class__ == MemoryManagerPaging:
            self.__view.MMUTablePushButton.setEnabled(True)

    def openMMUPageTableWindow(self):
        MMUPageTableController(self.hardwareToSee.mmu._currentProgramPaginationTable)

    def openListPcbReadyQueueWindow(self):
        ListedPCBReadyQueueController(self.kernelToSee.scheduler.readyQueueListed(), self.kernelToSee.memoryManager)

    def setLoggedUser(self):
        currentUser = HARDWARE.harddisk.userManager.getCurrentUserName()
        if currentUser != "none":
            self.__view.UserLoggedValueLabel.setText(currentUser)