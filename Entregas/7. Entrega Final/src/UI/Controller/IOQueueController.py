from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QListWidgetItem

from src.So.Pcb.Pcb import PCB
from src.UI.Controller.ProcessInspectorController import ProcessInspectorController
from src.UI.View.IOQueueView import IOQueueView


class IOQueueController():

    def __init__(self, aIoDeviceControler, aMemoryManager):
        self.deviceController   = aIoDeviceControler
        self.memoryManager      = aMemoryManager
        self.window = QtWidgets.QDialog()
        self.__view = IOQueueView(self.window, self)
        self.setRunningPCB()
        self.setIOQueue()

        self.window.show()

    def setRunningPCB(self):
        self.__view.RunningValueLabel.setText(repr(self.deviceController._currentPCB))
        self.enableRunningPCBButton()

    def setIOQueue(self):
        for eachPCB in self.deviceController._waiting_queue:
            pcbToAdd = eachPCB['pcb']
            item = QListWidgetItem()
            item.setText(repr(pcbToAdd))
            item.setData(Qt.ItemIsSelectable, pcbToAdd)
            self.__view.WaitingQueueListWidget.addItem(item)

    def openPCBInspectorForRunning(self):
        self.openPCBInspector(self.deviceController._currentPCB)

    def openPCBInspector(self, aPCB):
        ProcessInspectorController(aPCB, self.memoryManager)

    def enableButton(self):
        if self.__view.WaitingQueueListWidget.currentItem().data(1).__class__ == PCB:
            self.__view.SeePCBPushButton.setEnabled(True)

    def enableRunningPCBButton(self):
        if self.deviceController._currentPCB.__class__ == PCB:
            self.__view.SeeRunningPushButton.setEnabled(True)