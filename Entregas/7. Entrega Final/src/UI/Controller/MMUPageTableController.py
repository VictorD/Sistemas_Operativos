from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QTableWidgetItem

from src.UI.View.MMUPageTableView import MMUPageTableView


class MMUPageTableController():

    def __init__(self, aPageTable):
        self.pageTable      = aPageTable

        self.window         = QtWidgets.QDialog()
        self.__view         = MMUPageTableView(self.window, self)
        self.loadTable()
        self.window.show()

    def loadTable(self):
        pageTableRows = self.pageTable._table.values()
        pcbTableView = self.__view.PCBTableWidget
        indexer = 0
        for eachRow in pageTableRows:
            pcbTableView.insertRow(indexer)  # Va a insertar una nueva fila a la PCBTableWidget
            self.loadRow(eachRow, pcbTableView, indexer)
            indexer += 1

    def loadRow(self, aPTRow, tableView, rowIndex):
        rowInfo = [repr(aPTRow._page),
                   repr(aPTRow._frame),
                   repr(aPTRow._timeStampEntryToMemory),
                   repr(aPTRow.isDirty()),
                   repr(aPTRow._timeStampRead),
                   repr(aPTRow.hasSecondChance()),
                   repr(aPTRow._hasSwapFrame)]
        indexer = 0
        for eachInfo in rowInfo:
            item = QTableWidgetItem()
            item.setText(eachInfo)
            tableView.setItem(rowIndex, indexer, item)
            indexer += 1