from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QTableWidgetItem

from src.UI.View.ProcessInspectorView import ProcessInspectorView


class ProcessInspectorController():

    def __init__(self, aPCB, aMemoryManager):
        self.pcb                = aPCB
        self.memoryManager      = aMemoryManager
        self.window             = QtWidgets.QDialog()
        self.__view             = ProcessInspectorView(self.window, self)
        self.loadInfo()
        self.window.show()

    def loadInfo(self):
        self.__view.PidValueLabel.setText(str(self.pcb.pid))
        self.__view.StateValueLabel.setText(repr(self.pcb.state))
        self.__view.PCValueLabel.setText(str(self.pcb.pc))
        self.__view.PriorityValueLabel.setText(repr(self.pcb.priority))
        self.__view.SizeValueLabel.setText(str(self.pcb.size))
        self.loadTable()

    def loadTable(self):
        if self.memoryManager is None:
            self.__view.PCBTableWidget.setDisabled(True)
        else:
            self.loadRows()

    def loadRows(self):
        pageTableRows   = self.memoryManager.getPageTableOf(self.pcb.pid)._table.values()
        pcbTableView= self.__view.PCBTableWidget
        indexer     = 0
        for eachRow in pageTableRows:
            pcbTableView.insertRow(indexer)             # Va a insertar una nueva fila a la PCBTableWidget
            self.loadRow(eachRow, pcbTableView, indexer)
            indexer += 1


    def loadRow(self, aPTRow, tableView, rowIndex):
        rowInfo = [ repr(aPTRow._page),
                    repr(aPTRow._frame),
                    repr(aPTRow._timeStampEntryToMemory),
                    repr(aPTRow.isDirty()),
                    repr(aPTRow._timeStampRead),
                    repr(aPTRow.hasSecondChance()),
                    repr(aPTRow._hasSwapFrame)]
        indexer = 0
        for eachInfo in rowInfo:
            item = QTableWidgetItem()
            item.setText(eachInfo)
            tableView.setItem(rowIndex, indexer, item)
            indexer += 1