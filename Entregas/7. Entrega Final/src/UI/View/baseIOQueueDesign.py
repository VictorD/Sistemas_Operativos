# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'rsc/IOQueueWindow.ui'
#
# Created by: PyQt5 UI code generator 5.11
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_IoQueue(object):
    def setupUi(self, IoQueue):
        IoQueue.setObjectName("IoQueue")
        IoQueue.resize(180, 260)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(IoQueue.sizePolicy().hasHeightForWidth())
        IoQueue.setSizePolicy(sizePolicy)
        IoQueue.setMinimumSize(QtCore.QSize(180, 260))
        IoQueue.setMaximumSize(QtCore.QSize(180, 260))
        IoQueue.setBaseSize(QtCore.QSize(180, 260))
        self.WaitingQueueListWidget = QtWidgets.QListWidget(IoQueue)
        self.WaitingQueueListWidget.setGeometry(QtCore.QRect(9, 60, 91, 192))
        self.WaitingQueueListWidget.setObjectName("WaitingQueueListWidget")
        self.SeePCBPushButton = QtWidgets.QPushButton(IoQueue)
        self.SeePCBPushButton.setEnabled(False)
        self.SeePCBPushButton.setGeometry(QtCore.QRect(110, 60, 61, 23))
        self.SeePCBPushButton.setObjectName("SeePCBPushButton")
        self.ClosePushButton = QtWidgets.QPushButton(IoQueue)
        self.ClosePushButton.setGeometry(QtCore.QRect(110, 230, 61, 23))
        self.ClosePushButton.setObjectName("ClosePushButton")
        self.WaitingQueueLabel = QtWidgets.QLabel(IoQueue)
        self.WaitingQueueLabel.setGeometry(QtCore.QRect(20, 36, 81, 16))
        self.WaitingQueueLabel.setObjectName("WaitingQueueLabel")
        self.RunningValueLabel = QtWidgets.QLabel(IoQueue)
        self.RunningValueLabel.setGeometry(QtCore.QRect(60, 14, 71, 16))
        self.RunningValueLabel.setObjectName("RunningValueLabel")
        self.SeeRunningPushButton = QtWidgets.QPushButton(IoQueue)
        self.SeeRunningPushButton.setEnabled(False)
        self.SeeRunningPushButton.setGeometry(QtCore.QRect(110, 10, 61, 23))
        self.SeeRunningPushButton.setObjectName("SeeRunningPushButton")
        self.RunningLabel = QtWidgets.QLabel(IoQueue)
        self.RunningLabel.setGeometry(QtCore.QRect(10, 14, 71, 16))
        self.RunningLabel.setObjectName("RunningLabel")

        self.retranslateUi(IoQueue)
        self.ClosePushButton.clicked['bool'].connect(IoQueue.close)
        QtCore.QMetaObject.connectSlotsByName(IoQueue)

    def retranslateUi(self, IoQueue):
        _translate = QtCore.QCoreApplication.translate
        IoQueue.setWindowTitle(_translate("IoQueue", "I/O Queue"))
        self.SeePCBPushButton.setText(_translate("IoQueue", "See PCB"))
        self.ClosePushButton.setText(_translate("IoQueue", "Close"))
        self.WaitingQueueLabel.setText(_translate("IoQueue", "<html><head/><body><p><span style=\" font-style:italic;\">Waiting Queue</span></p></body></html>"))
        self.RunningValueLabel.setText(_translate("IoQueue", "<html><head/><body><p>No Pid</p></body></html>"))
        self.SeeRunningPushButton.setText(_translate("IoQueue", "See"))
        self.RunningLabel.setText(_translate("IoQueue", "<html><head/><body><p><span style=\" font-style:italic;\">Running:</span></p></body></html>"))

