# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'rsc/PcbTableWindow.ui'
#
# Created by: PyQt5 UI code generator 5.11
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_PCBTableView(object):
    def setupUi(self, PCBTableView):
        PCBTableView.setObjectName("PCBTableView")
        PCBTableView.resize(190, 210)
        PCBTableView.setMinimumSize(QtCore.QSize(190, 210))
        PCBTableView.setMaximumSize(QtCore.QSize(190, 210))
        PCBTableView.setBaseSize(QtCore.QSize(190, 210))
        self.PcbListWidget = QtWidgets.QListWidget(PCBTableView)
        self.PcbListWidget.setGeometry(QtCore.QRect(9, 10, 91, 192))
        self.PcbListWidget.setObjectName("PcbListWidget")
        self.SeePCBPushButton = QtWidgets.QPushButton(PCBTableView)
        self.SeePCBPushButton.setEnabled(False)
        self.SeePCBPushButton.setGeometry(QtCore.QRect(109, 10, 75, 23))
        self.SeePCBPushButton.setObjectName("SeePCBPushButton")
        self.ClosePushButton = QtWidgets.QPushButton(PCBTableView)
        self.ClosePushButton.setGeometry(QtCore.QRect(109, 180, 75, 23))
        self.ClosePushButton.setObjectName("ClosePushButton")

        self.retranslateUi(PCBTableView)
        self.ClosePushButton.clicked.connect(PCBTableView.close)
        QtCore.QMetaObject.connectSlotsByName(PCBTableView)

    def retranslateUi(self, PCBTableView):
        _translate = QtCore.QCoreApplication.translate
        PCBTableView.setWindowTitle(_translate("PCBTableView", "PCB Table"))
        self.SeePCBPushButton.setText(_translate("PCBTableView", "See PCB"))
        self.ClosePushButton.setText(_translate("PCBTableView", "Close"))

