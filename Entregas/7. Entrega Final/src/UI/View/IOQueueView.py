from src.UI.View.baseIOQueueDesign import Ui_IoQueue


class IOQueueView(Ui_IoQueue):

    def __init__(self, dialog,controller):
        self.__controller = controller  # Es el Controlador de la Ventana
        Ui_IoQueue.__init__(self)  # Es el Template creado en el QTDesigner
        self.setupUi(dialog)
        self.SeePCBPushButton.clicked.connect(self.seePCBSelected)
        self.WaitingQueueListWidget.itemSelectionChanged.connect(self.enableButton)
        self.SeeRunningPushButton.clicked.connect(self.seeRunningPCB)

    def seePCBSelected(self):
        self.__controller.openPCBInspector(self.WaitingQueueListWidget.currentItem().data(1))

    def seeRunningPCB(self):
        self.__controller.openPCBInspectorForRunning()

    def enableButton(self):
        self.__controller.enableButton()

    def enableRunningPCBButton(self):
        self.__controller.enableRunningPCBButton()