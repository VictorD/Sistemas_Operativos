from src.UI.View.baseProcessInspectorDesign import Ui_ProcessInspector


class ProcessInspectorView(Ui_ProcessInspector):

    def __init__(self, dialog,controller):
        self.__controller = controller      # Es el Controlador de la Ventana
        Ui_ProcessInspector.__init__(self)  # Es el Template creado en el QTDesigner
        self.setupUi(dialog)