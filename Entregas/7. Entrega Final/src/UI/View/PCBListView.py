from src.UI.View.basePCBTableDesign import Ui_PCBTableView


class PCBListView(Ui_PCBTableView):

    def __init__(self, dialog,controller):
        self.__controller = controller  # Es el Controlador de la Ventana
        Ui_PCBTableView.__init__(self)  # Es el Template creado en el QTDesigner
        self.setupUi(dialog)
        self.SeePCBPushButton.clicked.connect(self.seePCBSelected)
        self.PcbListWidget.itemSelectionChanged.connect(self.enableButton)

    def seePCBSelected(self):
        self.__controller.openPCBInspector(self.PcbListWidget.currentItem().data(1))

    def enableButton(self):
        self.__controller.enableButton()