from enum import Enum


class MemoryType(Enum):

    SIMPLE  = "SIMPLE"
    PAGING  = "PAGING"
    ONDEMAND= "ONDEMAND"
