from enum import Enum


class SchedulerType(Enum):
    FIFO                    = "FIFO"
    ROUNDROBIN              = "ROUNDROBIN"
    PRIORITYNOEXPROPIATIVE  = "PRIORITYNOEXPROPIATIVE"
    PRIORITYEXPROPIATIVE    = "PRIORITYEXPROPIATIVE"
    MULTILEVELQUEUE         = "MULTILEVELQUEUE"
    MULTILEVELFEEDBACKQUEUE = "MULTILEVELFEEDBACKQUEUE"
