from src.Hardware.Hardware import HARDWARE


class LoaderSwap():

    # ---DEFINITION OF SINGLETON--- #
    __instance      = None

    def __new__(cls):
        if LoaderSwap.__instance is None:
            LoaderSwap.__instance = object.__new__(cls)
        return LoaderSwap.__instance
    # ---END OF THE DEFINITION--- #

    def writeInSwap(self, someInstructions, aFrame, aFrameSize):
        addresToWrite = aFrame * aFrameSize
        for index in range(0, len(someInstructions)):
            HARDWARE.harddisk.putinSwap(addresToWrite, someInstructions[index])
            addresToWrite += 1

    def readFromSwap(self, aFrame, aFrameSize):
        addresToRead = aFrame * aFrameSize
        instructions = []
        for index in range(0, aFrameSize):
            instruction = HARDWARE.harddisk.getfromSwap(addresToRead)
            instructions.append(instruction)
            addresToRead += 1
        return instructions

    def __repr__(self):
        return "Loader Swap"