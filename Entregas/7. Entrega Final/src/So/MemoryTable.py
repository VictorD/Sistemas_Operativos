
class MemoryTable():

    def __init__(self):
        self._table = {}

    def loadPCBAndPageTable (self, aPid, pcbPagTable):
        pcbPagTable.setPid(aPid)
        self._table[aPid]   = pcbPagTable


    def pageTableOf(self, aPidPCB):
        return self._table[aPidPCB]

    # Libera todos los frames usados por el Pid pasado y los devuelve.
    def releaseAndReturnsFrames(self, aPid):
        tableOfAPid = self._table[aPid]
        frames      = tableOfAPid.getAllFrames()
        tableOfAPid.clearFrames()
        return frames

    def getPageWithLowestTimeLoad(self):
        listOfpages = self._table.values()
        # Me quedo con una lista de page tables que tienen frames
        listOfpages = list(filter(lambda x: x.hasAnyFrame(), listOfpages))

        # Me quedo con la primera page table como victima
        victim      = listOfpages.pop(0)
        # Pido la row con menor timestamp
        victimRow   = victim.getRowWithLowestTimeLoad()

        # Itero sobre las page table restantes comparando el tiempo de carga en memoria
        # y quedandome con la que tenga el menor tiempo de carga
        for each in listOfpages:

            posibleVictimRow= each.getRowWithLowestTimeLoad()
            if victimRow._timeStampRead > posibleVictimRow._timeStampRead:
                victim = each
                victimRow = posibleVictimRow

        return (victim.pid() , victimRow)

    def getPageWithLowestAccessTime(self):
        listOfpages = self._table.values()
        # Me quedo con una lista de page tables que tienen frames
        listOfpages = list(filter(lambda x: x.hasAnyFrame(), listOfpages))

        # Me quedo con la primera page table como victima
        victim = listOfpages.pop(0)
        # Pido la row que tenga el acceso mas viejo
        victimRow = victim.getRowWithLowestAccessTime()

        # Itero sobre las page table restantes comparando los tiempos de acceso
        # y quedandome con la que tenga el menor tiempo
        for each in listOfpages:
            posibleVictimRow = each.getRowWithLowestAccessTime()
            if victimRow._timeStampEntryToMemory > posibleVictimRow._timeStampEntryToMemory:
                victim = each
                victimRow = posibleVictimRow

        return (victim.pid(), victimRow)

    def getPageWithLowestEntryTimeAndWithoutChance(self):
        listOfpages = self._table.values()
        # Me quedo con una lista de page tables que tienen frames
        listOfpageTables = list(filter(lambda x: x.hasAnyFrame(), listOfpages))

        listOfTouplesOfPIdAndPtrows = []

        for each in listOfpageTables:
            each.addAllptrowsWithFrames(listOfTouplesOfPIdAndPtrows)

        listOfTouplesOfPIdAndPtrows.sort(key=lambda x: x[1]._timeStampEntryToMemory)

        victim = None
        while(victim == None and len(listOfTouplesOfPIdAndPtrows)!=0):
            posibleVictima = listOfTouplesOfPIdAndPtrows.pop(0)
            if(posibleVictima[1].hasSecondChance()):
                posibleVictima[1].takeSecondChance()
            else:
                victim = posibleVictima

        if(victim == None):
                victim = self.getPageWithLowestEntryTimeAndWithoutChance()

        return victim

#############
    def releaseSwapFrames(self, aPid):
        self._table[aPid].clearSwapFrames()

    def setSwapFrame(self, aPid, aPageNumber):
        self._table[aPid].setFrameInSwap(aPageNumber)

    def releaseFrameOf(self, aPid, aPageNumber):
        self._table[aPid].clearFrameOf(aPageNumber)