from src.Hardware.Hardware import HARDWARE
from src.So.Scheduler.SchedulerFIFO import SchedulerFIFO


class SchedulerRoundRobin(SchedulerFIFO):

    def __init__(self, aQuantum):
        super().__init__()
        HARDWARE.timer.quantum = aQuantum

    def __repr__(self):
        return "Round Robin"