from src.So.Scheduler.AbstractScheduler import AbstractScheduler

class SchedulerPriorityNoExp(AbstractScheduler):

    def __init__(self):
        super().__init__()
        self.setupQueue()

    def add(self, aPcb):
        self._readyQueue[aPcb.priority.value -1].append(aPcb)

    def getNext(self):
        for i in self.readyQueue:
            if len(i) != 0:
                nextPcb = i.pop(0)
                self.ageAll()
                return nextPcb


    def cleanAll(self):
        self.setupQueue()

    def isEmptyReadyQueue(self):
        isEmpty = True
        for i in self.readyQueue:
            if len(i) != 0:
               isEmpty = False
        return isEmpty

    @property
    def readyQueue(self):
        return self._readyQueue

    def ageAll(self):
        for i in range(1, 4):
            if len(self.readyQueue[i]) != 0:
                pcbToAge= self.readyQueue[i].pop(0)
                self.readyQueue[i-1].append(pcbToAge)

    def setupQueue(self):
        self._readyQueue = []
        self.readyQueue.append([])
        self.readyQueue.append([])
        self.readyQueue.append([])
        self.readyQueue.append([])
        self.readyQueue.append([])

    def __repr__(self):
        return "Priority No Expropiative"

    ##Metodo para UI##
    def readyQueueListed(self):
        readyQueueListed = []
        for each in self.readyQueue:
            readyQueueListed.extend(each)
        return readyQueueListed