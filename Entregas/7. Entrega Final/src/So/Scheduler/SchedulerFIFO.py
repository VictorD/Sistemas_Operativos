from src.So.Scheduler.AbstractScheduler import AbstractScheduler


class SchedulerFIFO(AbstractScheduler):

    def __init__(self):
        super().__init__()
        self._readyQueue = []

    def add(self, aPcb):
        self.readyQueue.append(aPcb)

    def getNext(self):
       return self.readyQueue.pop(0)

    def cleanAll(self):
        self._readyQueue.clear()

    def isEmptyReadyQueue(self):
        return self._readyQueue == []

    def __repr__(self):
        return "First In, First Out"