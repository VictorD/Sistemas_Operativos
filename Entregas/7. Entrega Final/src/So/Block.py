class Block():

    def __init__(self, init, end):
        self._init = init
        self._end = end

    def canFit(self,number):
        return (self.end +1 - self.init) >= number

    @property
    def init(self):
        return self._init

    @property
    def end(self):
        return self._end
