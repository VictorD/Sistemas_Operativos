
class SecondChanceStrategy():

    def getVictim(self, aMemoryTable):
        """Retorna una Tupla (pcb.Pid, PTRow)"""
        return aMemoryTable.getPageWithLowestEntryTimeAndWithoutChance()
