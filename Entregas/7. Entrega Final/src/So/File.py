class File:

    def __init__(self, name):
        self._data  = []
        self._name  = name

    @property
    def data(self):
        return self._data

    @property
    def name(self):
        return self._name

    @data.setter
    def data(self, data):
        self._data.extend(data)

    def __repr__(self):
        return "File {name}".format(name=self.name)
