from src.So.MemoryTable import MemoryTable


class MemoryManagerPaging():

    def __init__(self, frameSize, memorySize):
        self._frameSize = frameSize
        self._freeFrames= []
        self._pageTables= MemoryTable()
        self._freeMemory= memorySize
        self.setupFrames(memorySize)

    def setupFrames(self, memorySize):
        self._freeFrames.extend(list(range(0, int(memorySize / self._frameSize))))

    def getFreeFrames(self, quantityOfFrames):
        framesToUse         = self._freeFrames[0:quantityOfFrames]
        self._freeMemory   -= quantityOfFrames * self._frameSize
        self._freeFrames    = self._freeFrames[quantityOfFrames:]
        return framesToUse

    def savePCBAndPageTable(self, aPid, pcbPagTable):
        self._pageTables.loadPCBAndPageTable(aPid, pcbPagTable)

    def getPageTableOf(self, aPidPCB):
        return self._pageTables.pageTableOf(aPidPCB)

    def releaseFrames(self, aPid):
        framesFreed         = self._pageTables.releaseAndReturnsFrames(aPid)
        self._freeMemory   += len(framesFreed) * self._frameSize
        self._freeFrames.extend(framesFreed)

    def hasMemoryFor(self, quantityOfInstructions):
        return self._freeMemory >= quantityOfInstructions

    def __repr__(self):
        return 'Memory Manager Paging'