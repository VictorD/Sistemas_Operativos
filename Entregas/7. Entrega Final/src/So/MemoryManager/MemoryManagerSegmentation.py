from src.So.Block import Block
from src.So.SegmentTable import SegmentTable


class MemoryManagerOnSegmentation():

    def __init__(self, memorySize):
        self._freeBlocks                = [Block(0,memorySize)]
        self._segmentTables                = {}

        ##self._swapManager               = SwapManagerSegmentation()

    def freeMemory(self):
        counter = 0
        for i in self._freeBlocks:
            mem = i.end + 1-i.init
            counter= counter + mem
        return counter

    def getFreeBlock(self,someInstructions):
        blocks = list(filter(lambda x: x.canFit(someInstructions), self._freeBlocks))

        if (len(blocks) != 0):
            block = blocks.pop(0)
            self._freeBlocks.remove(block)
            if (block.end+1 - block.init == someInstructions ):
                return block
            else:
                freeBlock = Block(block.init, block.init + someInstructions-1)
                oldBlock = Block(block.init + someInstructions, block.end)

                self._freeBlocks.append(oldBlock)
            return freeBlock
        else:
            pass ## hacer swap despues


        return list(filter(lambda x: x != None, blocks))

    def savePidSegmentTable(self, pid, segmentTable):
        self._segmentTables[pid] = segmentTable

    def getSegmentTableOf(self,pid):
        return self._segmentTables[pid]

    def __repr__(self):
        return 'Memory Manager for Segmentation'