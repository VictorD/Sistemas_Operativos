from src.So.MemoryManager.Swap.SwapPageTable import SwapPageTable


class SwapTable():

    def __init__(self):
        self._swaptable     = {}


    def swapIn(self, aPageNumber,aPid,aFrame):
        if not self.hasTable(aPid):
            newPageTable = SwapPageTable()
            self._swaptable[aPid] = newPageTable

        self._swaptable[aPid].newEntry(aPageNumber,aFrame)

    def swapOut(self, aPageNumber,aPid):
        frame =  self._swaptable[aPid].frameOf(aPageNumber)
        self._swaptable[aPid].deleteEntry(aPageNumber)

        return frame

    def isInSwap(self,aPid,aPageNumber):
        return self.hasTable(aPid) and self._swaptable[aPid].hasPage(aPageNumber)

    def hasTable(self, aPid):
        return aPid in self._swaptable

#######
    def deleteSwapFramesOf(self, aPid):
        swapTableOfApid= self._swaptable.pop(aPid)
        return swapTableOfApid.getAllSwapFrames()