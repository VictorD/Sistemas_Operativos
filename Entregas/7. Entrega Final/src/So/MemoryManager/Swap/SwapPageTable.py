
class SwapPageTable():

    def __init__(self):
        self._table     = {}

    def newEntry(self, aPageNumber, aFrame):
        self._table[aPageNumber] = aFrame

    def frameOf(self, aPageNumber):
        return self._table[aPageNumber]

    def deleteEntry(self, aPageNumber):
        self._table.pop(aPageNumber)

    def hasPage(self, aPageNumber):
        return aPageNumber in self._table

    def getAllSwapFrames(self):
        return list(filter(lambda x: not x is None, self._table.values()))