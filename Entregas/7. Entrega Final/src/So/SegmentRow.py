
class SegmentRow():

    def __init__(self, segment):
        self._segment      = segment
        self._block     = None
        self._hasblock  = False
        self._hasBeenSwapped = False
        self._timeStampEntryToMemory = None

    def registryBlock(self, aBlock):
        self._block = aBlock

    def setDirty(self,aTick):
        self.setTimeStampEntryToMemory(aTick)

    def setTimeStampEntryToMemory(self, aTick):
         self._timeStampEntryToMemory = aTick