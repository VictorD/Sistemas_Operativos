from src import log
from src.So.InterruptionHandlers.AbstractInterruptionHandler import AbstractInterruptionHandler


class IoOutInterruptionHandler(AbstractInterruptionHandler):

    def execute(self, irq):
        pcbReturned = self.kernel.ioDeviceController.getFinishedPCB()
        self.contextSwitchInToCPU(pcbReturned)
        log.logger.info(self.kernel.ioDeviceController)
