from src import log
from src.So.ContextSwitcher.ContextSwitch import ContextSwitch

## emulates the  Interruptions Handlers
class AbstractInterruptionHandler():

    def __init__(self, kernel):
        self._kernel    = kernel

    def execute(self, irq):
        log.logger.error("-- EXECUTE MUST BE OVERRIDEN in class {classname}".format(classname=self.__class__.__name__))

    def contextSwitchOutOfCPU(self):

        validator   = not self.kernel.scheduler.isEmptyReadyQueue()
        ContextSwitch().outOfCPUCase().switch(validator, self.kernel)

    def contextSwitchInToCPU(self, aPCB):

        # El validador sirve para identificar si hay que agregar a la ready Queue (caso False) o
        # poner a correr el proceso (caso True)
        validator = self.kernel.pcbTable.hasNotRunningPCB()

        ContextSwitch().toCPUCase().switch(validator, self.kernel, aPCB)

    def saveStateAndResetRunningPCB(self, aPCB):
        self.kernel.dispatcher.save(aPCB)
        self.kernel.pcbTable.putRunningPCBinNone()

    # ----- Getters & Setters ----- #
    @property
    def kernel(self):
        return self._kernel
