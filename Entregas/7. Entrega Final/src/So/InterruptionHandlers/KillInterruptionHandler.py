from src import log
from src.So.InterruptionHandlers.AbstractInterruptionHandler import AbstractInterruptionHandler


class KillInterruptionHandler(AbstractInterruptionHandler):

    def execute(self, irq):
        log.logger.info(" Program Finished ")

        pcbToKill   = self.kernel.pcbTable.pcbRunning
        self.saveStateAndResetRunningPCB(pcbToKill)
        pcbToKill.terminated()

        self.contextSwitchOutOfCPU()