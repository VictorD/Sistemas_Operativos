from src.So.InterruptionHandlers.AbstractInterruptionHandler import AbstractInterruptionHandler
from src.StatisticsCollector import STATISTICSCOLLECTOR


class PageFaultInterruptionHandler(AbstractInterruptionHandler):

    def execute(self, irq):
        STATISTICSCOLLECTOR.countPageFault()                                # Para UI
        numberOfPageToLoad = irq.parameters[0]
        pcbRunning = self.kernel.pcbTable.pcbRunning

        self.kernel.dispatcher.save(pcbRunning)                             # Copio la page table del mmu a la global
                                                                            # para actualizar los datos.

        frame = self.kernel._memoryManager.getFreeFrame()                   # Consigo un frame libre (Sea por que hay o
                                                                            # por que se tiene que hacer swap, y si se
                                                                            # hace hay que actualizar la tabla de la
                                                                            # victima).

        self.kernel.loader.loadPage(pcbRunning, numberOfPageToLoad, frame)  # Cargo la pagina en el frame obtenido.
        self.kernel.dispatcher.load(pcbRunning)                             # Copio la page table actualizada al mmu
