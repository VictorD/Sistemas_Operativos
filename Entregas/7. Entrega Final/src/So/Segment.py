from src.Hardware.Asm import ASM, INSTRUCTION_EXIT

## emulates a compiled program
class Segment():

    def __init__(self, instructions: object, priority: object = "READ") -> object:
        self._instructions = self.expand(instructions)
        self._priority     = priority
        self._sid = 0

    def addInstr(self, instruction):
        self.instructions.append(instruction)

    def expand(self, instructions):
        expanded = []
        for i in instructions:
            if isinstance(i, list):
                ## is a list of instructions
                expanded.extend(i)
            else:
                ## a single instr (a String)
                expanded.append(i)

        return expanded

    # ----- Getters & Setters ----- #

    @property
    def instructions(self):
        return self._instructions

    @property
    def sid(self):
        return self._sid

    @property
    def priority(self):
        return self._priority
