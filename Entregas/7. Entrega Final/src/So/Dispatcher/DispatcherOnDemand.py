from src.Hardware.Hardware import HARDWARE
from src.So.Dispatcher.DispatcherPaging import DispatcherPaging


class DispatcherOnDemand(DispatcherPaging):

    def save(self, aPcb):
        super().save(aPcb)
        currentPageTable = HARDWARE.mmu._currentProgramPaginationTable
        self._memoryManager.savePCBAndPageTable(aPcb.pid, currentPageTable)
