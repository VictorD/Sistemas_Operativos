from src.Hardware.Hardware import HARDWARE
from src.So.Dispatcher.AbstractDispatcher import AbstractDispatcher


# Has the responsability of load the state of the PCB into the hardware, and save the state of hardware into the PCB
class Dispatcher(AbstractDispatcher):

    def load(self, aPcb):
        super().load(aPcb)
        HARDWARE.mmu.baseDir = aPcb.baseAddress
        self.informRunningProcess(aPcb)
