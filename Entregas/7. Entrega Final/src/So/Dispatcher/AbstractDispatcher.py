from src import log
from src.Hardware.Hardware import HARDWARE


class AbstractDispatcher():

    def save(self, aPcb):
        aPcb.pc         = HARDWARE.cpu.pc
        HARDWARE.cpu.pc = -1

    def load(self, aPcb):
        HARDWARE.cpu.pc     = aPcb.pc
        HARDWARE.mmu.limit  = aPcb.size

    def informRunningProcess(self, aPcb):
        log.logger.info(f"Running Process: {aPcb}")