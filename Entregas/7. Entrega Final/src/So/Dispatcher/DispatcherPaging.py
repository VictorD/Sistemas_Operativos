import copy
from src.Hardware.Hardware import HARDWARE
from src.So.Dispatcher.AbstractDispatcher import AbstractDispatcher


class DispatcherPaging(AbstractDispatcher):

    def __init__(self, memoryManager):
        self._memoryManager = memoryManager

    def load(self, aPcb):
        super().load(aPcb)
        HARDWARE.mmu._currentProgramPaginationTable = copy.deepcopy(self._memoryManager.getPageTableOf(aPcb.pid))
        self.informRunningProcess(aPcb)
