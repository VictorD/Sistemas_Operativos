from src.Hardware.Asm import ASM, INSTRUCTION_EXIT


## emulates a compiled program
from src.So.Segment import Segment


class Program():

    def __init__(self, name, segments):
        self._name = name
        if self.isAListOfInstructions(segments) :
            list =[]
            segment = Segment(segments)
            segment.addInstr('EXIT')
            list.append(segment)
            segments = list

        self._segments = segments
        self._sidCounter = 0
        self.expand(segments)

    def newWithSegments(self, instruction):
        self.instructions.append(instruction)

    def addInstr(self, instruction):
        self.instructions.append(instruction)

    def expand(self, segments):
        for i in segments:
            i._sid= self._sidCounter
            self._sidCounter = self._sidCounter+1

    # ----- Getters & Setters ----- #
    @property
    def name(self):
        return self._name

    @property
    def instructions(self):
        instructions = []
        for i in self._segments:
            instructions.extend(i._instructions)
        return instructions

    @property
    def data(self):
        return self.instructions

    def __repr__(self):
        return "Program({name}, {instructions})".format(name=self.name, instructions=self.instructions)

    def isAListOfInstructions(self, segments):
       return list(filter(lambda x: isinstance(x, Segment), segments)) == []
