from src.So.Switch.SwitchIn import SwitchIn


class SwitchInPriorityExpropiative(SwitchIn):

    # ---DEFINITION OF SINGLETON--- #
    __instance      = None

    def __new__(cls):
        if SwitchInPriorityExpropiative.__instance is None:
            SwitchInPriorityExpropiative.__instance = object.__new__(cls)
        return SwitchInPriorityExpropiative.__instance
    # ---END OF THE DEFINITION--- #

    def addInReadyQueue(self, kernel, aPCB):
        aPCB.ready()
        currentlyRunning = kernel.pcbTable.pcbRunning
        kernel.dispatcher.save(currentlyRunning)
        currentlyRunning.ready()
        pcbToCpu = kernel.scheduler.addAndReturn(aPCB, currentlyRunning)
        self.loadToRun(kernel, pcbToCpu)

    def __repr__(self):
        return "Priority Expropiative SwitchIn"