from src.Hardware.Hardware import HARDWARE


class AbstractSwitchMultilevelQueue():

    def resolveTimerFor(self, aPCB):
        if aPCB.hasLowLevelPriority():
            HARDWARE.timer.putOffIfNeed()
        else:
            HARDWARE.timer.resetTimeOutCounterAndPutOnIfNeed()
