from src.So.Switch.Switch import Switch


class SwitchOut(Switch):

    # ---DEFINITION OF SINGLETON--- #
    __instance      = None

    def __new__(cls):
        if SwitchOut.__instance is None:
            SwitchOut.__instance = object.__new__(cls)
        return SwitchOut.__instance
    # ---END OF THE DEFINITION--- #

    def addInReadyQueue(self, kernel, aPCB=None):
        pass

    def loadToRun(self, kernel, aPCB=None):
        nextPcb = aPCB
        if aPCB is None:
            nextPcb = kernel.scheduler.getNext()
        super().loadToRun(kernel, nextPcb)

    def __repr__(self):
        return "Switch Out"