from src.Hardware.Hardware import HARDWARE
from src.So.Switch.SwitchIn import SwitchIn


class SwitchInWithTimer(SwitchIn):

    # ---DEFINITION OF SINGLETON--- #
    __instance = None

    def __new__(cls):
        if SwitchInWithTimer.__instance is None:
            SwitchInWithTimer.__instance = object.__new__(cls)
        return SwitchInWithTimer.__instance

    # ---END OF THE DEFINITION--- #

    def loadToRun(self, kernel, aPCB):
        HARDWARE.timer.resetTimeOutCounterIfNeed()
        super().loadToRun(kernel, aPCB)
