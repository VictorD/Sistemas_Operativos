from abc import abstractmethod


class Switch():

    def __init__(self):
        self._cases = {False: self.addInReadyQueue, True: self.loadToRun}

    def switch(self, funcionValue, kernel, apcb=None):
        # Si recibe un False, ya hay alguien corriendo y lo tiene que agregar a la lista ready
        # Si recibe un True, la CPU esta idle y puede ponerlo a correr
        self._cases[funcionValue](kernel, apcb)

    @abstractmethod
    def addInReadyQueue(self, kernel, aPCB):
        raise NotImplementedError

    def loadToRun(self, kernel, aPCB):
        aPCB.running()
        kernel.pcbTable.pcbRunning = aPCB
        kernel.dispatcher.load(aPCB)

