##  These are the interrupts supported by our kernel
OUT_OF_MEMORY_INTERRUPTION_TYPE = "#OUTOFMEMORY"
KILL_INTERRUPTION_TYPE          = "#KILL"
IO_IN_INTERRUPTION_TYPE         = "#IO_IN"
IO_OUT_INTERRUPTION_TYPE        = "#IO_OUT"
NEW_INTERRUPTION_TYPE           = "#NEW"
SHUTDOWN_INTERRUPTION_TYPE      = "#SHUTDOWN"
TIMEOUT_INTERRUPTION_TYPE       = "#TIME_OUT"
PAGE_FAULT_INTERRUPTION_TYPE    = "#PAGE_FAULT"
SET_IN_INTERRUPTION_TYPE = "#SET"

## emulates an Interrupt request
class IRQ:

    def __init__(self, type, parameters = None):
        self._type = type
        self._parameters = parameters

    @property
    def parameters(self):
        return self._parameters

    @property
    def type(self):
        return self._type
