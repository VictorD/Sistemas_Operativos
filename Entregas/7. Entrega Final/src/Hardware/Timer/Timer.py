from src import log
from src.Hardware.Timer.AbstractTimer import AbstractTimer
from src.Hardware.Irq import IRQ, TIMEOUT_INTERRUPTION_TYPE


class Timer(AbstractTimer):

    def __init__(self, aCpu, aQuantum=5):
        self._timeOutCounter= 0
        self._quantum       = aQuantum
        self._cpu           = aCpu
        self.__on            = False

    def tick(self, tickNbr):
        if self.__on:
            if self._timeOutCounter < self._quantum:
                self._timeOutCounter   += 1
            else:
                log.logger.info("¡TIME OUT!")
                timeOutIRQ = IRQ(TIMEOUT_INTERRUPTION_TYPE)
                self._cpu._interruptVector.handle(timeOutIRQ)

    def setOff(self):
        self.__on           = False
        self._timeOutCounter= 0

    def setOn(self):
        self.__on           = True
        self._timeOutCounter= 0

    def resetTimeOutCounterIfNeed(self):
        if self.__on:
            self.resetTimer()

    def resetTimer(self):
        self._timeOutCounter = 0
        # log.logger.info('¡Timer Reset!')

    def resetTimeOutCounterAndPutOnIfNeed(self):
        if self.__on:
            self.resetTimer()
        else:
            self.setOn()
            log.logger.info('¡Timer ON!')

    def putOffIfNeed(self):
        if self.__on:
            self.setOff()
            log.logger.info('¡Timer OFF!')

    # ----Getters & Setters--- #
    @property
    def quantum(self):
        return self._quantum

    @quantum.setter
    def quantum(self, aInt):
        self._quantum   = aInt
