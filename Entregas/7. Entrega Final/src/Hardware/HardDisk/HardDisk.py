from src.Hardware.Memory import Memory


class HardDisk():

    def __init__(self):
            self._fileList = {}
            self._swap = Memory(200)

    @property
    def fileList(self):
        return self._fileList

    def fetchProgram(self, pointer):
        return  self.fileList[pointer]

    def getPointer(self):
        return len(self._fileList.keys()) + 1

    def saveProgram(self, pointer, aFile):
        self.fileList[pointer] = aFile
