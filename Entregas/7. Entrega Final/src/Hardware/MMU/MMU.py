from src.Hardware.MMU.AbstractMMU import AbstractMMU

## emulates the Memory Management Unit (MMU)
class MMU(AbstractMMU):

    def __init__(self, memory):
        super().__init__(memory)
        self._baseDir = 0

    @property
    def baseDir(self):
        return self._baseDir

    @baseDir.setter
    def baseDir(self, baseDir):
        self._baseDir = baseDir

    def calculateAddress(self, anLogicalAddress):
        return anLogicalAddress + self._baseDir

    def __repr__(self):
        return "MMU Simple"