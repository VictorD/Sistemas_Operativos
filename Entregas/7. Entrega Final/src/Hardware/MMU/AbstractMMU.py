from abc import abstractmethod


class AbstractMMU():

    def __init__(self, memory):
        self._memory = memory
        self._limit = 999

    @property
    def limit(self):
        return self._limit

    @limit.setter
    def limit(self, limit):
        self._limit = limit

    @abstractmethod
    def calculateAddress(self, anLogicalAddress):
        raise NotImplementedError("To be implemented")

    def fetch(self,  logicalAddress):
        if (logicalAddress >= self._limit):
            raise Exception("Invalid Address,  {logicalAddress} is eq or higher than process limit: {limit}".format(limit = self._limit, logicalAddress = logicalAddress))
        else:
            physicalAddress = self.calculateAddress(logicalAddress)
            return self._memory.get(physicalAddress)