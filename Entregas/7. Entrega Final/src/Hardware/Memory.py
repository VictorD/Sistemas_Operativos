##from tabulate import tabulate

## emulates the main memory (RAM)
class Memory():

    def __init__(self, size):
        self._cells = [''] * size

    def put(self, addr, value):
        self._cells[addr] = value

    def get(self, addr):
        return self._cells[addr]

    def cleanAll(self):
        self._cells.clear()

    def __repr__(self):
        return ""
        ##return tabulate(enumerate(self._cells), tablefmt='psql')
