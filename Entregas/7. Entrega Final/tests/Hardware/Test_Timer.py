from unittest import TestCase
from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.Hardware.Timer.Timer import Timer
from src.So.ContextSwitcher.ContextSwitch import ContextSwitch
from src.So.Kernel import Kernel
from src.So.Program import Program
from src.So.Scheduler.SchedulerRoundRobin import SchedulerRoundRobin
from src.So.Switch.WithTimer.SwitchInWithTimer import SwitchInWithTimer
from src.So.Switch.WithTimer.SwitchOutWithTimer import SwitchOutWithTimer


class TestTimer(TestCase):

    def setUp(self):
        HARDWARE.setup(55)
        self.kernel = Kernel()
        HARDWARE.timer = Timer(HARDWARE.cpu, 3)
        HARDWARE.clock._subscribers[0] = HARDWARE.timer
        self.kernel.scheduler = SchedulerRoundRobin(3)
        ContextSwitch().setSwitchOut(SwitchOutWithTimer())
        ContextSwitch().setSwitchIn(SwitchInWithTimer())

        self.sutTimer           = HARDWARE.timer
        self.sutTimer.setOn()
        self.program            = Program("Doggynator.dog", [ASM.CPU(3), ASM.IO()])
        HARDWARE.harddisk.newDirectory("C:/O'Doggor")
        HARDWARE.harddisk.saveProgram(self.program, "C:/O'Doggor")
        self.program            = Program("D-dog.dog", [ASM.CPU(1), ASM.IO(),ASM.CPU(2)])
        HARDWARE.harddisk.newDirectory("C:/bone")
        HARDWARE.harddisk.saveProgram(self.program, "C:/bone")

    def test_when_a_clock_tick_is_done_with_the_timer_on_and_the_TimeOutCounter_is_less_than_the_quantum_the_cpu_does_a_tick(self):
        # Setup
        self.kernel.execute("C:/O'Doggor/Doggynator.dog")

        # Exercise
        HARDWARE.clock.do_ticks(1)

        # Test
        self.assertEqual(1, HARDWARE.cpu.pc)
        self.assertEqual(1, self.sutTimer._timeOutCounter)

    def test_when_a_clock_tick_is_done_with_the_timer_on_and_the_TimeOutCounter_is_greater_than_the_quantum_a_timeout_interrupt_occurs(self):
        # Setup
        self.kernel.execute("C:/O'Doggor/Doggynator.dog")
        self.kernel.execute("C:/bone/D-dog.dog")

        # Exercise
        HARDWARE.clock.do_ticks(4)

        # Test
        self.assertEqual(1, HARDWARE.cpu.pc)
        self.assertEqual(1, self.sutTimer._timeOutCounter)
        self.assertEqual(5, HARDWARE.mmu.baseDir)
        self.assertEqual(1, self.kernel.pcbTable.pcbRunning.pid)
        self.assertEqual(1, len(self.kernel.scheduler.readyQueue))
        self.assertEqual(0, self.kernel.scheduler.readyQueue[0].pid)

    def test_when_a_clock_tick_is_done_with_the_timer_off_and_the_TimeOutCounter_is_greater_than_the_quantum_nothing_occurs(self):
        # Setup
        self.kernel.execute("C:/O'Doggor/Doggynator.dog")
        self.kernel.execute("C:/bone/D-dog.dog")
        self.sutTimer.setOff()

        # Exercise
        HARDWARE.clock.do_ticks(3)

        # Test
        self.assertEqual(3, HARDWARE.cpu.pc)
        self.assertEqual(0, self.sutTimer._timeOutCounter)
        self.assertEqual(0, HARDWARE.mmu.baseDir)
        self.assertEqual(0, self.kernel.pcbTable.pcbRunning.pid)
        self.assertEqual(1, self.kernel.scheduler.readyQueue[0].pid)
