from unittest import TestCase

from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.Hardware.MMU.MMUOnDemand import MMUOnDemand
from src.So.Kernel import Kernel
from src.So.Page import Page
from src.So.PageTable import PageTable
from src.So.Pcb.Pcb import PCB
from src.So.Program import Program
from src.So.victimStrategy.FIFOStrategy import FIFOStrategy
from src.SoBuilder.SoBuilder import SoBuilder


class TestMMUonDemand(TestCase):

    def setUp(self):
        HARDWARE.setup(50)

        # akernel._dispatcher = DispatcherPaging(akernel._memoryManager)
        # HARDWARE._mmu = MMUOnDemand(aFrameSize, HARDWARE.memory)
        # HARDWARE.cpu._mmu = HARDWARE.mmu

        self.mmuSut                                 = MMUOnDemand(4, HARDWARE.memory)
        self.pft                                    = PageTable()
        self.mmuSut._currentProgramPaginationTable  = self.pft
        self.page1 = Page()
        self.page1._number = 0
        self.page2 = Page()
        self.page2._number = 1
        self.page3 = Page()
        self.page3._number = 2
        self.page4 = Page()
        self.page4._number = 3

    def test_when_a_MMU_fetch_With_a_PC_Equals_to_0_and_the_corresponding_page_has_no_frame_raises_a_page_fault_and_the_page_is_loaded(self):
        # Setup
        akernel                     = Kernel()
        SoBuilder.withPagingOnDemand(akernel,4, FIFOStrategy())
        HARDWARE._mmu               = self.mmuSut
        HARDWARE.cpu._mmu           = self.mmuSut

        programTest1                = Program("Test.dog", [ASM.CPU(12)])
        HARDWARE.harddisk.newDirectory("C:/perrugrama")
        HARDWARE.harddisk.saveProgram(programTest1, "C:/perrugrama")
        pcbProgram1                 = PCB(0, "C:/perrugrama/Test.dog")

        akernel.pcbTable.pcbRunning = pcbProgram1
        akernel._memoryManager.savePCBAndPageTable(0, self.pft)
        self.pft.setPage(self.page1)
        self.pft.setPage(self.page2)
        self.pft.setPage(self.page3)
        # Exercise
        self.mmuSut.calculateAddress(0)
        # Test
        self.assertIsNotNone(self.mmuSut._currentProgramPaginationTable.frameOf(0))

    def test_A_MMU_with_a_frame_size_of_4_if_fetch_with_a_PC_equals_to_5_got_5(self):
        # Setup
        self.pft.setPage(self.page1)
        self.pft.setPage(self.page2)
        self.pft.assocFrames([0, 1])

        # Exercise
        addressToRead   = self.mmuSut.calculateAddress(5)

        # Test
        self.assertEqual(5, addressToRead)

    def test_A_MMU_with_a_frame_size_of_5_and_a_page_table_with_the_frame_1_if_fetch_with_a_PC_equals_to_0_got_5(self):
        # Setup
        self.pft.setPage(self.page1)
        self.pft.assocFrames([1])
        self.mmuSut._frameSize  = 5

        # Exercise
        addressToRead       = self.mmuSut.calculateAddress(0)

        # Test
        self.assertEqual(5, addressToRead)

    def test_A_MMU_with_a_pagination_table_of_4_pages_with_the_assigned_frames_are_5_2_1_3_and_frame_size_of_4_if_fetch_every_pc_got_the_correct_direction(self):
        # Setup
        self.pft.setPage(self.page1)
        self.pft.setPage(self.page2)
        self.pft.setPage(self.page3)
        self.pft.setPage(self.page4)
        self.pft.assocFrames([5, 2, 1, 3])

        # Test
        self.assertEqual(20, self.mmuSut.calculateAddress(0))
        self.assertEqual(21, self.mmuSut.calculateAddress(1))
        self.assertEqual(22, self.mmuSut.calculateAddress(2))
        self.assertEqual(23, self.mmuSut.calculateAddress(3))
        self.assertEqual(8, self.mmuSut.calculateAddress(4))
        self.assertEqual(9, self.mmuSut.calculateAddress(5))
        self.assertEqual(10, self.mmuSut.calculateAddress(6))
        self.assertEqual(11, self.mmuSut.calculateAddress(7))
        self.assertEqual(4, self.mmuSut.calculateAddress(8))
        self.assertEqual(5, self.mmuSut.calculateAddress(9))
        self.assertEqual(6, self.mmuSut.calculateAddress(10))
        self.assertEqual(7, self.mmuSut.calculateAddress(11))
        self.assertEqual(12, self.mmuSut.calculateAddress(12))
        self.assertEqual(13, self.mmuSut.calculateAddress(13))
        self.assertEqual(14, self.mmuSut.calculateAddress(14))
        self.assertEqual(15, self.mmuSut.calculateAddress(15))


## y se marca to do tipo de flags para los algoritmos de seleccion de victima
## FIFO: Usa el timestamp de cuando entro a memoria si no estaba dirty
## LRU : Usa el timestamp de cuando fue la ultima vez que se uso
## Second Chance: cada vez que lo lee si esta dirty le da la segunda vida
    def test_when_a_page_is_calculated_to_be_Read_by_the_mmu_then_that_page_is_marked_as_dirty_and_the_tick_is_saved_as_a_timestamp_of_when_it_entered_memory_and_when_was_It_the_lastTimeUsed(self):
        self.pft.setPage(self.page1)
        self.pft.setPage(self.page2)
        self.pft.setPage(self.page3)
        self.pft.setPage(self.page4)
        self.pft.assocFrames([5, 2, 1, 3])

        ptRow = self.mmuSut._currentProgramPaginationTable._table[self.page1._number]

        isDirty = ptRow._dirty
        tickNumber = ptRow._timeStampEntryToMemory
        self.assertFalse(isDirty)
        self.assertIsNone(tickNumber)

        self.mmuSut.calculateAddress(0)

        isDirty    = ptRow._dirty
        tickNumber = ptRow._timeStampEntryToMemory
        self.assertTrue(isDirty)
        self.assertIsNotNone(tickNumber)
        self.assertEqual(tickNumber,0)