from unittest import TestCase
from src.So.Pcb.Pcb import PCB
from src.So.Scheduler.SchedulerFIFO import SchedulerFIFO


class TestSchedulerFIFO(TestCase):

    def setUp(self):
        self.schedulerFIFO = SchedulerFIFO()
        self.pcb = PCB(1, "C:/huesohueso/bitcoinminner.dog")
        self.pcb2 = PCB(2, "C:/huesohueso/troyano.dog")
        self.pcb3 = PCB(3, "C:/huesohueso/keylogger.dog")


    def test_when_a_Scheduler_is_Created_its_ready_queue_is_empty(self):
         self.assertEqual(0, len(self.schedulerFIFO.readyQueue))

    def test_should_a_process_be_handed_to_the_scheduler_and_there_is_none_in_the_queue_it_goes_to_the_first_position_of_the_ready_queue(self):
        #Exercise
        self.schedulerFIFO.add(self.pcb)

        #Assert
        self.assertEqual(self.pcb,self.schedulerFIFO.readyQueue[0])

    def test_should_a_process_be_handed_to_the_scheduler_and_there_is_already_one_in_the_queue_it_goes_to_the_second_position_of_the_ready_queue(self):
        # Exercise
        self.schedulerFIFO.add(self.pcb)
        self.schedulerFIFO.add(self.pcb2)

        # Assert
        self.assertEqual(self.pcb2, self.schedulerFIFO.readyQueue[1])

    def test_should_a_process_be_handed_to_the_scheduler_and_there_is_are_two_in_the_queue_it_goes_to_the_third_position_of_the_ready_queue(self):
        # Exercise
        self.schedulerFIFO.add(self.pcb)
        self.schedulerFIFO.add(self.pcb2)
        self.schedulerFIFO.add(self.pcb3)

        # Assert
        self.assertEqual(self.pcb3, self.schedulerFIFO.readyQueue[2])

    def test_should_the_scheduler_be_indicated_to_get_next_it_picks_the_first_process_on_the_ready_queue(self):
        self.schedulerFIFO.add(self.pcb)
        self.schedulerFIFO.add(self.pcb2)
        self.schedulerFIFO.add(self.pcb3)

        firstPCB = self.schedulerFIFO.getNext()

        # Assert
        self.assertEqual(self.pcb, firstPCB)

