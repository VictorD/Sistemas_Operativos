from unittest import TestCase

from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.Hardware.Timer.Timer import Timer
from src.So.ContextSwitcher.ContextSwitch import ContextSwitch
from src.So.Kernel import Kernel
from src.So.Program import Program
from src.So.Switch.WithTimer.SwitchInWithTimer import SwitchInWithTimer
from src.So.Switch.WithTimer.SwitchOutWithTimer import SwitchOutWithTimer
from src.So.Scheduler.SchedulerRoundRobin import SchedulerRoundRobin


class TestSchedulerMultilevelQueue(TestCase):

    def setUp(self):
        HARDWARE.setup(50)
        self.kernel     = Kernel()
        HARDWARE.timer = Timer(HARDWARE.cpu, 2)
        HARDWARE.clock._subscribers[0]  = HARDWARE.timer
        self.kernel.scheduler             = SchedulerRoundRobin(2)
        HARDWARE.timer.setOn()
        ContextSwitch().setSwitchOut(SwitchOutWithTimer())
        ContextSwitch().setSwitchIn(SwitchInWithTimer())

        self.program    = Program("Dogma.dog", [ASM.CPU(1)])
        HARDWARE.harddisk.newDirectory("C:/dogminator")
        HARDWARE.harddisk.saveProgram(self.program, "C:/dogminator")

        self.program1 = Program("CatDog.dog", [ASM.CPU(10)])
        HARDWARE.harddisk.newDirectory("C:/kittys")
        HARDWARE.harddisk.saveProgram(self.program1, "C:/kittys")

    def test_with_an_CPU_has_next_instruction_to_run_is_exit_load_the_next_process(self):
        # Setup
        self.kernel.execute("C:/dogminator/Dogma.dog")
        self.kernel.execute("C:/kittys/CatDog.dog")

        # Exercise
        HARDWARE.clock.do_ticks(2)


        # Test
        self.assertEqual(1, self.kernel.pcbTable.pcbRunning.pid)
        self.assertEqual("C:/kittys/CatDog.dog", self.kernel.pcbTable.pcbRunning.direction)
        self.assertEqual(0, HARDWARE.cpu.pc)
        self.assertEqual(0, HARDWARE.timer._timeOutCounter)

    def test_when_a_process_is_expropriated_for_a_timeOut_the_next_program_runs_until_its_finished_and_the_expropriated_program_goes_to_run(self):
        # Setup
        self.kernel.execute("C:/kittys/CatDog.dog")
        self.kernel.execute("C:/dogminator/Dogma.dog")

        # Exercise
        HARDWARE.clock.do_ticks(3)

        # Test, ocurre una excepcion de TIME OUT donde se saca al PCB running actual de cpu y se pone al siguienta antes de
        # terminar el ciclo de reloj para que el siguiente PCB a correr pueda ejecutar su instruccion, el timer para esta ocasion
        # se setea en 1
        self.assertEqual(1, self.kernel.pcbTable.pcbRunning.pid)
        self.assertEqual("C:/dogminator/Dogma.dog", self.kernel.pcbTable.pcbRunning.direction)
        self.assertEqual(1, HARDWARE.cpu.pc)
        self.assertEqual(1, HARDWARE.timer._timeOutCounter)

        # Exercise
        HARDWARE.clock.do_ticks(1)

        # Test, el PCB que corre termina y entra el proximo a ejecutarse, su PC counter en este caso es  2 (Habia ejecutado
        # 2 instrucciones antes de que lo expropien, y el timer esta 0
        self.assertEqual(0, self.kernel.pcbTable.pcbRunning.pid)
        self.assertEqual("C:/kittys/CatDog.dog", self.kernel.pcbTable.pcbRunning.direction)
        self.assertEqual(2, HARDWARE.cpu.pc)
        self.assertEqual(0, HARDWARE.timer._timeOutCounter)

        # Exercise
        HARDWARE.clock.do_ticks(2)

        # Test
        self.assertEqual(0, self.kernel.pcbTable.pcbRunning.pid)
        self.assertEqual("C:/kittys/CatDog.dog", self.kernel.pcbTable.pcbRunning.direction)
        self.assertEqual(4, HARDWARE.cpu.pc)
        self.assertEqual(2, HARDWARE.timer._timeOutCounter)