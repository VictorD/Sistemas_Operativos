from unittest import TestCase

from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.So.Kernel import Kernel
from src.So.Pcb.PcbState import PcbState
from src.So.Program import Program


class TestKillInterruptionHandler(TestCase):

    def setUp(self):
        HARDWARE.setup(50)
        self.kernel    = Kernel()
        self.program   = Program("It's a Virus! Reworked.exe", [ASM.CPU(1)])
        HARDWARE.harddisk.newDirectory("C:/virus")
        HARDWARE.harddisk.saveProgram(self.program, "C:/virus")
        self.program1  = Program("Really? another virus! Remastered.exe", [ASM.CPU(4)])
        HARDWARE.harddisk.newDirectory("C:/fake")
        HARDWARE.harddisk.saveProgram(self.program1, "C:/fake")

    def test_when_a_process_terminates_and_the_ready_Queue_is_empty_the_Cpu_is_idle(self):
        # Setup
        self.kernel.execute("C:/virus/It's a Virus! Reworked.exe")

        # Exercise
        HARDWARE.clock.do_ticks(2)

        # Test
        self.assertTrue(self.kernel.isCPUIdle())
        self.assertTrue(self.kernel.scheduler.readyQueue == [])
        self.assertEqual(None, self.kernel.pcbTable.pcbRunning)
        self.assertEqual(PcbState.TERMINATED, self.kernel.pcbTable._pcbTable[0].state)
        self.assertEqual(0, self.kernel.pcbTable._pcbTable[0].pid)

    def test_when_a_process_terminates_and_the_ready_Queue_has_one_process_the_Cpu_is_busy(self):
        # Setup
        self.kernel.execute("C:/virus/It's a Virus! Reworked.exe")
        self.kernel.execute("C:/fake/Really? another virus! Remastered.exe")

        # Exercise
        HARDWARE.clock.do_ticks(2)

        # Test
        self.assertFalse(self.kernel.isCPUIdle())
        self.assertTrue(self.kernel.scheduler.readyQueue == [])
        self.assertNotEqual(None, self.kernel.pcbTable.pcbRunning)
        self.assertEqual(1, self.kernel.pcbTable.pcbRunning.pid)
        self.assertEqual(PcbState.RUNNING, self.kernel.pcbTable.pcbRunning.state)
        self.assertEqual(PcbState.TERMINATED, self.kernel.pcbTable._pcbTable[0].state)
        self.assertEqual(0, self.kernel.pcbTable._pcbTable[0].pid)