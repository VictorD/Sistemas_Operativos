from unittest import TestCase
from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.So.Kernel import Kernel
from src.So.Pcb.PcbState import PcbState
from src.So.Program import Program


class TestIoInInterruptionHandler(TestCase):

    def setUp(self):
        HARDWARE.setup(50)
        self.kernel     = Kernel()
        self.program    = Program("It's a Virus!.exe", [ASM.CPU(1), ASM.IO()])
        HARDWARE.harddisk.newDirectory("C:/virus")
        HARDWARE.harddisk.saveProgram(self.program, "C:/virus")

        self.program1   = Program("Really? another virus!.exe", [ASM.CPU(4)])
        HARDWARE.harddisk.newDirectory("C:/fake")
        HARDWARE.harddisk.saveProgram(self.program1, "C:/fake")

        self.program2   = Program("Flash.exe", [ASM.CPU(1)])
        HARDWARE.harddisk.newDirectory("C:/bios")
        HARDWARE.harddisk.saveProgram(self.program2, "C:/bios")

        self.program3   = Program("The System Breaker.exe", [ASM.IO()])
        HARDWARE.harddisk.newDirectory("C:/break")
        HARDWARE.harddisk.saveProgram(self.program3, "C:/break")

    def test_when_a_program_goes_to_do_IO_and_the_Ready_Queue_Is_Empty_the_Cpu_should_be_idle(self):
        # Setup
        # Exercise
        self.kernel.execute("C:/virus/It's a Virus!.exe")
        HARDWARE.clock.do_ticks(2)

        # Test
        self.assertTrue(self.kernel.isCPUIdle())
        self.assertTrue(self.kernel.scheduler.readyQueue == [])
        self.assertTrue(HARDWARE.ioDevice.is_busy)
        self.assertEqual(0, self.kernel.ioDeviceController._currentPCB.pid)
        self.assertEqual(PcbState.WAITING, self.kernel.ioDeviceController._currentPCB.state)
        self.assertEqual([], self.kernel.ioDeviceController._waiting_queue)

    def test_when_a_program_goes_to_do_IO_and_the_Ready_Queue_Is_Not_Empty_the_Cpu_should_be_busy(self):
        # Setup
        # Exercise
        self.kernel.execute("C:/virus/It's a Virus!.exe")
        self.kernel.execute("C:/fake/Really? another virus!.exe")
        HARDWARE.clock.do_ticks(2)

        # Test
        self.assertFalse(self.kernel.isCPUIdle())
        self.assertEqual(1, self.kernel.pcbTable.pcbRunning.pid)
        self.assertTrue(self.kernel.scheduler.readyQueue == [])
        self.assertTrue(HARDWARE.ioDevice.is_busy)
        self.assertEqual(0, self.kernel.ioDeviceController._currentPCB.pid)
        self.assertEqual([], self.kernel.ioDeviceController._waiting_queue)

    def test_when_a_program_goes_to_do_IO_and_the_Ready_Queue_has_two_processes_the_Cpu_should_be_busy_and_the_ready_Queue_should_be_empty(self):
        # Setup
        # Exercise
        self.kernel.execute("C:/virus/It's a Virus!.exe")
        self.kernel.execute("C:/fake/Really? another virus!.exe")
        self.kernel.execute("C:/bios/Flash.exe")
        HARDWARE.clock.do_ticks(2)

        # Test
        self.assertFalse(self.kernel.isCPUIdle())
        self.assertEqual(1, self.kernel.pcbTable.pcbRunning.pid)
        self.assertEqual(1, len(self.kernel.scheduler.readyQueue))
        self.assertTrue(HARDWARE.ioDevice.is_busy)
        self.assertEqual(0, self.kernel.ioDeviceController._currentPCB.pid)
        self.assertEqual([], self.kernel.ioDeviceController._waiting_queue)

    def test_when_two_programs_go_to_do_IO_and_the_Ready_Queue_has_two_processes_the_Cpu_should_be_busy_the_ready_Queue_should_not_be_empty_and_the_waiting_queue_of_io_should_not_be_empty(
            self):
        # Setup
        # Exercise
        self.kernel.execute("C:/virus/It's a Virus!.exe")
        self.kernel.execute("C:/break/The System Breaker.exe")
        self.kernel.execute("C:/fake/Really? another virus!.exe")
        self.kernel.execute("C:/bios/Flash.exe")
        HARDWARE.clock.do_ticks(3)

        # Test
        self.assertFalse(self.kernel.isCPUIdle())
        self.assertTrue(HARDWARE.ioDevice.is_busy)

        self.assertEqual(2, self.kernel.pcbTable.pcbRunning.pid)
        self.assertEqual(0, self.kernel.ioDeviceController._currentPCB.pid)
        self.assertEqual(PcbState.WAITING, self.kernel.ioDeviceController._waiting_queue[0]['pcb'].state)

        self.assertEqual(1, len(self.kernel.ioDeviceController._waiting_queue))
        self.assertEqual(1, len(self.kernel.scheduler.readyQueue))


