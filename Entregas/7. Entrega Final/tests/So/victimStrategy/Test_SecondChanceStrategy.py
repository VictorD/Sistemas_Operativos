from unittest import TestCase
from src.So.MemoryTable import MemoryTable
from src.So.Page import Page
from src.So.PageTable import PageTable
from src.So.victimStrategy.LRUStrategy import LRUStrategy
from src.So.victimStrategy.SecondChanceStrategy import SecondChanceStrategy


class TestLRUStrategy(TestCase):

    def setUp(self):
        self.secondChancetrategySut = SecondChanceStrategy()
        self.memoryTable    = MemoryTable()
        self.pageTablePid0  = self.setupPageTableOfPid0()
        self.pageTablePid1  = self.setupPageTableOfPid1()
        self.pageTablePid2  = self.setupPageTableOfPid2()
        self.pageTablePid3  = self.setupPageTableOfPid3()
        self.memoryTable.loadPCBAndPageTable(0, self.pageTablePid0)
        self.memoryTable.loadPCBAndPageTable(1, self.pageTablePid1)
        self.memoryTable.loadPCBAndPageTable(2, self.pageTablePid2)
        self.memoryTable.loadPCBAndPageTable(3, self.pageTablePid3)

    def test_when_asked_for_the_Second_chance_victim_it_fetchs_the_lower_entry_page_if_it_has_a_chance_it_takes_it_away_and_ask_for_the_next_until_it_finds_a_victim_without_a_second_chance(self):
        # Setup
        # Exercise
        self.assertTrue(self.pageTablePid0._table[1]._secondChance)
        self.assertTrue(self.pageTablePid3._table[0]._secondChance)

        victimSecondChance = self.secondChancetrategySut.getVictim(self.memoryTable)
        # Test
        self.assertFalse(self.pageTablePid0._table[1]._secondChance)
        self.assertFalse(self.pageTablePid3._table[0]._secondChance)

        self.assertEqual(1, victimSecondChance[0])
        self.assertEqual(2, victimSecondChance[1]._frame)

    def test_when_asked_for_the_Second_chance_victim_it_fetchs_the_lower_entry_page_if_it_has_a_chance_it_takes_it_away_and_ask_for_the_next_until_it_finds_a_victim_without_a_second_chance_if_it_does_not_find_it_it_starts_again(
            self):
        # Setup

        self.pageTablePid1.setSecondChance(0)
        self.pageTablePid0.setSecondChance(0)

        # Exercise
        self.assertTrue(self.pageTablePid1._table[0]._secondChance)
        self.assertTrue(self.pageTablePid0._table[0]._secondChance)
        self.assertTrue(self.pageTablePid0._table[1]._secondChance)
        self.assertTrue(self.pageTablePid3._table[0]._secondChance)

        victimSecondChance = self.secondChancetrategySut.getVictim(self.memoryTable)
        # Test
        self.assertFalse(self.pageTablePid1._table[0]._secondChance)
        self.assertFalse(self.pageTablePid0._table[0]._secondChance)
        self.assertFalse(self.pageTablePid0._table[1]._secondChance)
        self.assertFalse(self.pageTablePid3._table[0]._secondChance)

        self.assertEqual(3, victimSecondChance[0])
        self.assertEqual(12, victimSecondChance[1]._frame)


    ##[-----Setup Methods-----]##
    def setupPageTableOfPid2(self):
        pageTablePid2 = PageTable()
        page1Pid2 = Page()
        page1Pid2._number = 0
        pageTablePid2.assocPages([page1Pid2])
        return pageTablePid2

    def setupPageTableOfPid1(self):
        pageTablePid1       = PageTable()
        page1Pid1           = Page()
        page1Pid1._number   = 0
        page2Pid1           = Page()
        page2Pid1._number   = 1
        pageTablePid1.assocPages([page1Pid1, page2Pid1])
        pageTablePid1.assocFrames([2])
        pageTablePid1.setDirty(0, 1)
        pageTablePid1.setEntryTimeStamp(0, 14)
        return pageTablePid1

    def setupPageTableOfPid0(self):
        pageTablePid0       = PageTable()
        page1Pid0          = Page()
        page1Pid0._number  = 0
        page2Pid0          = Page()
        page2Pid0._number  = 1
        page3Pid0          = Page()
        page3Pid0._number  = 2
        page4Pid0          = Page()
        page4Pid0._number  = 3
        pageTablePid0.assocPages([page1Pid0, page2Pid0, page3Pid0, page4Pid0])
        pageTablePid0.assocFrames([4, 0])
        pageTablePid0.setDirty(0, 5)
        pageTablePid0.setDirty(1, 9)
        pageTablePid0.setEntryTimeStamp(0, 15)
        pageTablePid0.setEntryTimeStamp(1, 10)
        pageTablePid0.setSecondChance(1)
        return pageTablePid0

    def setupPageTableOfPid3(self):
        pageTablePid3 = PageTable()
        page1Pid3 = Page()
        page1Pid3._number = 0
        pageTablePid3.assocPages([page1Pid3])
        pageTablePid3.assocFrames([12])
        pageTablePid3.setDirty(0, 0)
        pageTablePid3.setEntryTimeStamp(0, 1)
        pageTablePid3.setSecondChance(0)
        return pageTablePid3