from unittest import TestCase

from src.Hardware.Asm import ASM
from src.So.Block import Block
from src.So.Page import Page
from src.So.PageTable import PageTable
from src.So.Segment import Segment
from src.So.SegmentTable import SegmentTable

class TestSegmentTable(TestCase):

    def setUp(self):
        self.sgTableSut = SegmentTable()
        self.segment1 = Segment([ASM.CPU(2), ASM.IO(), ASM.CPU(3), ASM.IO(), ASM.CPU(2), ASM.EXIT(1)])
        self.segment1._sid = 1
        self.segment2 = Segment([ASM.CPU(2), ASM.IO(), ASM.CPU(3), ASM.IO(), ASM.CPU(2),ASM.EXIT(1)])
        self.segment2._sid = 2
        self.segment3 = Segment([ASM.CPU(2), ASM.IO(), ASM.CPU(3), ASM.IO(), ASM.CPU(2),ASM.EXIT(1)])
        self.segment3._sid = 3
        self.segment4 = Segment([ASM.CPU(2), ASM.IO(), ASM.CPU(3), ASM.IO(), ASM.CPU(2),ASM.EXIT(1)])
        self.segment4._sid = 4

    def test_when_4associate_4_new_segments_those_segments_dont_have_blocks_assigned(self):
        # Setup
        # Exercise
        self.sgTableSut.assocSegments([self.segment1, self.segment2, self.segment3, self.segment4])

        # Test
        self.assertIsNone(self.sgTableSut.blockOf(4))
        self.assertIsNone(self.sgTableSut.blockOf(1))
        self.assertIsNone(self.sgTableSut.blockOf(2))
        self.assertIsNone(self.sgTableSut.blockOf(3))


    def test_when_2_blocks_are_associated_in_a_table_of_4_segments_and_those_segments_dont_have_assigned_blocks_two_of_these_are_associated(
            self):
        # Setup
        self.sgTableSut.assocSegments([self.segment1, self.segment2, self.segment3, self.segment4])

        # Exercise
        self.sgTableSut.setBlockToSegment(1, Block(0,100))
        self.sgTableSut.setBlockToSegment(2, Block(101,150))

        # Test
        self.assertEqual(Block(0,100).init  , self.sgTableSut.blockOf(1).init)
        self.assertEqual(Block(101,150).init , self.sgTableSut.blockOf(2).init)
        self.assertIsNone(self.sgTableSut.blockOf(3))
        self.assertIsNone(self.sgTableSut.blockOf(4))

    def test_when_clear_the_table_all_segments_dont_have_assigned_blocks(self):
        # Setup
        self.sgTableSut.assocSegments([self.segment1, self.segment2, self.segment3, self.segment4])

        # Exercise
        self.sgTableSut.setBlockToSegment(1, (0, 100))
        self.sgTableSut.setBlockToSegment(2, (101, 150))

        # Exercise
        self.sgTableSut.clearBlocks()

        # Test
        self.assertIsNone(self.sgTableSut.blockOf(1))
        self.assertIsNone(self.sgTableSut.blockOf(2))
        self.assertIsNone(self.sgTableSut.blockOf(3))
        self.assertIsNone(self.sgTableSut.blockOf(4))

    def test_when_ask_for_all_blocks_and_some_segments_dont_have_blocks_only_return_the_assignded_blocks(self):
        # Setup
        self.sgTableSut.assocSegments([self.segment1, self.segment2, self.segment3, self.segment4])

        self.sgTableSut.setBlockToSegment(1, (0, 100))
        self.sgTableSut.setBlockToSegment(2, (101, 150))

        # Exercise
        blocks  = self.sgTableSut.getAllBlocks()

        # Test
        self.assertEqual(2, len(blocks))