from unittest import TestCase
from src.Hardware.Hardware import HARDWARE
from src.So.MemoryManager.MemoryManagerOnDemand import MemoryManagerOnDemand
from src.So.Page import Page
from src.So.PageTable import PageTable


class TestMemoryManagerOnDemand(TestCase):

    def setUp(self):
        HARDWARE.setup(50)
        self.mermoryManagerSut  = MemoryManagerOnDemand(5, 50)
        self.page1              = Page.newPage(5, 0)
        self.page2              = Page.newPage(5, 1)
        self.page3              = Page.newPage(5, 2)
        self.pageTablePid0 = PageTable()
        self.pageTablePid0.setPage(self.page1)
        self.pageTablePid0.setPage(self.page2)

    def test_a_Memory_manager_with_a_Empty_Memory_of_size_50_and_a_frame_Size_of_5_has_10_frames_all_empty(self):
        # Test
        self.assertEqual(5, self.mermoryManagerSut._frameSize)
        self.assertEqual(50, self.mermoryManagerSut._freeMemory)
        self.assertEqual(10, len(self.mermoryManagerSut._freeFrames))
        self.assertListEqual(list(range(0, 10)), self.mermoryManagerSut._freeFrames)
        self.assertTrue(self.mermoryManagerSut._pageTables._table == {})


    def test_when_the_memory_manager_is_asked_for_a_frame_it_returns_the_first_one_From_its_empty_Frames_list_and_is_no_longer_in_the_free_list (self):
        # Setup
        # Exercise
        frame       = self.mermoryManagerSut.getFreeFrame()
        # Test
        self.assertEqual(0, frame)
        self.assertEqual(45, self.mermoryManagerSut._freeMemory)
        self.assertFalse(0 in self.mermoryManagerSut._freeFrames)


    def test_when_the_memory_manager_is_asked_for_the_page_table_of_a_pid_it_returns_The_corresponding_page_table(self):
        # Setup
        self.pageTablePid0.assocFrames([3, 4])
        #Exercise
        self.mermoryManagerSut.savePCBAndPageTable(0, self.pageTablePid0)
        # Test
        self.assertEqual(self.pageTablePid0, self.mermoryManagerSut.getPageTableOf(0))

    def test_when_the_memory_manager_is_given_a_pid_and_Asked_to_release_the_page_table_the_frames_are_Free_Again(self):
        # Setup
        self.mermoryManagerSut.savePCBAndPageTable(0, self.pageTablePid0)
        self.mermoryManagerSut._freeFrames.remove(3)
        self.mermoryManagerSut._freeFrames.remove(4)
        self.mermoryManagerSut._freeMemory  = 40
        self.pageTablePid0.assocFrames([3, 4])
        # Exercise
        self.mermoryManagerSut.releaseFrames(0)
        # Test
        self.assertEqual(50, self.mermoryManagerSut._freeMemory)
        self.assertEqual(10, len(self.mermoryManagerSut._freeFrames))
        self.assertTrue(3 in self.mermoryManagerSut._freeFrames)
        self.assertTrue(4 in self.mermoryManagerSut._freeFrames)
        self.assertIsNone(self.pageTablePid0.frameOf(0))
        self.assertIsNone(self.pageTablePid0.frameOf(1))

    def test_when_the_memory_manager_is_asked_for_a_frame_and_dont_have_more_select_a_victim_and_goes_to_swap(self):
        # Setup
        self.mermoryManagerSut              = MemoryManagerOnDemand(5, 5)
        swapManager                         = self.mermoryManagerSut._swapManager
        self.pageTablePid0.setFrameToPage(0, 0)
        HARDWARE.memory._cells[0]           = 'CPU'
        HARDWARE.memory._cells[1]           = 'CPU'
        HARDWARE.memory._cells[2]           = 'CPU'
        HARDWARE.memory._cells[3]           = 'CPU'
        HARDWARE.memory._cells[4]           = 'CPU'
        self.mermoryManagerSut.savePCBAndPageTable(0, self.pageTablePid0)
        self.mermoryManagerSut._freeFrames  = []
        self.mermoryManagerSut._freeMemory  = 0

        # Exercise
        frame       = self.mermoryManagerSut.getFreeFrame()

        # Test
        self.assertEqual(0, frame)
        self.assertEqual(0, self.mermoryManagerSut._freeMemory)
        self.assertFalse(0 in self.mermoryManagerSut._freeFrames)
        self.assertListEqual([], swapManager._freeSwapFrames)

    # def test_when_the_memory_manager_with_the_selection_of_victim_FIFO_is_asked_for_a_victim_it_selects_the_last_page_that_Was_inserted_in_memory(self):
    #     pageTablePid0 = PageTable()
    #     pageTablePid0.setPage(self.page1)
    #     pageTablePid0.setFrameToPage(0, 0)
    #     pageTablePid0.setEntryTimeStamp(0,0)
    #     pageTablePid1 = PageTable()
    #     pageTablePid1.setPage(self.page2)
    #     pageTablePid1.setFrameToPage(1, 1)
    #     pageTablePid1.setEntryTimeStamp(1,1)
    #     pageTablePid2 = PageTable()
    #     pageTablePid2.setPage(self.page3)
    #     pageTablePid2.setFrameToPage(2, 2)
    #     pageTablePid2.setEntryTimeStamp(2,2)
    #     self.mermoryManagerSut.savePCBAndPageTable(0, pageTablePid0)
    #     self.mermoryManagerSut.savePCBAndPageTable(1, pageTablePid1)
    #     self.mermoryManagerSut.savePCBAndPageTable(2, pageTablePid2)
    #
    #     victim = self.mermoryManagerSut.getVictim()
    #
    #     self.assertEqual(victim, pageTablePid0._table.pop(0) )
    #
    #
    #
    # def test_when_the_memory_manager_with_the_selection_of_victim_LRU_is_asked_for_a_victim_it_selects_the_last_page_that_Was_checked(self):
    #     pass
    #
    # def test_when_the_memory_manager_with_the_selection_of_victim_SecondChance_is_asked_for_a_victim_it_selects_the_page_as_it_were_fifo_but_skips_it_to_the_next_if_it_has_a_second_chance_taking_that_chance_from_the_page_that_wasnt_removed(self):
    #     pass