from unittest import TestCase

from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.Hardware.Irq import PAGE_FAULT_INTERRUPTION_TYPE, IRQ
from src.Hardware.MMU.MMUOnDemand import MMUOnDemand
from src.So.Dispatcher.DispatcherOnDemand import DispatcherOnDemand
from src.So.InterruptionHandlers.PageFaultInterruptionHandler import PageFaultInterruptionHandler
from src.So.Kernel import Kernel
from src.So.Loader.LoaderOnDemand import LoaderOnDemand
from src.So.MemoryManager.MemoryManagerOnDemand import MemoryManagerOnDemand
from src.So.Program import Program


class TestPageFaultInterruptionHandler(TestCase):

    def setUp(self):
        HARDWARE.setup(5)
        self.mmu                    = MMUOnDemand(4, HARDWARE.memory)
        HARDWARE._mmu               = self.mmu
        HARDWARE.cpu._mmu           = self.mmu
        self.kernel                 = Kernel()
        self.kernel._memoryManager  = MemoryManagerOnDemand(5, 5)
        self.kernel._loader         = LoaderOnDemand(self.kernel._memoryManager)
        self.kernel._dispatcher     = DispatcherOnDemand(self.kernel._memoryManager)
        self.program1               = Program("Really? another virus! Remastered.exe", [ASM.CPU(5)])
        HARDWARE.harddisk.newDirectory("C:/fake")
        HARDWARE.harddisk.saveProgram(self.program1, "C:/fake")
        self.program2               = Program("Remastered.exe", [ASM.IO(), ASM.CPU(4), ASM.IO()])
        HARDWARE.harddisk.newDirectory("C:/dark")
        HARDWARE.harddisk.saveProgram(self.program2, "C:/dark")
        self.pageFaultSut = PageFaultInterruptionHandler(self.kernel)
        # pageFaultHandler = PageFaultInterruptionHandler(self.kernel)
        # HARDWARE.interruptVector.register(PAGE_FAULT_INTERRUPTION_TYPE, pageFaultHandler)

    def test_when_a_page_fault_occurs_and_the_page_is_in_disk_and_has_free_frames_its_loaded_in_memory(self):
        # Setup
        self.kernel.execute("C:/fake/Really? another virus! Remastered.exe")
        pageTableOfPid = self.kernel._memoryManager.getPageTableOf(0)
        anIRQCall = IRQ(PAGE_FAULT_INTERRUPTION_TYPE, [0])
        # Test Before Exercise
        self.assertFalse(pageTableOfPid.hasFrame(0))
        self.assertIsNone(pageTableOfPid.frameOf(0))
        self.assertFalse(pageTableOfPid.isInSwap(0))

        # Exercise
        self.pageFaultSut.execute(anIRQCall)
        pageTableOfPid = self.kernel._memoryManager.getPageTableOf(0)
        # Test
        self.assertTrue(pageTableOfPid.hasFrame(0))
        self.assertEqual(0, pageTableOfPid.frameOf(0))
        self.assertFalse(pageTableOfPid.isInSwap(0))
        self.assertListEqual(['CPU', 'CPU', 'CPU', 'CPU', 'CPU'], HARDWARE.memory._cells[0:6])

    def test_when_a_page_fault_occurs_and_the_page_is_in_swap_and_has_free_frames_its_loaded_in_memory(self):
        # Setup
        swapManager = self.kernel._memoryManager._swapManager
        HARDWARE.memory._cells  = ['CPU', '', 'EXIT', 'IO', 'IO']
        HARDWARE.cpu._pc        = 3
        self.kernel.execute("C:/dark/Remastered.exe")
        pageTableOfPid          = self.kernel._memoryManager.getPageTableOf(0)
        pageTableOfPid.setFrameToPage(0, 0)
        anIRQCall               = IRQ(PAGE_FAULT_INTERRUPTION_TYPE, [0])
        self.pageFaultSut.execute(anIRQCall)
        anIRQCall = IRQ(PAGE_FAULT_INTERRUPTION_TYPE, [1])

        # Test Before Exercise
        self.assertTrue(pageTableOfPid.hasFrame(0))
        self.assertEqual(0,pageTableOfPid.frameOf(0))
        self.assertFalse(pageTableOfPid.isInSwap(0))
        self.assertFalse(pageTableOfPid.hasFrame(1))
        self.assertFalse(pageTableOfPid.isInSwap(0))
        self.assertListEqual(['IO', 'CPU', 'CPU', 'CPU', 'CPU'], HARDWARE.memory._cells[0:6])

        # Exercise
        self.pageFaultSut.execute(anIRQCall)
        pageTableOfPid = self.kernel._memoryManager.getPageTableOf(0)

        # Test
        self.assertFalse(pageTableOfPid.hasFrame(0))
        self.assertIsNone(pageTableOfPid.frameOf(0))
        self.assertTrue(pageTableOfPid.isInSwap(0))
        self.assertTrue(swapManager.isInSwap(0, 0))
        self.assertTrue(pageTableOfPid.hasFrame(1))
        self.assertEqual(0, pageTableOfPid.frameOf(1))
        self.assertFalse(pageTableOfPid.isInSwap(1))
        self.assertListEqual(['IO', 'EXIT', 'CPU', 'CPU', 'CPU'], HARDWARE.memory._cells[0:6])