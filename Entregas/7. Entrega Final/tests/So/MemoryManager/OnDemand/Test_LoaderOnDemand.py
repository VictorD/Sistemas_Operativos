from unittest import TestCase

from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.So.Loader.LoaderOnDemand import LoaderOnDemand
from src.So.MemoryManager.MemoryManagerOnDemand import MemoryManagerOnDemand
from src.So.Pcb.Pcb import PCB
from src.So.Program import Program


class TestLoaderOnDemand(TestCase):

    def setUp(self):
        HARDWARE.setup(40)
        self.memoryManager  = MemoryManagerOnDemand(4, 40)
        self.loaderSUT      = LoaderOnDemand(self.memoryManager)
        self.programTest1   = Program("Test.dog", [ASM.CPU(12)])
        HARDWARE.harddisk.newDirectory("C:/perrugrama")
        HARDWARE.harddisk.saveProgram(self.programTest1, "C:/perrugrama")
        self.pcbProgram1    = PCB(0, "C:/perrugrama/Test.dog")

        self.programTest2   = Program("It's not a Virus.dog", [ASM.CPU(7)])
        HARDWARE.harddisk.newDirectory("C:/It's Dog Time!")
        HARDWARE.harddisk.saveProgram(self.programTest2, "C:/It's Dog Time!")
        self.pcbProgram2    = PCB(1, "C:/It's Dog Time!/It's not a Virus.dog")

        self.programTest3   = Program("loader.dog", [ASM.CPU(5)])
        HARDWARE.harddisk.newDirectory("C:/MemoryManger")
        HARDWARE.harddisk.saveProgram(self.programTest3, "C:/MemoryManger")
        self.pcbProgram3    = PCB(2, "C:/MemoryManger/loader.dog")

    def test_when_a_program_with_4_pages_is_loaded_then_the_loader_creates_a_page_table_for_these_pages_and_saves_them_in_the_memory_manager(self):
        # Setup
        # Exercise
        pageTableOfPidZero = self.loaderSUT.createPageTable(len(self.programTest1.instructions), self.pcbProgram1.pid)
        # Test
        self.assertEqual(40, self.loaderSUT._memoryManager._freeMemory)
        self.assertEqual(pageTableOfPidZero, self.loaderSUT._memoryManager.getPageTableOf(0))
        self.assertEqual(10, len(self.loaderSUT._memoryManager._freeFrames))
        self.assertListEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], self.loaderSUT._memoryManager._freeFrames)
        self.assertEqual(4, len(pageTableOfPidZero._table))

        self.assertIsNone(pageTableOfPidZero.frameOf(0))
        self.assertIsNone(pageTableOfPidZero.frameOf(1))
        self.assertIsNone(pageTableOfPidZero.frameOf(2))
        self.assertIsNone(pageTableOfPidZero.frameOf(3))

        self.assertFalse (pageTableOfPidZero._table[0]._hasFrame)
        self.assertFalse (pageTableOfPidZero._table[1]._hasFrame)
        self.assertFalse (pageTableOfPidZero._table[2]._hasFrame)
        self.assertFalse (pageTableOfPidZero._table[3]._hasFrame)

    def test_when_the_loader_is_asked_to_load_a_program_the_program_isnt_loaded_in_memory_and_only_Create_their_pcb_and_page_table(self):
        # Setup
        self.memoryManager._freeFrames  = [4, 3]
        self.memoryManager._freeMemory  = 8

        # Exercise
        self.loaderSUT.loadProgram(self.pcbProgram2)

        # Test
        pageTableOfPidOne = self.loaderSUT._memoryManager.getPageTableOf(1)
        self.assertEqual(8, self.loaderSUT._memoryManager._freeMemory)
        self.assertListEqual([4, 3], self.loaderSUT._memoryManager._freeFrames)
        self.assertEqual(2, len(pageTableOfPidOne._table))
        self.assertIsNone(pageTableOfPidOne.frameOf(0))
        self.assertIsNone(pageTableOfPidOne.frameOf(1))
        self.assertListEqual(['', '', '', '','', '', '', '','', '', '', '', '', '', '', '','', '', '', ''], HARDWARE.memory._cells[0:20])

    def test_when_the_loader_is_asked_to_load_a_page_and_the_program_isnt_loaded_in_memory_it_loads_the_page_on_memory_and_asigns_the_frame(
            self):
        # Setup
        self.memoryManager._freeFrames = [4, 3]
        self.memoryManager._freeMemory = 8

        # Exercise
        self.loaderSUT.loadProgram(self.pcbProgram2)
        pageTableOfProgram2 = self.memoryManager.getPageTableOf(self.pcbProgram2.pid)
        rowOfProgram2       = pageTableOfProgram2._table[0]
        aPageOfProgram2     = rowOfProgram2._page
        freeFrame           = self.memoryManager.getFreeFrame()
        self.loaderSUT.loadPage(self.pcbProgram2, aPageOfProgram2._number, freeFrame)

        # Test
        self.assertEqual(4, self.loaderSUT._memoryManager._freeMemory)
        self.assertListEqual([3], self.loaderSUT._memoryManager._freeFrames)
        self.assertIsNotNone(pageTableOfProgram2.frameOf(0))
        self.assertIsNone(pageTableOfProgram2.frameOf(1))
        self.assertListEqual(['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'CPU', 'CPU', 'CPU', 'CPU'],
                             HARDWARE.memory._cells[0:20])

    def test_when_the_loader_is_asked_to_load_a_page_and_the_instructions_is_in_swap_brings_back_the_instructions_from_swap_and_loads_them_on_memory(self):
        # Setup
        self.memoryManager._freeFrames  = [3, 4]
        self.memoryManager._freeMemory  = 8
        swapManager                     = self.memoryManager._swapManager
        swapManager.swapIN(0, 1, ['CPU', 'CPU', 'CPU', 'CPU'])
        self.loaderSUT.loadProgram(self.pcbProgram2)
        pageTableOfProgram2             = self.memoryManager.getPageTableOf(self.pcbProgram2.pid)
        rowOfProgram2                   = pageTableOfProgram2._table[0]
        aPageOfProgram2                 = rowOfProgram2._page
        pageTableOfProgram2.setFrameInSwap(aPageOfProgram2._number)
        freeFrame                       = self.memoryManager.getFreeFrame()

        # Test Before Exercise
        self.assertFalse    (rowOfProgram2._hasFrame)
        self.assertTrue     (rowOfProgram2._hasSwapFrame)
        self.assertListEqual(['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''], HARDWARE.memory._cells[0:20])

        # Exercise
        self.loaderSUT.loadPage(self.pcbProgram2, aPageOfProgram2._number, freeFrame)

        # Test
        self.assertEqual    (10, len(swapManager._freeSwapFrames))
        self.assertEqual    (4, self.loaderSUT._memoryManager._freeMemory)
        self.assertListEqual([4], self.loaderSUT._memoryManager._freeFrames)
        self.assertIsNotNone(pageTableOfProgram2.frameOf(0))
        self.assertIsNone   (pageTableOfProgram2.frameOf(1))
        self.assertTrue     (rowOfProgram2._hasFrame)
        self.assertFalse    (rowOfProgram2._hasSwapFrame)
        self.assertListEqual(['', '', '', '', '', '', '', '', '', '', '', '', 'CPU', 'CPU', 'CPU', 'CPU', '', '', '', ''], HARDWARE.memory._cells[0:20])
