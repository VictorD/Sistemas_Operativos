from unittest import TestCase

from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.So.Kernel import Kernel
from src.So.Pcb.PcbState import PcbState
from src.So.Program import Program


class TestKernel(TestCase):

    def setUp(self):
        HARDWARE.setup(25)
        self.kernel = Kernel()
        self.prg1   = Program("Tor.dog", [ASM.CPU(2)])
        HARDWARE.harddisk.newDirectory("C:/RotwailerFace")
        HARDWARE.harddisk.saveProgram(self.prg1, "C:/RotwailerFace")

        self.prg2   = Program("Chichi.dog", [ASM.CPU(1), ASM.IO()])
        HARDWARE.harddisk.newDirectory("C:/Mexi Chihuahua")
        HARDWARE.harddisk.saveProgram(self.prg2, "C:/Mexi Chihuahua")

        self.prg3   = Program("Pepe Dog.dog", [ASM.IO(), ASM.CPU(2)])
        HARDWARE.harddisk.newDirectory("C:/No Race")
        HARDWARE.harddisk.saveProgram(self.prg3, "C:/No Race")

    def test_when_a_program_is_executed_it_is_loaded_in_memory_and_its_marked_as_running(self):
        # Execute
        self.kernel.execute("C:/RotwailerFace/Tor.dog")

        # Test
        self.assertEqual(HARDWARE.memory._cells[0:3],['CPU','CPU','EXIT'])
        self.assertEqual(len(self.kernel.scheduler.readyQueue), 0)
        self.assertEqual(self.kernel.pcbTable.pcbRunning.pid, 0)
        self.assertEqual(PcbState.RUNNING, self.kernel.pcbTable.pcbRunning.state)

    def test_when_two_programs_has_both_been_loaded_in_memory_The_first_is_marked_as_Running_and_the_second_goes_to_ready_list(self):
        # Execute
        self.kernel.execute("C:/RotwailerFace/Tor.dog")
        self.kernel.execute("C:/Mexi Chihuahua/Chichi.dog")

        # Test
        self.assertEqual(['CPU','CPU','EXIT','CPU','IO','EXIT'], HARDWARE.memory._cells[0:6])
        self.assertEqual(1, len(self.kernel.scheduler.readyQueue))
        self.assertEqual(0, self.kernel.pcbTable.pcbRunning.pid)
        self.assertEqual(PcbState.RUNNING, self.kernel.pcbTable.pcbRunning.state)

        self.assertEqual(1, self.kernel.scheduler.readyQueue[0].pid)
        self.assertEqual(PcbState.READY, self.kernel.scheduler.readyQueue[0].state)

    def test_when_a_running_program_goes_to_IO_another_Process_will_be_Run(self):
        # Execute
        self.kernel.execute("C:/No Race/Pepe Dog.dog")
        self.kernel.execute("C:/Mexi Chihuahua/Chichi.dog")
        oldPcbRunning   = self.kernel.pcbTable.pcbRunning
        HARDWARE.clock.do_ticks(1)

        # Test
        self.assertNotEqual(oldPcbRunning.pid, self.kernel._pcbtable._pcbRunning.pid)
        self.assertNotEqual(oldPcbRunning.baseAddress, HARDWARE.mmu.baseDir)
        self.assertNotEqual(oldPcbRunning.size, HARDWARE.mmu.limit)

    def test_when_a_program_in_IO_returns_its_placed_at_the_end_of_the_Ready_Queue(self):
        # Execute
        self.kernel.execute("C:/No Race/Pepe Dog.dog")
        self.kernel.execute("C:/RotwailerFace/Tor.dog")
        self.kernel.execute("C:/Mexi Chihuahua/Chichi.dog")
        HARDWARE.clock.do_ticks(5)

        # Test
        self.assertNotEqual(0, self.kernel._pcbtable._pcbRunning.pid)
        self.assertEqual(1, len(self.kernel.scheduler.readyQueue))
        self.assertEqual(0, self.kernel.scheduler.readyQueue[0].pid)
