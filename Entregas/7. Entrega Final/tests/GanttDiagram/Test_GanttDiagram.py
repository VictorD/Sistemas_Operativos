from unittest import TestCase

from src.GanttDiagram.DummyPCB import DummyPCB
from src.GanttDiagram.GanttDiagram import GanttDiagram
from src.GanttDiagram.Snapshot import Snapshot
from src.So.Pcb.PcbState import PcbState
from src.So.Pcb.PcbTable import PcbTable


class TestGanttDiagram(TestCase):

    def setUp(self):
        aPCBTable                   = PcbTable()
        self.ganttDiagram           = GanttDiagram(aPCBTable)

    def test_in_a_system_with_3_pcb_if_sum_ready_state_of_all_from_the_tick_0_to_tick_5_the_average_time_is_2(self):
        # Setup
        self.ganttDiagram._quantityOfPCB = 3
        self.setupTest1()

        # Exercise
        averageTime = self.ganttDiagram.calculateGantDiagram()
        # Test
        self.assertEqual(2, averageTime)

    def test_in_a_system_with_4_pcb_if_sum_ready_state_of_all_from_the_tick_0_to_tick_10_the_average_time_is_6dot75(self):
        # Setup
        self.ganttDiagram._quantityOfPCB = 4
        self.setupTest2()

        # Exercise
        averageTime = self.ganttDiagram.calculateGantDiagram()
        # Test
        self.assertEqual(6.75, averageTime)

    def setupTest1(self):
        self.loadNewSnapshotTest1(0, PcbState.RUNNING, PcbState.READY, PcbState.READY)
        self.loadNewSnapshotTest1(1, PcbState.RUNNING, PcbState.READY, PcbState.READY)
        self.loadNewSnapshotTest1(2, PcbState.TERMINATED, PcbState.RUNNING, PcbState.READY)
        self.loadNewSnapshotTest1(3, PcbState.TERMINATED, PcbState.RUNNING, PcbState.READY)
        self.loadNewSnapshotTest1(4, PcbState.TERMINATED, PcbState.TERMINATED, PcbState.RUNNING)
        self.loadNewSnapshotTest1(5, PcbState.TERMINATED, PcbState.TERMINATED, PcbState.RUNNING)

    def setupTest2(self):
        self.loadNewSnapshotTest2(0, PcbState.RUNNING, PcbState.READY, PcbState.READY, PcbState.READY)
        self.loadNewSnapshotTest2(1, PcbState.RUNNING, PcbState.READY, PcbState.READY, PcbState.READY)
        self.loadNewSnapshotTest2(2, PcbState.RUNNING, PcbState.READY, PcbState.READY, PcbState.READY)
        self.loadNewSnapshotTest2(3, PcbState.WAITING, PcbState.RUNNING, PcbState.READY, PcbState.READY)
        self.loadNewSnapshotTest2(4, PcbState.WAITING, PcbState.RUNNING, PcbState.READY, PcbState.READY)
        self.loadNewSnapshotTest2(5, PcbState.WAITING, PcbState.RUNNING, PcbState.READY, PcbState.READY)
        self.loadNewSnapshotTest2(6, PcbState.READY, PcbState.RUNNING, PcbState.READY, PcbState.READY)
        self.loadNewSnapshotTest2(7, PcbState.READY, PcbState.RUNNING, PcbState.READY, PcbState.READY)
        self.loadNewSnapshotTest2(8, PcbState.READY, PcbState.WAITING, PcbState.RUNNING, PcbState.READY)
        self.loadNewSnapshotTest2(9, PcbState.READY, PcbState.WAITING, PcbState.RUNNING, PcbState.READY)
        self.loadNewSnapshotTest2(10, PcbState.READY, PcbState.WAITING, PcbState.RUNNING, PcbState.READY)

    def loadNewSnapshotTest1(self, tick, statePCB1, statePCB2, statePCB3):
        newSnapshot = Snapshot(tick)
        newPcb1 = DummyPCB(0, statePCB1)
        newPcb2 = DummyPCB(1, statePCB2)
        newPcb3 = DummyPCB(2, statePCB3)
        newSnapshot._pcbList = [newPcb1, newPcb2, newPcb3]
        self.ganttDiagram._snapshotList.append(newSnapshot)

    def loadNewSnapshotTest2(self, tick, statePCB1, statePCB2, statePCB3, statePCB14):
        newSnapshot = Snapshot(tick)
        newPcb1 = DummyPCB(0, statePCB1)
        newPcb2 = DummyPCB(1, statePCB2)
        newPcb3 = DummyPCB(2, statePCB3)
        newPcb4 = DummyPCB(3, statePCB14)
        newSnapshot._pcbList = [newPcb1, newPcb2, newPcb3, newPcb4]
        self.ganttDiagram._snapshotList.append(newSnapshot)
