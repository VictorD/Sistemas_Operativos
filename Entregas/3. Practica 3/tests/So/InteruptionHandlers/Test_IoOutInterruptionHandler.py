from unittest import TestCase

from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.So.Kernel import Kernel
from src.So.Pcb.PcbState import PcbState
from src.So.Program import Program


class TestIoOutInterruptionHandler(TestCase):

    def setUp(self):
        HARDWARE.setup(50)
        self.kernel    = Kernel()
        self.program   = Program("It's a Virus!.exe", [ASM.IO(),ASM.CPU(1)])
        HARDWARE.harddisk.newDirectory("C:/virus")
        HARDWARE.harddisk.saveProgram(self.program, "C:/virus")

        self.program1  = Program("Really? another virus!.exe", [ASM.CPU(4)])
        HARDWARE.harddisk.newDirectory("C:/fake")
        HARDWARE.harddisk.saveProgram(self.program1, "C:/fake")

    def test_when_a_process_is_back_from_io_and_cpu_is_idle_it_goes_to_running(self):
        # Setup
        self.kernel.execute("C:/virus/It's a Virus!.exe")

        # Execise
        HARDWARE.clock.do_ticks(5)

        # Test
        self.assertFalse(self.kernel.isCPUIdle())
        self.assertFalse(HARDWARE.ioDevice.is_busy)
        self.assertEqual(0, self.kernel.pcbTable.pcbRunning.pid)
        self.assertEqual(PcbState.RUNNING, self.kernel.pcbTable.pcbRunning.state)
        self.assertEqual([], self.kernel.ioDeviceController._waiting_queue)

    def test_when_a_process_is_back_from_io_and_cpu_is_busy_it_goes_to_the_ready_Queue(self):
        # Setup
        self.kernel.execute("C:/virus/It's a Virus!.exe")
        self.kernel.execute("C:/fake/Really? another virus!.exe")

        # Execise
        HARDWARE.clock.do_ticks(5)

        # Test
        self.assertEqual(1, self.kernel.pcbTable.pcbRunning.pid)
        self.assertEqual(PcbState.RUNNING, self.kernel.pcbTable.pcbRunning.state)
        self.assertEqual(PcbState.READY, self.kernel.pcbTable._readyQueue[0].state)
