from unittest import TestCase
from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.So.Kernel import Kernel
from src.So.Program import Program


class TestShutdownInterruptionHandler(TestCase):

    def setUp(self):
        HARDWARE.setup(50)
        self.kernel    = Kernel()
        self.program   = Program("It's a Virus!.exe", [ASM.IO(), ASM.CPU(1)])
        HARDWARE.harddisk.newDirectory("C:/virus")
        HARDWARE.harddisk.saveProgram(self.program, "C:/virus")

        self.program1  = Program("Really? another virus!.exe", [ASM.IO(), ASM.CPU(1)])
        HARDWARE.harddisk.newDirectory("C:/virus/fake")
        HARDWARE.harddisk.saveProgram(self.program1, "C:/virus/fake")

        self.program2  = Program("Flash.exe", [ASM.CPU(7)])
        HARDWARE.harddisk.newDirectory("C:/bios")
        HARDWARE.harddisk.saveProgram(self.program2, "C:/bios")

        self.shutdown  = Program("Shutdown.exe", [ASM.SD()])
        HARDWARE.harddisk.newDirectory("C:/system/private/risk")
        HARDWARE.harddisk.saveProgram(self.shutdown, "C:/system/private/risk")


    def test_when_a_program_executes_SD_The_PCBTABLE_should_Be_clean(self):

        # Exercise
        self.kernel.execute("C:/virus/It's a Virus!.exe")
        self.kernel.execute("C:/virus/fake/Really? another virus!.exe")
        self.kernel.execute("C:/bios/Flash.exe")
        self.kernel.execute("C:/system/private/risk/Shutdown.exe")

        # we set up a program  and program1 to go directly to IO, while program 2 runs, so we can have them on the ready queue with shutdown
        HARDWARE.clock.do_ticks(9)

        # We check that the ready queue is as expected, with shutdown, program and program 1 in the ready queue
        # And program 2 currently running
        self.assertEqual(len(self.kernel.pcbTable._readyQueue), 3)
        self.assertFalse(self.kernel.pcbTable.hasNotRunningPCB())

        # The next following tick exits program2, and then the next one runs the shutdown instruction
        HARDWARE.clock.do_ticks(2)

        ## We check that the PCB table is now clean
        self.assertTrue(self.kernel.pcbTable.isEmptyReadyQueue())
        self.assertTrue(self.kernel.pcbTable.hasNotRunningPCB())


    def test_when_a_program_executes_SD_it_should_signal_the_IOS_that_it_is_terminating_leaving_it_without_any_running_operation_and_idle(self):
        # Exercise
        self.kernel.execute("C:/virus/It's a Virus!.exe")
        self.kernel.execute("C:/system/private/risk/Shutdown.exe")

        # we set up a program  to go directly to IO
        HARDWARE.clock.do_ticks(1)

        # We check that the printer is not idle and with an operation running
        self.assertTrue(HARDWARE.ioDevice._busy)
        self.assertIsNotNone(HARDWARE.ioDevice._operation)

        # The next following tick runs the shutdown instruction
        HARDWARE.clock.do_ticks(1)

        ## We check that the printer is idle and without any operation
        self.assertFalse(HARDWARE.ioDevice.is_busy)
        self.assertIsNone(HARDWARE.ioDevice._operation)


    def test_when_a_program_executes_SD_it_should_Clean_its_waiting_queue_and_current_pcb(self):
        # Exercise
        self.kernel.execute("C:/virus/It's a Virus!.exe")
        self.kernel.execute("C:/virus/fake/Really? another virus!.exe")
        self.kernel.execute("C:/system/private/risk/Shutdown.exe")

        # we set up a program  and program1 to go directly to IO so program 1 is on the waiting queue
        HARDWARE.clock.do_ticks(2)

        # We check that the driver has program 1 on its waiting queue and is running a pcb
        self.assertEqual(len(self.kernel.ioDeviceController._waiting_queue),1)
        self.assertIsNotNone(self.kernel.ioDeviceController._currentPCB)

        # The next following tick runs the shutdown instruction
        HARDWARE.clock.do_ticks(1)

        # We check that the driver now no longer has any pcb in its waiting list and is not running a pcb
        self.assertEqual(len(self.kernel.ioDeviceController._waiting_queue), 0)
        self.assertIsNone(self.kernel.ioDeviceController._currentPCB)


    def test_when_a_program_executes_SD_The_Clock_should_not_be_running(self):
        # Exercise
        self.kernel.execute("C:/virus/It's a Virus!.exe")
        self.kernel.execute("C:/virus/fake/Really? another virus!.exe")
        self.kernel.execute("C:/bios/Flash.exe")
        self.kernel.execute("C:/system/private/risk/Shutdown.exe")

        # we set up a program  and program1 to go directly to IO, while program 2 runs, so we can have them on the ready queue with shutdown
        HARDWARE.clock.do_ticks(9)

        # The next following tick exits program2, and then the next one runs the shutdown instruction
        HARDWARE.clock.do_ticks(2)

        ## We check that the Clock has stopped following the shut down execution
        self.assertFalse(HARDWARE.clock._running)

    def test_when_a_program_executes_SD_The_Cpu_should_be_left_Idle(self):
        # Exercise
        self.kernel.execute("C:/virus/It's a Virus!.exe")
        self.kernel.execute("C:/virus/fake/Really? another virus!.exe")
        self.kernel.execute("C:/bios/Flash.exe")
        self.kernel.execute("C:/system/private/risk/Shutdown.exe")

        # we set up a program  and program1 to go directly to IO, while program 2 runs, so we can have them on the ready queue with shutdown
        HARDWARE.clock.do_ticks(9)

        # We check that the CPU is not idle as expected
        self.assertFalse(self.kernel.isCPUIdle())

        # The next following tick exits program2, and then the next one runs the shutdown instruction
        HARDWARE.clock.do_ticks(2)

        ## We check that the PC has been left idle
        self.assertTrue(self.kernel.isCPUIdle())
