class Directory():

    def __init__(self, name):
            self._name = name
            self._directories = {}
            self._programs = {}

    @property
    def name(self):
        return self._name

    @property
    def directories(self):
        return self._directories

    @property
    def programs(self):
        return self._programs

    def fetchProgram(self,aDirection):
        aDirection.pop(0)

        if len(aDirection) == 1:
            return self.programs[aDirection[0]]
        else:
            return self.directories[aDirection[0]].fetchProgram(aDirection)

    def newRute(self, aDirection):
        aDirection.pop(0)

        # hay que pensar una forma mas simple para este switch case
        if len(aDirection) == 1:
            self.newDirectory(aDirection[0])
        elif self.hasDirection(aDirection[0]):
            self.directories[aDirection[0]].newRute(aDirection)
        else:
            self.newDirectory(aDirection[0])
            self.directories[aDirection[0]].newRute(aDirection)

    def hasDirection(self, aDirection):
        return aDirection in self.directories

    def newDirectory(self, aDirection):
        self.directories[aDirection] = Directory(aDirection)

    def saveProgram(self,aProgram,aDirection):
        aDirection.pop(0)

        if len(aDirection) == 0:
            self.programs[aProgram.name] = aProgram
        else:
            self.directories[aDirection[0]].saveProgram(aProgram,aDirection)