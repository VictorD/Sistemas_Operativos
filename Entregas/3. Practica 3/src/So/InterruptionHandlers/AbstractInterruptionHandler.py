from src import log

## emulates the  Interruptions Handlers
class AbstractInterruptionHandler():

    def __init__(self, kernel):
        self._kernel    = kernel

    def execute(self, irq):
        log.logger.error("-- EXECUTE MUST BE OVERRIDEN in class {classname}".format(classname=self.__class__.__name__))

    def contextSwitchOutOfCPU(self):
        if not self.kernel.pcbTable.isEmptyReadyQueue():
            self.kernel.pcbTable.assignNextProgramToRun()
            self.kernel.pcbTable.pcbRunning.running()
            self.kernel.dispatcher.load(self.kernel.pcbTable.pcbRunning)

    def contextSwitchInToCPU(self, aPCB):
        if self.kernel.pcbTable.hasNotRunningPCB():
            aPCB.running()
            self.kernel.pcbTable.pcbRunning = aPCB
            self.kernel.dispatcher.load(aPCB)
        else:
            aPCB.ready()
            self.kernel.pcbTable.addToReadyQueue(aPCB)

    # ----- Getters & Setters ----- #
    @property
    def kernel(self):
        return self._kernel
