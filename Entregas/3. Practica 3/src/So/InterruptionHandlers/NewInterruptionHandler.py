from src.So.InterruptionHandlers.AbstractInterruptionHandler import AbstractInterruptionHandler


class NewInterruptionHandler(AbstractInterruptionHandler):

    def execute(self, irq):
        directionOfProgramToLoad = irq.parameters

        pcbCreated = self.kernel.pcbTable.createNewPcb(directionOfProgramToLoad)

        self.kernel.loader.loadProgram(pcbCreated)

        self.contextSwitchInToCPU(pcbCreated)
