from src import log
from src.So.InterruptionHandlers.AbstractInterruptionHandler import AbstractInterruptionHandler


class IoInInterruptionHandler(AbstractInterruptionHandler):

    def execute(self, irq):
        log.logger.info(self.kernel.ioDeviceController)
        pcbRunning = self.kernel.pcbTable.pcbRunning
        operation = irq.parameters  # Es la operacion de IO que se va a ejecutar
        self.kernel.dispatcher.save(pcbRunning)
        pcbRunning.waiting()
        self.kernel.pcbTable.putRunningPCBinNone()
        self.contextSwitchOutOfCPU()
        self.kernel.ioDeviceController.runOperation(pcbRunning, operation)



