from src import log
from src.Hardware.Hardware import HARDWARE


# Has the responsability of load the state of the PCB into the hardware, and save the state of hardware into the PCB
class Dispatcher():

    def save(self, aPcb):
        aPcb.pc         = HARDWARE.cpu.pc
        HARDWARE.cpu.pc = -1

    def load(self, aPcb):
        HARDWARE.cpu.pc     = aPcb.pc
        HARDWARE.mmu.baseDir= aPcb.baseAddress
        HARDWARE.mmu.limit  = aPcb.size
        log.logger.info(f"Running Process: {aPcb}")
