from src.So.Pcb.PcbState import PcbState


class PCB:

    def __init__(self, aPID, aDirection):
        self._pid           = aPID
        self._direction     = aDirection
        self._pc            = 0
        self._state         = PcbState.NEW
        self._baseAddress   = 0
        self._size          = 0

    # Change the State of PCB to Running
    def running(self):
        self._state = PcbState.RUNNING

    # Change the State of PCB to Ready
    def ready(self):
        self._state = PcbState.READY

    # Change the State of PCB to Terminated
    def terminated(self):
        self._state = PcbState.TERMINATED

    # Change the State of PCB to Waiting
    def waiting(self):
        self._state = PcbState.WAITING

    # ----- Getters & Setters ----- #
    @property
    def pid(self):
        return self._pid

    @property
    def direction(self):
        return self._direction

    @property
    def pc(self):
        return self._pc

    @property
    def baseAddress(self):
        return self._baseAddress

    @property
    def state(self):
        return self._state

    @property
    def size(self):
        return self._size

    @size.setter
    def size(self, aSize):
        self._size = aSize

    @pid.setter
    def pid(self, aPid):
        self._pid = aPid

    @pc.setter
    def pc(self, aPc):
        self._pc = aPc

    @baseAddress.setter
    def baseAddress(self, aBaseAddress):
        self._baseAddress = aBaseAddress

    def __repr__(self):
        return f'PID: {self.pid}'
