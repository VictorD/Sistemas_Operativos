from src.So.Pcb.Pcb import PCB

# Class that has the responsibility of managing PCB
class PcbTable():

    def __init__(self):
        self._pidAcumulator = 0
        self._pcbTable      = {}
        self._readyQueue    = []
        self._pcbRunning    = None

    def addToReadyQueue(self, somePCB):
        self._readyQueue.append(somePCB)

    def getNextOfReadyQueue(self):
        return self._readyQueue.pop(0)

    def isEmptyReadyQueue(self):
        return self._readyQueue == []

    def createNewPcb(self, aDirection):
        newPCB  = PCB(self._pidAcumulator, aDirection)
        self._pcbTable.setdefault(self._pidAcumulator, newPCB)
        self._pidAcumulator += 1
        return newPCB

    def hasNotRunningPCB(self):
        return self.pcbRunning is None

    def putRunningPCBinNone(self):
        self.pcbRunning = None

    def assignNextProgramToRun(self):
        self.pcbRunning = self.getNextOfReadyQueue()

    def cleanAll(self):
        self._pidAcumulator = 0
        self._pcbTable = {}
        self._readyQueue = []
        self.pcbRunning = None

    # ----- Getters & Setters ----- #
    @property
    def pcbRunning(self):
        return self._pcbRunning

    @pcbRunning.setter
    def pcbRunning(self, apcb):
        self._pcbRunning = apcb

    @property
    def readyQueue(self):
        return self._readyQueue
