#!/usr/bin/env python
from src import log
from src.Hardware.Hardware import HARDWARE
from src.Hardware.Irq import KILL_INTERRUPTION_TYPE, IO_IN_INTERRUPTION_TYPE, IO_OUT_INTERRUPTION_TYPE, \
    NEW_INTERRUPTION_TYPE, IRQ, SHUTDOWN_INTERRUPTION_TYPE
from src.So.Dispatcher import Dispatcher
from src.So.InterruptionHandlers.ShutDownInterruptionHandler import ShutDownInterruptionHandler
from src.So.IoDeviceController import IoDeviceController
from src.So.InterruptionHandlers.IoInInterruptionHandler import IoInInterruptionHandler
from src.So.InterruptionHandlers.IoOutInterruptionHandler import IoOutInterruptionHandler
from src.So.InterruptionHandlers.KillInterruptionHandler import KillInterruptionHandler
from src.So.Loader import Loader
from src.So.InterruptionHandlers.NewInterruptionHandler import NewInterruptionHandler
from src.So.Pcb.PcbTable import PcbTable


# emulates the core of an Operative System
class Kernel():

    def __init__(self):
        ## setup interruption handlers
        killHandler             = KillInterruptionHandler(self)
        HARDWARE.interruptVector.register(KILL_INTERRUPTION_TYPE, killHandler)

        ioInHandler             = IoInInterruptionHandler(self)
        HARDWARE.interruptVector.register(IO_IN_INTERRUPTION_TYPE, ioInHandler)

        ioOutHandler            = IoOutInterruptionHandler(self)
        HARDWARE.interruptVector.register(IO_OUT_INTERRUPTION_TYPE, ioOutHandler)

        newHandler = NewInterruptionHandler(self)
        HARDWARE.interruptVector.register(NEW_INTERRUPTION_TYPE, newHandler)

        shutDownHandler = ShutDownInterruptionHandler(self)
        HARDWARE.interruptVector.register(SHUTDOWN_INTERRUPTION_TYPE, shutDownHandler)

        ## controls the Hardware's I/O Device
        self._ioDeviceController= IoDeviceController(HARDWARE.ioDevice)

        # Setup of pcbtable
        self._pcbtable          = PcbTable()

        # Setup of dispatcher
        self._dispatcher        = Dispatcher()

        # Setup of loader
        self._loader            = Loader()

    ## emulates a "system call" for programs execution
    def execute(self, aDirection):

        newIRQ = IRQ(NEW_INTERRUPTION_TYPE, aDirection)
        HARDWARE.cpu._interruptVector.handle(newIRQ)
        log.logger.info("\n Executing program in: " + aDirection)
        log.logger.info(HARDWARE)

    # Check if the Cpu is Idle
    def isCPUIdle(self):
        return HARDWARE.cpu.pc == -1

    # Cleans all devices and data types
    def cleanAll(self):
        HARDWARE.memory.cleanAll()
        log.logger.info("MEMORY CLEANED")

        self.pcbTable.cleanAll()
        log.logger.info("PCB TABLE CLEANED")

        self.ioDeviceController.cleanAll()
        log.logger.info("IO DEVICE CONTROLLERS CLEANED")

    # ShutDowns the machine
    def shutDown(self):
        HARDWARE.switchOff()

    # ----- Getters & Setters ----- #
    @property
    def ioDeviceController(self):
        return self._ioDeviceController

    @property
    def pcbTable(self):
        return self._pcbtable

    @property
    def dispatcher(self):
        return self._dispatcher

    @property
    def loader(self):
        return self._loader

    def __repr__(self):
        return "Kernel "
