from unittest import TestCase

from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.So.ContextSwitcher.ContextSwitch import ContextSwitch
from src.So.Kernel import Kernel
from src.So.Pcb.PCBPriority import PCBPriority
from src.So.Program import Program


class TestSchedulerMultilevelQueue(TestCase):

    def setUp(self):
        HARDWARE.setup(50)
        self.kernel     = Kernel.withMultilevelQueueScheduler(5)
        self.program    = Program("Dogma.dog", [ASM.IO(), ASM.CPU(10)])
        HARDWARE.harddisk.newDirectory("C:/dogminator")
        HARDWARE.harddisk.saveProgram(self.program, "C:/dogminator")

        self.program1 = Program("CatDog.dog", [ASM.CPU(10)])
        HARDWARE.harddisk.newDirectory("C:/kittys")
        HARDWARE.harddisk.saveProgram(self.program1, "C:/kittys")

        self.program2 = Program("Droppy.dog", [ASM.EXIT(1)])
        HARDWARE.harddisk.newDirectory("C:/cartoon")
        HARDWARE.harddisk.saveProgram(self.program2, "C:/cartoon")

        self.program3 = Program("PuppyWar.dog", [ASM.CPU(10)])
        HARDWARE.harddisk.newDirectory("C:/P/W")
        HARDWARE.harddisk.saveProgram(self.program3, "C:/P/W")

    def test_with_an_empty_ready_queue_and_a_idle_CPU_if_a_new_process_with_high_priority_is_created_goes_to_running(self):
        # Setup

        # Exercise
        self.kernel.execute("C:/dogminator/Dogma.dog", PCBPriority.HIGH)

        # Test
        self.assertEqual(0, self.kernel.pcbTable.pcbRunning.pid)
        self.assertEqual("C:/dogminator/Dogma.dog", self.kernel.pcbTable.pcbRunning.direction)
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue())
        self.assertTrue(self.kernel.scheduler.isEmptyForegroundQueue())

    def test_with_an_empty_ready_queue_and_a_busy_CPU_if_a_new_process_with_low_priority_is_created_goes_to_background_queue(self):
        # Setup
        self.kernel.execute("C:/dogminator/Dogma.dog", PCBPriority.HIGH)

        # Exercise
        self.kernel.execute("C:/kittys/CatDog.dog", PCBPriority.LOW)

        # Test
        self.assertEqual(0, self.kernel.pcbTable.pcbRunning.pid)
        self.assertEqual("C:/dogminator/Dogma.dog", self.kernel.pcbTable.pcbRunning.direction)
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue())
        self.assertTrue(self.kernel.scheduler.isEmptyForegroundQueue())
        self.assertFalse(self.kernel.scheduler.isEmptyBackgroundQueue())
        self.assertEqual(1, self.kernel.scheduler.backgroundQueue[0].pid)

    def test_with_an_empty_foreground_queue_and_a_busy_CPU_with_a_high_priority_process_if_a_new_process_is_created_and_has_the_same_Level_of_priority_of_that_its_running_goes_to_foreground_queue(
            self):
        # Setup
        self.kernel.execute("C:/dogminator/Dogma.dog", PCBPriority.HIGH)

        # Exercise
        self.kernel.execute("C:/kittys/CatDog.dog", PCBPriority.HIGHEST)

        # Test
        self.assertEqual(0, self.kernel.pcbTable.pcbRunning.pid)
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue())
        self.assertFalse(self.kernel.scheduler.isEmptyForegroundQueue())
        self.assertTrue(self.kernel.scheduler.isEmptyBackgroundQueue())
        self.assertEqual(1, self.kernel.scheduler.foregroundQueue[0].pid)
    def test_with_an_empty_background_queue_and_a_busy_CPU_with_a_low_priority_process_if_a_new_process_is_created_and_has_the_same_Level_of_priority_of_that_its_running_goes_to_background_queue(
            self):
        # Setup
        self.kernel.execute("C:/dogminator/Dogma.dog", PCBPriority.LOWEST)

        # Exercise
        self.kernel.execute("C:/kittys/CatDog.dog", PCBPriority.MEDIUM)

        # Test
        self.assertEqual(0, self.kernel.pcbTable.pcbRunning.pid)
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue())
        self.assertTrue(self.kernel.scheduler.isEmptyForegroundQueue())
        self.assertFalse(self.kernel.scheduler.isEmptyBackgroundQueue())
        self.assertEqual(1, self.kernel.scheduler.backgroundQueue[0].pid)

    def test_with_a_busy_CPU_with_a_low_level_priority_process_if_a_new_process_with_high_level_priority_Comes_the_runing_process_goes_to_background_queue_and_high_level_process_goes_to_cpu(self):
        # Setup
        self.kernel.execute("C:/dogminator/Dogma.dog", PCBPriority.MEDIUM)

        # Exercise
        self.kernel.execute("C:/kittys/CatDog.dog", PCBPriority.HIGH)

        # Test
        self.assertEqual(1, self.kernel.pcbTable.pcbRunning.pid)
        self.assertEqual("C:/kittys/CatDog.dog", self.kernel.pcbTable.pcbRunning.direction)
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue())
        self.assertTrue(self.kernel.scheduler.isEmptyForegroundQueue())
        self.assertFalse(self.kernel.scheduler.isEmptyBackgroundQueue())
        self.assertEqual(0, self.kernel.scheduler.backgroundQueue[0].pid)

    def test_when_a_process_come_back_of_IO_and_has_more_priority_Level_than_the_running_the_runing_process_goes_to_the_ready_queue_and_high_level_process_goes_to_cpu(self):
        # Setup
        self.kernel.execute("C:/dogminator/Dogma.dog", PCBPriority.HIGH)
        self.kernel.execute("C:/kittys/CatDog.dog", PCBPriority.MEDIUM)

        # Exercise
        HARDWARE.clock.do_ticks(5)

        # Test
        self.assertEqual(0, self.kernel.pcbTable.pcbRunning.pid)
        self.assertEqual("C:/dogminator/Dogma.dog", self.kernel.pcbTable.pcbRunning.direction)
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue())
        self.assertTrue(self.kernel.scheduler.isEmptyForegroundQueue())
        self.assertFalse(self.kernel.scheduler.isEmptyBackgroundQueue())
        self.assertEqual(1, self.kernel.scheduler.backgroundQueue[0].pid)

    def test_when_a_process_ends_and_the_ready_queue_is_empty_the_cpu_goes_idle(self):
        # Setup
        self.kernel.execute("C:/cartoon/Droppy.dog")

        # Exercise
        HARDWARE.clock.do_ticks(2)

        # Test
        self.assertIsNone(self.kernel.pcbTable.pcbRunning)
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue())
        self.assertTrue(self.kernel.isCPUIdle())

    def test_when_a_process_ends_and_the_ready_queue_has_one_process_the_cpu_goes_busy(self):
        # Setup
        self.kernel.execute("C:/cartoon/Droppy.dog")
        self.kernel.execute("C:/kittys/CatDog.dog", PCBPriority.LOW)
        # Exercise
        HARDWARE.clock.do_ticks(2)

        # Test
        self.assertFalse(self.kernel.isCPUIdle())
        self.assertIsNotNone(self.kernel.pcbTable.pcbRunning)
        self.assertEqual(1, self.kernel.pcbTable.pcbRunning.pid)
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue())

    def test_when_a_process_go_to_io_and_the_ready_queue_is_empty_the_cpu_goes_idle(self):
        # Setup
        self.kernel.execute("C:/dogminator/Dogma.dog")

        # Exercise
        HARDWARE.clock.do_ticks(1)

        # Test
        self.assertIsNone(self.kernel.pcbTable.pcbRunning)
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue())
        self.assertTrue(self.kernel.isCPUIdle())

    def test_when_a_process_go_to_io_and_the_ready_queue_has_one_process_the_cpu_goes_busy(self):
        # Setup
        self.kernel.execute("C:/dogminator/Dogma.dog", PCBPriority.HIGHEST)
        self.kernel.execute("C:/kittys/CatDog.dog", PCBPriority.HIGH)
        # Exercise
        HARDWARE.clock.do_ticks(1)

        # Test
        self.assertFalse(self.kernel.isCPUIdle())
        self.assertIsNotNone(self.kernel.pcbTable.pcbRunning)
        self.assertEqual(1, self.kernel.pcbTable.pcbRunning.pid)
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue())

    def test_when_a_Time_Out_ocurrs_and_the_ready_queue_is_empty_the_cpu_goes_idle(self):
        # Setup
        HARDWARE.timer.quantum = 1

        # Exercise
        HARDWARE.clock.do_ticks(2)

        # Test
        self.assertIsNone(self.kernel.pcbTable.pcbRunning)
        self.assertTrue(self.kernel.scheduler.isEmptyReadyQueue())
        self.assertTrue(self.kernel.isCPUIdle())
        self.assertEqual(0, HARDWARE.timer._timeOutCounter)

    def test_when_a_Time_Out_ocurrs_and_the_ready_queue_is_not_empty_the_cpu_keeps_busy(self):
        # Setup
        HARDWARE.timer.quantum = 1
        self.kernel.execute("C:/P/W/PuppyWar.dog", PCBPriority.HIGH)
        self.kernel.execute("C:/kittys/CatDog.dog", PCBPriority.HIGH)

        # Exercise
        HARDWARE.clock.do_ticks(2)

        # Test
        self.assertIsNotNone(self.kernel.pcbTable.pcbRunning)
        self.assertEqual(1, self.kernel.pcbTable.pcbRunning.pid)
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue())
        self.assertFalse(self.kernel.scheduler.isEmptyForegroundQueue())
        self.assertTrue(self.kernel.scheduler.isEmptyBackgroundQueue())
        self.assertEqual(0, self.kernel.scheduler.foregroundQueue[0].pid)
        self.assertFalse(self.kernel.isCPUIdle())
        self.assertEqual(1, HARDWARE.timer._timeOutCounter)

    def tearDown(self):
        ContextSwitch.kill()