from unittest import TestCase

from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.So.Kernel import Kernel
from src.So.Pcb.PCBPriority import PCBPriority
from src.So.Program import Program



class TestSchedulerPriorityExp(TestCase):

    def setUp(self):
        HARDWARE.setup(50)
        self.kernel = Kernel.withPriorityExpropiativeScheduler()
        self.program = Program("It's a Virus!.exe", [ASM.IO(), ASM.CPU(10)])
        HARDWARE.harddisk.newDirectory("C:/virus")
        HARDWARE.harddisk.saveProgram(self.program, "C:/virus")

        self.program1 = Program("Really? another virus!.exe", [ASM.CPU(10)])
        HARDWARE.harddisk.newDirectory("C:/virus/fake")
        HARDWARE.harddisk.saveProgram(self.program1, "C:/virus/fake")

        self.program2 = Program("Flash.exe", [ASM.CPU(10)])
        HARDWARE.harddisk.newDirectory("C:/bios")
        HARDWARE.harddisk.saveProgram(self.program2, "C:/bios")

        self.program3 = Program("troyanWar.exe", [ASM.CPU(10)])
        HARDWARE.harddisk.newDirectory("C:/system/private/risk")
        HARDWARE.harddisk.saveProgram(self.program3, "C:/system/private/risk")


    def test_should_a_process_return_from_io_and_be_handled_to_the_scheduler_and_there_is_a_process_with_more_priority_running_it_goes_to_the_queue(self):
        # Setup
        self.kernel.execute("C:/virus/It's a Virus!.exe", PCBPriority.LOW)
        self.kernel.execute("C:/virus/fake/Really? another virus!.exe", PCBPriority.HIGH)

        # we set up a program  and program to go directly to IO, while program1 runs, once program  comes back from io, it should go to the queue as it has
        #a lower priority than the one currently running
        HARDWARE.clock.do_ticks(8)

        # We check that the ready queue is as expected, with program on the readyQueue
        # And program1 currently running
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue())
        self.assertEqual(self.kernel.scheduler.readyQueue[PCBPriority.LOW.value -1][0].direction, "C:/virus/It's a Virus!.exe")
        self.assertEqual(self.kernel.pcbTable.pcbRunning.direction, "C:/virus/fake/Really? another virus!.exe")


    def test_should_a_process_return_from_io_and_be_handed_to_the_scheduler_and_it_has_more_priority_than_the_one_that_its_running_it_goes_to_cpu_and_the_other_to_the_queue(self):
        # Setup
        self.kernel.execute("C:/virus/It's a Virus!.exe", PCBPriority.HIGH)
        self.kernel.execute("C:/virus/fake/Really? another virus!.exe", PCBPriority.LOW)

        # we set up a program  and program to go directly to IO, while program1 runs, once program  comes back from io, it should go to cpu as it has
        # a higher priority than the one currently running
        HARDWARE.clock.do_ticks(8)

        # We check that the ready queue is as expected, with program1 on the readyQueue
        # And program currently running
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue())
        self.assertEqual(self.kernel.scheduler.readyQueue[PCBPriority.LOW.value -1][0].direction, "C:/virus/fake/Really? another virus!.exe")
        self.assertEqual(self.kernel.pcbTable.pcbRunning.direction, "C:/virus/It's a Virus!.exe")

    def test_should_a_process_be_created_and_be_handled_to_the_scheduler_and_there_is_a_process_with_more_priority_running_it_goes_to_the_queue(self):
        # Setup
        self.kernel.execute("C:/bios/Flash.exe", PCBPriority.HIGH)

        # we set up a program, while program2 runs, once program3 is created it should go to the queue as it has
        # a lower priority than the one currently running
        HARDWARE.clock.do_ticks(5)

        self.kernel.execute("C:/system/private/risk/troyanWar.exe", PCBPriority.LOW)
        HARDWARE.clock.do_ticks(2)

        # We check that the ready queue is as expected, with program on the readyQueue
        # And program2 currently running
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue())
        self.assertEqual(self.kernel.scheduler.readyQueue[PCBPriority.LOW.value - 1][0].direction,
                         "C:/system/private/risk/troyanWar.exe")
        self.assertEqual(self.kernel.pcbTable.pcbRunning.direction, "C:/bios/Flash.exe")

    def test_should_a_process_be_created_and_be_handled_to_the_scheduler__and_it_has_more_priority_than_the_one_that_its_running_it_goes_to_cpu_and_the_other_to_the_queue(self):
        # Setup
        self.kernel.execute("C:/bios/Flash.exe", PCBPriority.LOW)

        # we set up a program, while program2 runs, once program3 is created it should go to cpu  as it has
        # a higher priority than the one currently running
        HARDWARE.clock.do_ticks(5)

        self.kernel.execute("C:/system/private/risk/troyanWar.exe", PCBPriority.HIGH)
        HARDWARE.clock.do_ticks(3)

        # We check that the ready queue is as expected, with program on the readyQueue
        # And program3 currently running
        self.assertFalse(self.kernel.scheduler.isEmptyReadyQueue())
        self.assertEqual(self.kernel.scheduler.readyQueue[PCBPriority.LOW.value - 1][0].direction,
                         "C:/bios/Flash.exe")
        self.assertEqual(self.kernel.pcbTable.pcbRunning.direction, "C:/system/private/risk/troyanWar.exe")
