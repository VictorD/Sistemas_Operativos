from unittest import TestCase

from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.So.Loader import Loader
from src.So.Pcb.PCBPriority import PCBPriority
from src.So.Pcb.PcbState import PcbState
from src.So.Pcb.PcbTable import PcbTable
from src.So.Program import Program


class TestPcbTable(TestCase):

    def setUp(self):
        self.pcbTableSUT = PcbTable()

    def test_if_two_PCB_are_created_with_an_Empty_PCBtable_they_should_have_pid_0_and_pid_1(self):
        # Setup
        # Exercise
        self.pcbTableSUT.createNewPcb("Mi direccion 1", PCBPriority.MEDIUM)
        self.pcbTableSUT.createNewPcb("Yo no tengo Direccion", PCBPriority.MEDIUM)

        pcbPID0 = self.pcbTableSUT._pcbTable.get(0)
        pcbPID1 = self.pcbTableSUT._pcbTable.get(1)

        # Test
        self.assertEqual(0, pcbPID0.pid)
        self.assertEqual(0, pcbPID0.baseAddress)
        self.assertEqual(0, pcbPID0.size)
        self.assertEqual(PcbState.NEW, pcbPID0.state)
        self.assertEqual(PcbState.NEW, pcbPID1.state)
        self.assertEqual(0, pcbPID1.size)
        self.assertEqual(0, pcbPID1.baseAddress)
        self.assertEqual(1, pcbPID1.pid)
        self.assertEqual({0: pcbPID0, 1: pcbPID1}, self.pcbTableSUT._pcbTable)

    def test_when_two_programs_are_created_and_loaded_into_an_empty_memory_the_first_pcb_must_have_baseAddress_0_and_size_3_and_the_second_baseAddress_3_and_size_7(self):
        # Setup
        HARDWARE.setup(40)
        loaderSUT   = Loader()
        dogOne      = Program("doggyTest.dog", [ASM.CPU(2), ASM.IO()])
        HARDWARE.harddisk.newDirectory("C:/perrenomiun")
        HARDWARE.harddisk.saveProgram(dogOne, "C:/perrenomiun")
        pcb1        = self.pcbTableSUT.createNewPcb("C:/perrenomiun/doggyTest.dog", PCBPriority.MEDIUM)

        dogTwo      = Program("DogVirus.dog", [ASM.CPU(7)])
        HARDWARE.harddisk.newDirectory("C:/perrenomiun/Virus Time")
        HARDWARE.harddisk.saveProgram(dogTwo, "C:/perrenomiun/Virus Time")
        pcb2    = self.pcbTableSUT.createNewPcb("C:/perrenomiun/Virus Time/DogVirus.dog", PCBPriority.MEDIUM)

        # Exercise
        loaderSUT.loadProgram(pcb1)
        loaderSUT.loadProgram(pcb2)

        #Test
        self.assertEqual(0, pcb1.baseAddress)
        self.assertEqual(4, pcb1.size)

        self.assertEqual(4, pcb2.baseAddress)
        self.assertEqual(8, pcb2.size)
        self.assertEqual(12, loaderSUT.addressToAssign)

    def tearDown(self):
        pass
