from src.GanttDiagram.GanttDiagram import GanttDiagram
from src.Hardware.Asm import ASM
from src.Hardware.Hardware import *
from src.So.Kernel import Kernel
from src.So.Pcb.PCBPriority import PCBPriority
from src.So.Program import Program
from src import log

##
##  MAIN 
##
if __name__ == '__main__':
    log.setupLogger()
    log.logger.info('Starting emulator')

    ## setup our hardware and set memory size to 25 "cells"
    HARDWARE.setup(25)

    ## new create the Operative System Kernel
    kernel          = Kernel()
    # kernel          = Kernel.withPriorityNoExpropiativeScheduler()
    # kernel          = Kernel.withRoundRobinScheduler(3)
    # kernel          = Kernel.withPriorityExpropiativeScheduler()
    # kernel          = Kernel.withMultilevelQueueScheduler(3)
    diagramaDeGantt  = GanttDiagram.setGanttChart(kernel)

    ##  create a program
    prg1 = Program("prg1.dog", [ASM.CPU(2), ASM.IO(), ASM.CPU(3), ASM.IO(), ASM.CPU(2)])
    prg2 = Program("prg2.dog", [ASM.CPU(4), ASM.IO(), ASM.CPU(1)])
    prg3 = Program("prg3.dog", [ASM.CPU(3)])
    prg4 = Program("Shutdown.dog",[ASM.SD()])

    ## Create Directories in hard disk and save the programs there

    HARDWARE.harddisk.newDirectory("C:/programitasPerrunos")
    HARDWARE.harddisk.newDirectory("C:/programitasPerrunos/huesoSabueso")
    HARDWARE.harddisk.newDirectory("C:/programitasPerrunos/woofwoof")
    HARDWARE.harddisk.saveProgram(prg1,"C:/programitasPerrunos")
    HARDWARE.harddisk.saveProgram(prg2, "C:/programitasPerrunos")
    HARDWARE.harddisk.saveProgram(prg3, "C:/programitasPerrunos/huesoSabueso")
    HARDWARE.harddisk.saveProgram(prg4, "C:/programitasPerrunos/woofwoof")

    # execute all programs "concurrently"
    kernel.execute("C:/programitasPerrunos/prg1.dog", PCBPriority.HIGH)
    kernel.execute("C:/programitasPerrunos/prg2.dog", PCBPriority.LOW)
    kernel.execute("C:/programitasPerrunos/huesoSabueso/prg3.dog", PCBPriority.HIGHEST)
    # kernel.execute("C:/programitasPerrunos/woofwoof/Shutdown.dog", PCBPriority.LOWEST)

    ## start
    # HARDWARE.switchOn()
    HARDWARE.clock.do_ticks(21)
    diagramaDeGantt.printResult()