#!/usr/bin/env python
from src import log
from src.Hardware.Hardware import HARDWARE
from src.Hardware.Irq import KILL_INTERRUPTION_TYPE, IO_IN_INTERRUPTION_TYPE, IO_OUT_INTERRUPTION_TYPE, \
    NEW_INTERRUPTION_TYPE, IRQ, SHUTDOWN_INTERRUPTION_TYPE, TIMEOUT_INTERRUPTION_TYPE
from src.Hardware.Timer.Timer import Timer
from src.So.ContextSwitcher.ContextSwitch import ContextSwitch
from src.So.Dispatcher import Dispatcher
from src.So.InterruptionHandlers.ShutDownInterruptionHandler import ShutDownInterruptionHandler
from src.So.InterruptionHandlers.TimeOutInterruptionHandler import TimeOutInterruptionHandler
from src.So.IoDeviceController import IoDeviceController
from src.So.InterruptionHandlers.IoInInterruptionHandler import IoInInterruptionHandler
from src.So.InterruptionHandlers.IoOutInterruptionHandler import IoOutInterruptionHandler
from src.So.InterruptionHandlers.KillInterruptionHandler import KillInterruptionHandler
from src.So.Loader import Loader
from src.So.InterruptionHandlers.NewInterruptionHandler import NewInterruptionHandler
from src.So.Pcb.PCBPriority import PCBPriority
from src.So.Pcb.PcbTable import PcbTable
from src.So.Scheduler.SchedulerFIFO import SchedulerFIFO
from src.So.Scheduler.SchedulerMultilevelQueue import SchedulerMultilevelQueue
from src.So.Scheduler.SchedulerPriorityExp import SchedulerPriorityExp
from src.So.Scheduler.SchedulerPriorityNoExp import SchedulerPriorityNoExp
from src.So.Scheduler.SchedulerRoundRobin import SchedulerRoundRobin
from src.So.Switch.MultilevelQueue.SwitchInMultilevelQueue import SwitchInMultilevelQueue
from src.So.Switch.MultilevelQueue.SwitchOutMultilevelQueue import SwitchOutMultilevelQueue
from src.So.Switch.SwitchInPriorityExpropiative import SwitchInPriorityExpropiative
from src.So.Switch.WithTimer.SwitchInWithTimer import SwitchInWithTimer
from src.So.Switch.WithTimer.SwitchOutWithTimer import SwitchOutWithTimer


# emulates the core of an Operative System
class Kernel():

    def __init__(self):
        ## setup interruption handlers
        killHandler = KillInterruptionHandler(self)
        HARDWARE.interruptVector.register(KILL_INTERRUPTION_TYPE, killHandler)

        ioInHandler = IoInInterruptionHandler(self)
        HARDWARE.interruptVector.register(IO_IN_INTERRUPTION_TYPE, ioInHandler)

        ioOutHandler = IoOutInterruptionHandler(self)
        HARDWARE.interruptVector.register(IO_OUT_INTERRUPTION_TYPE, ioOutHandler)

        newHandler = NewInterruptionHandler(self)
        HARDWARE.interruptVector.register(NEW_INTERRUPTION_TYPE, newHandler)

        shutDownHandler = ShutDownInterruptionHandler(self)
        HARDWARE.interruptVector.register(SHUTDOWN_INTERRUPTION_TYPE, shutDownHandler)

        timeOutHandler = TimeOutInterruptionHandler(self)
        HARDWARE.interruptVector.register(TIMEOUT_INTERRUPTION_TYPE, timeOutHandler)

        ## controls the Hardware's I/O Device
        self._ioDeviceController = IoDeviceController(HARDWARE.ioDevice)

        # Setup of pcbtable
        self._pcbtable = PcbTable()

        # Setup of Scheduler
        self._scheduler = SchedulerFIFO()

        # Setup of dispatcher
        self._dispatcher = Dispatcher()

        # Setup of loader
        self._loader = Loader()

    ## emulates a "system call" for programs execution
    def execute(self, aDirection, aPriority=PCBPriority.MEDIUM):
        newIRQ = IRQ(NEW_INTERRUPTION_TYPE, [aDirection, aPriority])
        HARDWARE.cpu._interruptVector.handle(newIRQ)
        log.logger.info("\nExecuting program in: " + aDirection)
        log.logger.info(HARDWARE)

    # Check if the Cpu is Idle
    def isCPUIdle(self):
        return HARDWARE.cpu.pc == -1

    # Cleans all devices and data types
    def cleanAll(self):
        HARDWARE.memory.cleanAll()
        log.logger.info("MEMORY CLEANED")

        self.pcbTable.cleanAll()
        log.logger.info("PCB TABLE CLEANED")

        self.scheduler.cleanAll()
        log.logger.info("SCHEDULER CLEANED")

        self.ioDeviceController.cleanAll()
        log.logger.info("IO DEVICE CONTROLLERS CLEANED")

        HARDWARE.timer.setOff()

    # ShutDowns the machine
    def shutDown(self):
        HARDWARE.switchOff()

    # ----- Getters & Setters ----- #
    @property
    def ioDeviceController(self):
        return self._ioDeviceController

    @property
    def pcbTable(self):
        return self._pcbtable

    @property
    def scheduler(self):
        return self._scheduler

    @property
    def dispatcher(self):
        return self._dispatcher

    @property
    def loader(self):
        return self._loader

    @scheduler.setter
    def scheduler(self, aScheduler):
        self._scheduler = aScheduler
    # ----- End of Getters & Setters ----- #

    def __repr__(self):
        return "Kernel "

    # ----- Class Methods ----- #
    @classmethod
    def withRoundRobinScheduler(cls, aQuantum):
        newKernel                       = Kernel()
        HARDWARE.timer                  = Timer(HARDWARE.cpu, aQuantum)
        HARDWARE.clock._subscribers[0]  = HARDWARE.timer
        newKernel.scheduler             = SchedulerRoundRobin(aQuantum)
        HARDWARE.timer.setOn()
        ContextSwitch().setSwitchOut(SwitchOutWithTimer())
        ContextSwitch().setSwitchIn(SwitchInWithTimer())

        return newKernel

    @classmethod
    def withPriorityNoExpropiativeScheduler(cls):
        newKernel           = Kernel()
        newKernel.scheduler = SchedulerPriorityNoExp()
        return newKernel

    @classmethod
    def withPriorityExpropiativeScheduler(cls):
        newKernel                   = Kernel()
        newKernel.scheduler         = SchedulerPriorityExp()
        ContextSwitch().setSwitchIn(SwitchInPriorityExpropiative())

        return newKernel

    @classmethod
    def withMultilevelQueueScheduler(self, aQuantum):
        newKernel                       = Kernel()
        newKernel.scheduler             = SchedulerMultilevelQueue(aQuantum)
        HARDWARE.timer = Timer(HARDWARE.cpu, aQuantum)
        HARDWARE.clock._subscribers[0] = HARDWARE.timer
        ContextSwitch().setSwitchOut(SwitchOutMultilevelQueue())
        ContextSwitch().setSwitchIn(SwitchInMultilevelQueue())

        return newKernel
    # ----- End of Class Methods ----- #
