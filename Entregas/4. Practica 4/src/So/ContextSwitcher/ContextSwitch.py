from src.So.Switch.SwitchIn import SwitchIn
from src.So.Switch.SwitchOut import SwitchOut


class ContextSwitch(object):

    _switchOut  = SwitchOut()
    _switchIn   = SwitchIn()

    # ---DEFINITION OF SINGLETON--- #
    __instance = None

    def __new__(cls):
        if ContextSwitch.__instance is None:
            ContextSwitch.__instance = object.__new__(cls)
        return ContextSwitch.__instance
    # ---END OF THE DEFINITION--- #

    def toCPUCase(self):
        return self._switchIn

    def outOfCPUCase(self):
        return self._switchOut

    def setSwitchOut(self, aSwitchOut):
        self._switchOut = aSwitchOut

    def setSwitchIn(self, aSwitchIn):
        self._switchIn = aSwitchIn

    @classmethod
    def kill(cls):
        cls.__instance = None
