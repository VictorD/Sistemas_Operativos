from src.So.Switch.MultilevelQueue.AbstractSwitchMultilevelQueue import AbstractSwitchMultilevelQueue
from src.So.Switch.SwitchIn import SwitchIn


class SwitchInMultilevelQueue(SwitchIn, AbstractSwitchMultilevelQueue):

    # ---DEFINITION OF SINGLETON--- #
    __instance = None

    def __new__(cls):
        if SwitchInMultilevelQueue.__instance is None:
            SwitchInMultilevelQueue.__instance = object.__new__(cls)
        return SwitchInMultilevelQueue.__instance
    # ---END OF THE DEFINITION--- #

    def addInReadyQueue(self, kernel, aPCB):
        pcbToReadyQueue = aPCB
        if aPCB.hasMoreLevelPriorityThat(kernel.pcbTable.pcbRunning):
            pcbToReadyQueue = kernel.pcbTable.pcbRunning
            kernel.dispatcher.save(pcbToReadyQueue)
            self.loadToRun(kernel, aPCB)
        super().addInReadyQueue(kernel, pcbToReadyQueue)

    def loadToRun(self, kernel, aPCB):
        self.resolveTimerFor(aPCB)
        super().loadToRun(kernel, aPCB)

    def __repr__(self):
        return "Multilevel Queue SwitchIn"
