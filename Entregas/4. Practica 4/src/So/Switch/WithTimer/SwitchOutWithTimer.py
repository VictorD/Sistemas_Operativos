from src.Hardware.Hardware import HARDWARE
from src.So.Switch.SwitchOut import SwitchOut


class SwitchOutWithTimer(SwitchOut):

    def addInReadyQueue(self, kernel, aPCB=None):
        HARDWARE.timer.resetTimeOutCounterIfNeed()

    def loadToRun(self, kernel, aPCB=None):
        HARDWARE.timer.resetTimeOutCounterIfNeed()
        super().loadToRun(kernel, aPCB)
