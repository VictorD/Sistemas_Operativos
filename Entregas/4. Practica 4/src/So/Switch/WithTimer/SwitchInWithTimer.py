from src.Hardware.Hardware import HARDWARE
from src.So.Switch.SwitchIn import SwitchIn


class SwitchInWithTimer(SwitchIn):

    def loadToRun(self, kernel, aPCB):
        HARDWARE.timer.resetTimeOutCounterIfNeed()
        super().loadToRun(kernel, aPCB)
