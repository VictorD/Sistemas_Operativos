from src.So.Switch.Switch import Switch


class SwitchIn(Switch):

    # ---DEFINITION OF SINGLETON--- #
    __instance      = None

    def __new__(cls):
        if SwitchIn.__instance is None:
            SwitchIn.__instance = object.__new__(cls)
        return SwitchIn.__instance
    # ---END OF THE DEFINITION--- #

    def addInReadyQueue(self, kernel, aPCB):
        aPCB.ready()
        # if (kernel.scheduler.isExpropiative()):
        #     currentlyRunning = kernel.pcbTable.pcbRunning
        #     kernel.dispatcher.save(currentlyRunning)
        #     currentlyRunning.ready()
        #     pcbToCpu = kernel.scheduler.addAndReturn(aPCB, currentlyRunning)
        #     self.loadToRun(kernel, pcbToCpu)
        # else:
        #     kernel.scheduler.add(aPCB)
        kernel.scheduler.add(aPCB)

    def __repr__(self):
        return "Switch In"