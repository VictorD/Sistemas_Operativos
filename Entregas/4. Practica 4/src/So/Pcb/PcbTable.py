from src.So.Pcb.Pcb import PCB

# Class that has the responsibility of managing PCB
class PcbTable():

    def __init__(self):
        self._pidAcumulator = 0
        self._pcbTable      = {}
        self._pcbRunning = None

    def createNewPcb(self, aDirection, aPriority):
        newPCB          = PCB(self._pidAcumulator, aDirection)
        newPCB.priority = aPriority
        self._pcbTable.setdefault(self._pidAcumulator, newPCB)
        self._pidAcumulator += 1
        return newPCB

    def hasNotRunningPCB(self):
        return self.pcbRunning is None

    def putRunningPCBinNone(self):
        self.pcbRunning = None

    def hasRunningPCB(self):
        return not self.hasNotRunningPCB()

    def cleanAll(self):
        self._pidAcumulator = 0
        self._pcbTable = {}
        self.pcbRunning = None

    # ----- Getters & Setters ----- #
    @property
    def pcbRunning(self):
        return self._pcbRunning

    @pcbRunning.setter
    def pcbRunning(self, apcb):
        self._pcbRunning = apcb
