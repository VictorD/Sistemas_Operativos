from src import log
from src.So.InterruptionHandlers.AbstractInterruptionHandler import AbstractInterruptionHandler


class ShutDownInterruptionHandler(AbstractInterruptionHandler):

    def execute(self, irq):
        log.logger.info(" ... Now Shutting Down ... ")
        self.kernel.cleanAll()
        self.kernel.shutDown()




