from src import log
from src.So.InterruptionHandlers.AbstractInterruptionHandler import AbstractInterruptionHandler


class IoInInterruptionHandler(AbstractInterruptionHandler):

    def execute(self, irq):
        log.logger.info(self.kernel.ioDeviceController)
        operation = irq.parameters  # Es la operacion de IO que se va a ejecutar

        pcbRunning = self.kernel.pcbTable.pcbRunning
        self.saveStateAndResetRunningPCB(pcbRunning)

        pcbRunning.waiting()
        self.contextSwitchOutOfCPU()
        self.kernel.ioDeviceController.runOperation(pcbRunning, operation)



