from src.Hardware.Hardware import HARDWARE
from src.So.InterruptionHandlers.AbstractInterruptionHandler import AbstractInterruptionHandler


class TimeOutInterruptionHandler(AbstractInterruptionHandler):

    def execute(self, irq):
        if self.hasReadyPCBsOrPCBRunning():

            aPcb   = self.kernel.pcbTable.pcbRunning
            self.saveStateAndResetRunningPCB(aPcb)

            aPcb.ready()
            self.kernel.scheduler.add(aPcb)
            self.contextSwitchOutOfCPU()

        HARDWARE.timer._timeOutCounter = 1 # Se setea en 1 por que una vez cargado el proximo PCB corre su instruccion

    def hasReadyPCBsOrPCBRunning(self):
        return (not self.kernel.scheduler.isEmptyReadyQueue()) or (not self.kernel.pcbTable.hasNotRunningPCB())
