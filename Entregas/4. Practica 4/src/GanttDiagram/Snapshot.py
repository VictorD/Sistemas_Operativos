from src.GanttDiagram.DummyPCB import DummyPCB


class Snapshot():

    def __init__(self, aTickNumber):
        self._tick          = aTickNumber
        self._pcbList       = []
        self._quantityOfPCB = 0

    @classmethod
    def takeSnapshot(self, tickNbr, aPcbTable):
        newSnapshot = Snapshot(tickNbr)
        newSnapshot.__doDummyPCBList(aPcbTable)
        return newSnapshot

    def __doDummyPCBList(self, aPcbTable):
        self._pcbList = [self.__createDummyPCB(v) for v in aPcbTable._pcbTable.values()]

    def __createDummyPCB(self, v):
        self._quantityOfPCB += 1
        return DummyPCB(v.pid, v.state)

    def quantityOfReadyState(self):
        return len(list(filter(lambda x: x.isReady(), self._pcbList)))

    def quantityOfPCB(self):
        return self._quantityOfPCB

    def listOfState(self):
        return [v._state.value for v in self._pcbList]
