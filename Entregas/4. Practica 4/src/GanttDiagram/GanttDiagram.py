from tabulate import tabulate

from src.GanttDiagram.Snapshot import Snapshot
from src.Hardware.Hardware import HARDWARE


class GanttDiagram():

    def __init__(self, pcbTable):
        self._lastTick       = 0
        self._pcbList        = pcbTable
        self._snapshotList   = []
        self._quantityOfPCB  = 0

    @classmethod
    def setGanttChart(self, aKernel):
        newGanttChart           = GanttDiagram(aKernel.pcbTable)
        HARDWARE.clock.addSubscriber(newGanttChart)
        return newGanttChart

    def tick(self, tickNbr):
        newSnapshot = Snapshot.takeSnapshot(tickNbr, self._pcbList)
        self._quantityOfPCB = max(self._quantityOfPCB, newSnapshot.quantityOfPCB())
        self._snapshotList.append(newSnapshot)
        self._lastTick += 1

    def calculateGantDiagram(self):
        return round(
                    sum(
                        map(
                            lambda x : x.quantityOfReadyState(), self._snapshotList
                            )
                        ) / self._quantityOfPCB
                    , 2)

    def printResult(self):
        ticksList = map(lambda x : f"Tick {x}", range(0, self._lastTick))
        listasDePcb = list(self._pcbList._pcbTable.values())
        tabla1      = zip(* [v.listOfState() for v in self._snapshotList])

        print("Gantt Chart")

        print(tabulate(tabla1, headers=ticksList, showindex=listasDePcb, tablefmt="grid"))

        print(f"Average Time: {self.calculateGantDiagram()} ")

