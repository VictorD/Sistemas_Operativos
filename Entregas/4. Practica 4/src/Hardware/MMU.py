from builtins import Exception

## emulates the Memory Management Unit (MMU)

class MMU():

    def __init__(self, memory):
        self._memory = memory
        self._baseDir = 0
        self._limit = 999

    @property
    def limit(self):
        return self._limit

    @limit.setter
    def limit(self, limit):
        self._limit = limit

    @property
    def baseDir(self):
        return self._baseDir

    @baseDir.setter
    def baseDir(self, baseDir):
        self._baseDir = baseDir

    def fetch(self,  logicalAddress):
        if (logicalAddress >= self._limit):
            raise Exception("Invalid Address,  {logicalAddress} is eq or higher than process limit: {limit}".format(limit = self._limit, logicalAddress = logicalAddress))
        else:
            physicalAddress = logicalAddress + self._baseDir
            return self._memory.get(physicalAddress)
