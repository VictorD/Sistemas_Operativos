from unittest import TestCase
from unittest.mock import patch
from src.hardware.Asm import ASM
from src.hardware.Hardware import HARDWARE
from src.so.Kernel import Kernel
from src.so.Program import Program
import src.log #No Borrar, si se quita rompe el @Patch

class TestKernel(TestCase):

    def setUp(self):
        self.prg    = Program("test.exe", [ASM.CPU(1)])
        self.prg2   = Program("test.exe", [ASM.CPU(1), ASM.IO()])
        self.kernel = Kernel()
        HARDWARE.setup(20)

    @patch('src.log.logger.info')
    def test_execute_an_program_with_two_instructions(self, mock_info):

        #excerice
        self.kernel.execute(self.prg)

        #assert
        self.assertEqual(4, mock_info.call_count)

    @patch('src.log.logger.info')
    def test_executeBatch(self,mock_info):
        # setUp
        batch   = [self.prg,self.prg2]

        # excerice
        self.kernel.executeBatch(batch)

        # assert
        self.assertEqual(9, mock_info.call_count)

    def tearDown(self):
        pass