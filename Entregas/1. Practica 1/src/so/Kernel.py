from time import sleep
from src import log
from src.hardware.Hardware import *


# emulates the core of an Operative System

class Kernel:

    def __init__(self):
        pass

    def load_program(self, program):
        # loads the program in main memory
        progSize = len(program.instructions)
        for index in range(0, progSize):
            inst = program.instructions[index]
            HARDWARE.memory.put(index, inst)

    ## emulates a "system call" for programs execution
    def execute(self, program):
        self.load_program(program)
        log.logger.info("\n Executing program: {name}".format(name=program.name))
        log.logger.info(HARDWARE)

        # set CPU program counter at program's first intruction
        HARDWARE.cpu.pc = 0
        progSize = len(program.instructions)
        for i in range(0, progSize):
            HARDWARE.cpu.tick(i)
            sleep(1)

    # Executes all programs in a batch in secuential order.
    def executeBatch(self, batch):
        for program in batch:
                self.execute(program)

    def __repr__(self):
        return "Kernel "
