from src.hardware.Memory import Memory
from src.hardware.Cpu import Cpu


## emulates the Hardware that were the Operative System run
class Hardware:

    ## Setup our hardware
    def setup(self, memorySize):
        ## add the components to the "motherboard"
        self._memory = Memory(memorySize)

        ## "wire" the components each others
        self._cpu = Cpu(self._memory)

    @property
    def cpu(self):
        return self._cpu

    @property
    def memory(self):
        return self._memory

    def __repr__(self):
        return "HARDWARE state {cpu}\n{mem}".format(cpu=self._cpu, mem=self._memory)


### HARDWARE is a global variable
### can be access from any
HARDWARE = Hardware()