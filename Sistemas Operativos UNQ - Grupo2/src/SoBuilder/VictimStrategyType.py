from enum import Enum


class VictimStrategyType(Enum):

    FCFS        = "FCFS"
    LRU         = "LRU"
    SECONDCHANCE= "SECONDCHANCE"
