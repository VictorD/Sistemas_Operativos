from src.So.Pcb.PcbState import PcbState


class DummyPCB():

    def __init__(self, apid, aState):
        self._pid   = apid
        self._state = aState

    def isReady(self):
        return self._state == PcbState.READY

    def __str__(self):
        return f"PCB {self._pid}"

    ##METODO PARA UI##
    @property
    def state(self):
        return self._state
