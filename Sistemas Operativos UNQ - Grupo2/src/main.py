from src.GanttDiagram.GanttDiagram import GanttDiagram
from src.Hardware.Asm import ASM
from src.Hardware.Hardware import *
from src.So.Pcb.PCBPriority import PCBPriority
from src.So.Program import Program
from src import log
from src.So.Segment import Segment
from src.SoBuilder.MemoryType import MemoryType
from src.SoBuilder.SchedulerType import SchedulerType
from src.SoBuilder.SoBuilder import SoBuilder

##
##  MAIN 
##
if __name__ == '__main__':
    log.setupLogger()
    log.logger.info('Starting emulator')

    ## setup our hardware and set memory size to 120 "cells"
    HARDWARE.setup(24)
    # HARDWARE.setup(125)

    ## new create the Operative System Kernel
    # kernel          = SoBuilder.createKernel(SchedulerType.FIFO, MemoryType.SIMPLE)
    # kernel          = SoBuilder.createKernel(SchedulerType.PRIORITYNOEXPROPIATIVE, MemoryType.SIMPLE)
    # kernel          = SoBuilder.createKernel(SchedulerType.ROUNDROBIN, MemoryType.SIMPLE, 3)
    # kernel          = SoBuilder.createKernel(SchedulerType.PRIORITYEXPROPIATIVE, MemoryType.SIMPLE)
    kernel          = SoBuilder.createKernel(SchedulerType.MULTILEVELFEEDBACKQUEUE, MemoryType.ONDEMAND, 4,4)
    # kernel          = SoBuilder.createKernel(SchedulerType.MULTILEVELFEEDBACKQUEUE, MemoryType.SIMPLE)

    # kernel          = SoBuilder.createKernel(SchedulerType.FIFO, MemoryType.PAGING, aFrameSize=5)
    # kernel          = SoBuilder.createKernel(SchedulerType.PRIORITYNOEXPROPIATIVE, MemoryType.PAGING, aFrameSize=5)
    # kernel          = SoBuilder.createKernel(SchedulerType.ROUNDROBIN, MemoryType.PAGING, 3, 5)
    # kernel          = SoBuilder.createKernel(SchedulerType.PRIORITYEXPROPIATIVE, MemoryType.PAGING, aFrameSize=5)
    # kernel          = SoBuilder.createKernel(SchedulerType.MULTILEVELQUEUE, MemoryType.PAGING, 3, 5)
    # kernel          = SoBuilder.createKernel(SchedulerType.MULTILEVELFEEDBACKQUEUE, MemoryType.PAGING, aFrameSize=5)

    # kernel = SoBuilder.createKernel(SchedulerType.FIFO, MemoryType.ONDEMAND, aFrameSize=5)
    # kernel          = SoBuilder.createKernel(SchedulerType.ROUNDROBIN, MemoryType.ONDEMAND, 4, 5)

    diagramaDeGantt = GanttDiagram.setGanttChart(kernel)

    ##  create a program
    segment1prg1 = Segment([ASM.CPU(2), ASM.IO(), ASM.CPU(3), ASM.IO(), ASM.CPU(2),ASM.EXIT(1)])
    prg1 = Program("prg1.dog",[segment1prg1])

    segment1prg2 = Segment([ASM.VARIABLE(1)],"WRITE")
    segment2prg2 = Segment([ASM.CPU(4), ASM.IO(), ASM.CPU(1), ASM.SETVARIABLE('0'),ASM.EXIT(1)])
    prg2 = Program("prg2.dog", [segment1prg2,segment2prg2])

    segment1prg3 = Segment([ASM.VARIABLE(3)],"WRITE")
    segment2prg3 = Segment([ASM.CPU(3), ASM.SETVARIABLE('0'),ASM.SETVARIABLE('1'),ASM.SETVARIABLE('2'),ASM.EXIT(1)])
    prg3 = Program("prg3.dog", [segment1prg3,segment2prg3])

    segment1prg4 = Segment([ASM.CPU(15), ASM.IO(),ASM.EXIT(1)])
    prg4 = Program("prg4.dog", [segment1prg4])

    segment1prg5 = Segment([ASM.VARIABLE(1)],"WRITE")
    segment2prg5 = Segment([ASM.CPU(2), ASM.IO(), ASM.SETVARIABLE('0'),ASM.EXIT(1)])
    prg5 = Program("prg5.dog", [segment1prg5,segment2prg5])

    ## register new user and log in
    userManager= HARDWARE.harddisk.userManager
    userManager.registerUser("pepita","vuelavuela")
    userManager.logInUser("pepita","vuelavuela")
    print(userManager.getCurrentUserName())

    ## Create Directories in hard disk and save the programs there
    HARDWARE.harddisk.newDirectory("C:/programitasPerrunos")
    HARDWARE.harddisk.newDirectory("C:/programitasPerrunos/huesoSabueso")
    HARDWARE.harddisk.newDirectory("C:/programitasPerrunos/woofwoof")
    HARDWARE.harddisk.newDirectoryWithPermision("C:/programitasPerrunos/woofwoof/pepitaonly","pepita")


    HARDWARE.harddisk.saveProgram(prg1, "C:/programitasPerrunos")
    HARDWARE.harddisk.saveProgram(prg2, "C:/programitasPerrunos")
    HARDWARE.harddisk.saveProgram(prg3, "C:/programitasPerrunos/huesoSabueso")
    HARDWARE.harddisk.saveProgram(prg4, "C:/programitasPerrunos/woofwoof")
    HARDWARE.harddisk.saveProgram(prg5, "C:/programitasPerrunos/woofwoof/pepitaonly")

    # execute all programs "concurrently"
    kernel.execute("C:/programitasPerrunos/prg1.dog", PCBPriority.HIGH)
    kernel.execute("C:/programitasPerrunos/prg2.dog", PCBPriority.LOW)
    kernel.execute("C:/programitasPerrunos/huesoSabueso/prg3.dog", PCBPriority.HIGHEST)
    kernel.execute("C:/programitasPerrunos/woofwoof/prg4.dog", PCBPriority.LOWEST)
    kernel.execute("C:/programitasPerrunos/woofwoof/pepitaonly/prg5.dog", PCBPriority.LOWEST)

    ## start
    # HARDWARE.switchOn()
    # HARDWARE.clock.do_ticks(22)
    HARDWARE.clock.do_ticks(60)
    diagramaDeGantt.printResult()