
class StatisticsCollector():

    def __init__(self):
        self.__pageFaultCounter   = 0
        self.__swapInCounter      = 0
        self.__swapOutCounter     = 0
        self.__victimCounter      = 0


    def countPageFault(self):
        self.__pageFaultCounter += 1

    def countSwapIn(self):
        self.__swapInCounter += 1

    def countSwapOut(self):
        self.__swapOutCounter += 1

    def countVictim(self):
        self.__victimCounter += 1

    @property
    def pageFaultCounter(self):
        return self.__pageFaultCounter

    @property
    def swapInCounter(self):
        return self.__swapInCounter

    @property
    def swapOutCounter(self):
        return self.__swapOutCounter

    @property
    def victimCounter(self):
        return self.__victimCounter

STATISTICSCOLLECTOR = StatisticsCollector()