from src.Hardware.HardDisk.Directory import Directory
from src.Hardware.HardDisk.HardDisk import HardDisk
from src.Hardware.HardDisk.UserManager import UserManager
from src.So.File import File


class FileSystem():

    def __init__(self):
            self._root = Directory("C:/")
            self._hardDisk = HardDisk()
            self._userManager = UserManager()

    @property
    def root(self):
        return self._root

    @property
    def userManager(self):
        return self._userManager

    @property
    def hardDisk(self):
        return self._hardDisk

    def readProgram(self, aDirection):

        if (self.hasPermisionToAccessDirection(aDirection)):
            split = aDirection.split("/")
            programPointer = self.root.fetchProgram(split)
            return self.hardDisk.fetchProgram(programPointer)
        else:
            pass


    def readInstructionsOfProgram(self,aDirection,init,end):
        split = aDirection.split("/")
        programPointer = self.root.fetchProgram(split)
        program = self.hardDisk.fetchProgram(programPointer)
        return program.data[init:end]


    def saveProgram(self,aProgram,aDirection):
        if (self.hasPermisionToAccessDirection(aDirection)):
            split = aDirection.split("/")
            programName= aProgram.name
            pointer = self._hardDisk.getPointer()
            self.root.saveProgram(pointer, programName,split)

            ##file = File(aProgram.name)
            ##file.data = aProgram.instructions

            self._hardDisk.saveProgram(pointer, aProgram)
        else:
            pass

    def newDirectory(self,aDirection):
        split = aDirection.split("/")
        self.root.newRute(split)

    def newDirectoryWithPermision(self,aDirection,anUserName):
        split = aDirection.split("/")
        self.root.newRuteWithPermision(split,anUserName)

    def putinSwap(self,addresToWrite, someInstructions):
        self.hardDisk._swap.put(addresToWrite, someInstructions)

    def getfromSwap(self,addresToRead):
        return self.hardDisk._swap.get(addresToRead)

    def hasPermisionToAccessDirection(self, aDirection):
        split = aDirection.split("/")
        userName = self._userManager.getCurrentUserName()
        return self.root.hasPermissionToAccess(userName, split)

