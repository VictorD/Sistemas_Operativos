

class UserManager():

    def __init__(self):
        self._registeredUsers = {}
        self._currentlyLoggedUser ="none"

    def registerUser(self,anUsername,aPassowrd):
        if  anUsername not in self._registeredUsers.keys():
            self._registeredUsers[anUsername] = aPassowrd

    def logInUser(self, aUsername, aPassword):
       if self._registeredUsers[aUsername] == aPassword:
           self._currentlyLoggedUser = aUsername

    def getCurrentUserName(self):
        return self._currentlyLoggedUser
