from src.So.File import File


class Directory():

    def __init__(self, name):
            self._name = name
            self._directories = {}
            self._programs = {}
            self._permisions =  []

    @property
    def name(self):
        return self._name

    @property
    def directories(self):
        return self._directories

    @property
    def programs(self):
        return self._programs

    def fetchProgram(self,aDirection):
        aDirection.pop(0)

        if len(aDirection) == 1:
            return self.programs[aDirection[0]]
        else:
            return self.directories[aDirection[0]].fetchProgram(aDirection)

    def newRute(self, aDirection):
        aDirection.pop(0)

        if len(aDirection) == 1:
            self.newDirectory(aDirection[0])
        elif self.hasDirection(aDirection[0]):
            self.directories[aDirection[0]].newRute(aDirection)
        else:
            self.newDirectory(aDirection[0])
            self.directories[aDirection[0]].newRute(aDirection)

    def hasDirection(self, aDirection):
        return aDirection in self.directories

    def newDirectory(self, aDirection):
        self.directories[aDirection] = Directory(aDirection)

    def newDirectoryWithPermison(self, aDirection, anUserName):
        directory =  Directory(aDirection)
        directory.addPermission(anUserName)
        self.directories[aDirection] =directory


    def saveProgram(self, pointer,programName,aDirection):
        aDirection.pop(0)

        if len(aDirection) == 0:
            self.programs[programName]= pointer
        else:
            self.directories[aDirection[0]].saveProgram(pointer, programName, aDirection)

    def newRuteWithPermision(self, aDirection, anUserName):
        if (self.hasPermission(anUserName)):
            aDirection.pop(0)

            if len(aDirection) == 1:
                self.newDirectoryWithPermison(aDirection[0],anUserName)
            elif self.hasDirection(aDirection[0]):
                self.directories[aDirection[0]].newRuteWithPermision(aDirection,anUserName)
            else:
                self.newDirectoryWithPermison(aDirection[0], anUserName)
                self.directories[aDirection[0]].newRuteWithPermision(aDirection, anUserName)
        else:
            pass

    def hasPermission(self, anUserName):
        return self._permisions == [] or anUserName in self._permisions

    def addPermission(self, anUserName):
        self._permisions.append(anUserName)

    def hasPermissionToAccess(self, userName, aDirection):
        if (self.hasPermission(userName)):
            aDirection.pop(0)

            if len(aDirection) == 1:
                lastDirection= aDirection.pop(0)
                if lastDirection in self._programs:
                    return True
                elif lastDirection in self._directories:
                    return self._directories[lastDirection].hasPermission(userName)
                else:
                    return False

            else:
                return self.directories[aDirection[0]].hasPermissionToAccess(userName, aDirection)

        else:
            return False