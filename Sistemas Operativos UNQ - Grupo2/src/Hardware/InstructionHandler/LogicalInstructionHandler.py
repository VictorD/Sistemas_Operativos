from src import log
from src.Hardware.Irq import IRQ, SHUTDOWN_INTERRUPTION_TYPE, KILL_INTERRUPTION_TYPE, IO_IN_INTERRUPTION_TYPE, \
    SET_IN_INTERRUPTION_TYPE


class LogicalInstructionHandler(object):

    # ---DEFINITION OF SINGLETON--- #
    __instance      = None

    def __new__(cls):
        if LogicalInstructionHandler.__instance is None:
            LogicalInstructionHandler.__instance = object.__new__(cls)
        return LogicalInstructionHandler.__instance
    # ---END OF THE DEFINITION--- #

    def __init__(self):
        self._cases = {'IO': self.__handleIoInInstruction, 'CPU': self.__handleCpuInstruction, 'VARIABLE': self.__handleCpuInstruction,
                       'EXIT': self.__handleExitInstruction, 'SD': self.__handleShutdownInstruction}

    def executeInstruction(self, aCpu):
        if 'SET' not in aCpu._ir:
            self._cases[aCpu._ir](aCpu)
        else:
            self.handleSetVariable(aCpu)


    def __handleShutdownInstruction(self, aCpu):
        ShutdownIRQ = IRQ(SHUTDOWN_INTERRUPTION_TYPE)
        aCpu._interruptVector.handle(ShutdownIRQ)

    def __handleExitInstruction(self, aCpu):
        killIRQ = IRQ(KILL_INTERRUPTION_TYPE)
        aCpu._interruptVector.handle(killIRQ)

    def __handleIoInInstruction(self, aCpu):
        ioInIRQ = IRQ(IO_IN_INTERRUPTION_TYPE, aCpu._ir)
        aCpu._interruptVector.handle(ioInIRQ)

    def __handleCpuInstruction(self, aCpu):
        log.logger.info("cpu - Exec: {instr}, PC={pc}".format(instr=aCpu._ir, pc=aCpu._pc))

    def handleSetVariable(self, aCpu):
        setIRQ = IRQ(SET_IN_INTERRUPTION_TYPE, int (aCpu._ir[-1:]))
        aCpu._interruptVector.handle(setIRQ)
