from src.Hardware.MMU.AbstractMMU import AbstractMMU
from src.So.PageTable import PageTable

## emulates the Memory Management Unit (MMU)
class MMUPagination(AbstractMMU):

    def __init__(self, aFrameSize, aMemory):
        super().__init__(aMemory)
        self._frameSize                     = aFrameSize
        self._currentProgramPaginationTable = PageTable()

    def calculateAddress(self, anLogicalAddress):
        logicalAddressCalculated= divmod(anLogicalAddress, self._frameSize)
        page                    = int(logicalAddressCalculated[0])
        offset                  = int(logicalAddressCalculated[1])
        return self._currentProgramPaginationTable.frameOf(page) * self._frameSize + offset

    def __repr__(self):
        return "MMU Paging"