from src.Hardware.Hardware import HARDWARE
from src.Hardware.Irq import IRQ, PAGE_FAULT_INTERRUPTION_TYPE
from src.Hardware.MMU.MMUPagination import MMUPagination


class MMUOnDemand(MMUPagination):

    def calculateAddress(self, anLogicalAddress):
        logicalAddressCalculated= divmod(anLogicalAddress, self._frameSize)
        page                    = int(logicalAddressCalculated[0])
        offset                  = int(logicalAddressCalculated[1])
        # Invertir el if no lo haria mas entendible? Solo entraria al if si no hay frame, hace lo que tenga que hacer y continua.
        if self._currentProgramPaginationTable.hasFrame(page):
            self.dirtyPage(page)
            return self._currentProgramPaginationTable.frameOf(page) * self._frameSize + offset
        else:
            pageFaultIRQ = IRQ(PAGE_FAULT_INTERRUPTION_TYPE, [page])
            HARDWARE.cpu._interruptVector.handle(pageFaultIRQ)
            return self.calculateAddress(anLogicalAddress)


    def __repr__(self):
        return "MMU On Demand"

    def dirtyPage(self, page):
        self._currentProgramPaginationTable.setDirty(page, HARDWARE.clock._tickNbr)
