from src.So.SegmentRow import SegmentRow


class SegmentTable():

    def __init__(self):
        self._table     = {}
        self._assocPid = None

    def assocSegments(self, segments):
        for each in segments:
            self.setSegment(each)

    def blockOf(self, segmentid):
        return self._table[segmentid]._block

    def setBlockToSegment(self, aSid, aBlock):
        segmentRow = self._table[aSid]
        segmentRow.registryBlock(aBlock)
        segmentRow._hasBlock = True

    def clearBlocks(self):
        for k in self._table.keys():
            rowToClean = self._table[k]
            rowToClean._block   = None
            rowToClean._hasBlock = False

    ##Returns used blocks
    def getAllBlocks(self):
        blocks = list(map(lambda x: x._block, self._table.values()))
        return list(filter(lambda x: x!=None , blocks))

    def setSegment(self, segment):
        self._table[segment._sid] = SegmentRow(segment)