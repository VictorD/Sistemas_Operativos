from src.So.Scheduler.SchedulerPriorityNoExp import SchedulerPriorityNoExp


class SchedulerPriorityExp(SchedulerPriorityNoExp):

    def __init__(self):
        super().__init__()

    def isExpropiative(self):
        return True

    def addAndReturn(self, aPcb, anotherPCB):
        pcbtoCpu   = anotherPCB
        pcbToQueue = aPcb
        if aPcb.priority.value < anotherPCB.priority.value:
            pcbtoCpu   = aPcb
            pcbToQueue = anotherPCB
        self.add(pcbToQueue)
        return pcbtoCpu

    def __repr__(self):
        return "Priority Expropiative"