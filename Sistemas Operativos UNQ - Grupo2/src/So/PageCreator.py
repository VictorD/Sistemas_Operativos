from src.So.Page import Page


class PageCreator():

    @classmethod
    def createPages(cls, sizeOfInstructions, pageSize):

        listOfPages = []
        pageNumber  = 0
        return cls.listPages(listOfPages, pageNumber, pageSize, sizeOfInstructions)

    @classmethod
    def listPages(cls, listOfPages, pageNumber, pageSize, sizeOfInstructions):
        if sizeOfInstructions > 0:
            listOfPages.append(Page.newPage(pageSize, pageNumber))
            return cls.listPages(listOfPages, pageNumber + 1, pageSize, sizeOfInstructions - pageSize)
        else:
            return listOfPages
