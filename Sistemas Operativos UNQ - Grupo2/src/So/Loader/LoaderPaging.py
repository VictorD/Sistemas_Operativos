import math

from src.Hardware.Hardware import HARDWARE
from src.Hardware.Irq import OUT_OF_MEMORY_INTERRUPTION_TYPE, IRQ
from src.So.Loader.AbstractLoaderPaging import AbstractLoaderPaging


class LoaderPaging(AbstractLoaderPaging):

    # Carga las instrucciones en memoria, segun el frame de la page que tenga asociada
    def load(self, aInstructions, aPageTable):
        # Por cada pagina va a cargar las instrucciones que necesita en memoria segun el frame que le toca
        pageList = aPageTable.getAllPages()

        for aPage in pageList:
            initInstruction = aPage.firstInstruction()    # Calcula la posicion de la primera instrucion
            endInstruction  = aPage.endInstruction()      # Calcula la posicion de la ultima instrucion

            self.write(aInstructions[initInstruction:endInstruction], aPageTable.frameOf(aPage._number))

    # # Crea las paginas necesarias para alojar las instruccioens en memoria
    def createPageTable(self, sizeOfProgram, aPcbPid):
        paginationTable = super().createPageTable(sizeOfProgram, aPcbPid)
        framesToUse     = math.ceil(sizeOfProgram/self._memoryManager._frameSize)
        usedFrames      = self._memoryManager.getFreeFrames(framesToUse)
        paginationTable.assocFrames(usedFrames)
        return paginationTable

    def loadProgram(self, aPCB):
        aProgram = HARDWARE.harddisk.readProgram(aPCB.direction)
        sizeOfProgram = len(aProgram.data)

        if self._memoryManager.hasMemoryFor(sizeOfProgram):
            paginationTable = self.createPageTable(sizeOfProgram, aPCB.pid)  # pagina, pide los frames, crea la tabla y ya la carga en el memorymanager

            self.load(aProgram.data, paginationTable)

            aPCB.size = sizeOfProgram
        else:
            outOfMemoryIRQ = IRQ(OUT_OF_MEMORY_INTERRUPTION_TYPE)
            HARDWARE.cpu._interruptVector.handle(outOfMemoryIRQ)