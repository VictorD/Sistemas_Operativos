
class Page():

    def __init__(self, aSize = 0):
        self._number        = 0
        self._size          = aSize

    @classmethod
    def newPage(cls, aSize, pageNumber=0):
        newPage         = Page(aSize)
        newPage._number = pageNumber
        return newPage

    def firstInstruction(self):
        return self._number * self._size

    def endInstruction(self):
        return self.firstInstruction() + self._size

    def __repr__(self):
        return f'{self._number}'