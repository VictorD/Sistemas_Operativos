from src.So.PTRow import PTRow


class PageTable():

    def __init__(self):
        self._table     = {}
        self._assocPid = None

    # Setea la pagina en una fila de la tabla cuya posicion corresponde al numero de la pagina
    def setPage(self, aKey):
        self._table[aKey._number] = PTRow.newRow(aKey)

    # asocia una frame a una pagina.
    def setFrameToPage(self, aKey, aFrame):
        pageRow = self._table[aKey]
        pageRow.registryFrame(aFrame)
        pageRow._hasFrame = True

    # retorna el frame correspondiente al numero de pagina. Puede retornar None si no hay frame asociado
    def frameOf(self, pageNumber):
        if pageNumber in self._table.keys():
            return self._table[pageNumber]._frame
        else:
            return None

    # Setea un listado de paginas a la tabla de paginas
    def assocPages(self, pages):
        for each in pages:
            self.setPage(each)

    # Asocia una lista de frames a la tabla. Asocia de la manera:
    # 1er frame de la lista al 1er page de la tabla
    # 2do frame de la lista al 2do page de la tabla
    # etc
    def assocFrames(self, frames):
        index = 0
        for each in frames:
            self.setFrameToPage(index, each)
            index += 1

    # Devuelve todos los frames que hay en la tabla.
    def getAllFrames(self):
        return list(filter(lambda x: not x is None, map(lambda x: x._frame, self._table.values())))

    # Limpia de frames la tabla.
    def clearFrames(self):
        for k in self._table.keys():
            rowToClean = self._table[k]
            rowToClean._frame   = None
            rowToClean._hasFrame = False

    #TODO: ¿Tenemos que liberar algun timestamp?
    def clearFrameOf(self, aPageNumber):
        rowToClean          = self._table[aPageNumber]
        rowToClean._frame   = None
        rowToClean._hasFrame= False

    @classmethod
    def createWith(cls, listOfPages, listOfFrames=None):
        newPageTable    = PageTable()
        newPageTable.assocPages(listOfPages)
        if listOfFrames is not None:
            newPageTable.assocFrames(listOfFrames)

        return newPageTable

    def getAllPages(self):
        return  list(map(lambda x: x._page, self._table.values()))

    def hasFrame(self, aPageNumber):
        return self._table[aPageNumber]._hasFrame


    def setDirty(self, page, aTick):
        ptRow = self._table[page]
        if not ptRow.isDirty():
            ptRow.setDirty(aTick)
        else:
            ptRow.setSecondChance()

        ptRow.newAcces(aTick)

    def setEntryTimeStamp(self, aPage, aTick):
        self._table[aPage].setTimeStampEntryToMemory(aTick)

    def hasAnyFrame(self):
        return any(x._hasFrame for x in self._table.values())

    def setPid(self, aPid):
        self._assocPid = aPid

    def pid(self):
        return self._assocPid

    def getRowWithLowestTimeLoad(self):
        ptrows          = list(filter(lambda x: x._hasFrame, self._table.values()))
        lowestTimeStamp = ptrows.pop(0)
        for eachPTrow in ptrows:
            if lowestTimeStamp._timeStampRead > eachPTrow._timeStampRead:
                lowestTimeStamp=eachPTrow
        return lowestTimeStamp

    def getRowWithLowestAccessTime(self):
        ptrows          = list(filter(lambda x: x._hasFrame, self._table.values()))
        lowestTimeStamp = ptrows.pop(0)

        for eachPTrow in ptrows:
            if lowestTimeStamp._timeStampEntryToMemory > eachPTrow._timeStampEntryToMemory:
                lowestTimeStamp=eachPTrow

        return lowestTimeStamp

    def setSecondChance(self, aPage):
        self._table[aPage].setSecondChance()


    def addAllptrowsWithFrames(self,listOfTouplesOfPIdAndPtrows):
        ptrowswithframe = list(filter(lambda x: x._hasFrame, self._table.values()))

        for each in ptrowswithframe:
            listOfTouplesOfPIdAndPtrows.append((self.pid(),each))

##############
    def clearSwapFrames(self):
        for k in self._table.keys():
            self.clearSwapOfPage(k)

    def isInSwap(self, aPageNumber):
        return self._table[aPageNumber]._hasSwapFrame

    def clearSwapOfPage(self, aPageNumber):
        rowToClean              = self._table[aPageNumber]
        # rowToClean.swapFrame    = None
        rowToClean._hasSwapFrame= False

    def setFrameInSwap(self, aPageNumber):
        self._table[aPageNumber]._hasSwapFrame= True