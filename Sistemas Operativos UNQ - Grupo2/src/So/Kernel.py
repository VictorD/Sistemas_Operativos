#!/usr/bin/env python
from src import log
from src.Hardware.Hardware import HARDWARE
from src.Hardware.Irq import KILL_INTERRUPTION_TYPE, IO_IN_INTERRUPTION_TYPE, IO_OUT_INTERRUPTION_TYPE, \
    NEW_INTERRUPTION_TYPE, IRQ, SHUTDOWN_INTERRUPTION_TYPE, TIMEOUT_INTERRUPTION_TYPE, SET_IN_INTERRUPTION_TYPE
from src.So.Dispatcher.Dispatcher import Dispatcher
from src.So.InterruptionHandlers.SetInterruptionHandler import SetInterruptionHandler
from src.So.InterruptionHandlers.ShutDownInterruptionHandler import ShutDownInterruptionHandler
from src.So.InterruptionHandlers.TimeOutInterruptionHandler import TimeOutInterruptionHandler
from src.So.IoDeviceController import IoDeviceController
from src.So.InterruptionHandlers.IoInInterruptionHandler import IoInInterruptionHandler
from src.So.InterruptionHandlers.IoOutInterruptionHandler import IoOutInterruptionHandler
from src.So.InterruptionHandlers.KillInterruptionHandler import KillInterruptionHandler
from src.So.InterruptionHandlers.NewInterruptionHandler import NewInterruptionHandler
from src.So.Loader.Loader import Loader
from src.So.Pcb.PCBPriority import PCBPriority
from src.So.Pcb.PcbTable import PcbTable
from src.So.Scheduler.SchedulerFIFO import SchedulerFIFO


# emulates the core of an Operative System
class Kernel():

    def __init__(self):
        ## setup interruption handlers
        killHandler = KillInterruptionHandler(self)
        HARDWARE.interruptVector.register(KILL_INTERRUPTION_TYPE, killHandler)

        ioInHandler = IoInInterruptionHandler(self)
        HARDWARE.interruptVector.register(IO_IN_INTERRUPTION_TYPE, ioInHandler)

        ioOutHandler = IoOutInterruptionHandler(self)
        HARDWARE.interruptVector.register(IO_OUT_INTERRUPTION_TYPE, ioOutHandler)

        newHandler = NewInterruptionHandler(self)
        HARDWARE.interruptVector.register(NEW_INTERRUPTION_TYPE, newHandler)

        shutDownHandler = ShutDownInterruptionHandler(self)
        HARDWARE.interruptVector.register(SHUTDOWN_INTERRUPTION_TYPE, shutDownHandler)

        timeOutHandler = TimeOutInterruptionHandler(self)
        HARDWARE.interruptVector.register(TIMEOUT_INTERRUPTION_TYPE, timeOutHandler)

        setVariableHandle = SetInterruptionHandler(self)
        HARDWARE.interruptVector.register(SET_IN_INTERRUPTION_TYPE, setVariableHandle)

        ## controls the Hardware's I/O Device
        self._ioDeviceController = IoDeviceController(HARDWARE.ioDevice)

        # Setup of pcbtable
        self._pcbtable = PcbTable()

        # Setup of Scheduler
        self._scheduler = SchedulerFIFO()

        # Setup of dispatcher
        self._dispatcher = Dispatcher()

        # Setup of loader
        self._loader = Loader()

        # Setup of Memory Manager
        self._memoryManager = None

    ## emulates a "system call" for programs execution
    def execute(self, aDirection, aPriority=PCBPriority.MEDIUM):
        newIRQ = IRQ(NEW_INTERRUPTION_TYPE, [aDirection, aPriority])
        HARDWARE.cpu._interruptVector.handle(newIRQ)
        log.logger.info("\nExecuting program in: " + aDirection)
        log.logger.info(HARDWARE)

    # Check if the Cpu is Idle
    def isCPUIdle(self):
        return HARDWARE.cpu.pc == -1

    # Cleans all devices and data types
    def cleanAll(self):
        HARDWARE.memory.cleanAll()
        log.logger.info("MEMORY CLEANED")

        self.pcbTable.cleanAll()
        log.logger.info("PCB TABLE CLEANED")

        self.scheduler.cleanAll()
        log.logger.info("SCHEDULER CLEANED")

        self.ioDeviceController.cleanAll()
        log.logger.info("IO DEVICE CONTROLLERS CLEANED")

        HARDWARE.timer.setOff()

    # ShutDowns the machine
    def shutDown(self):
        HARDWARE.switchOff()

    # ----- Getters & Setters ----- #
    @property
    def ioDeviceController(self):
        return self._ioDeviceController

    @property
    def pcbTable(self):
        return self._pcbtable

    @property
    def scheduler(self):
        return self._scheduler

    @property
    def dispatcher(self):
        return self._dispatcher

    @property
    def loader(self):
        return self._loader

    @scheduler.setter
    def scheduler(self, aScheduler):
        self._scheduler = aScheduler
    # ----- End of Getters & Setters ----- #

    def __repr__(self):
        return "Kernel "

    ##METODOS PARA UI##
    @property
    def memoryManager(self):
        return self._memoryManager