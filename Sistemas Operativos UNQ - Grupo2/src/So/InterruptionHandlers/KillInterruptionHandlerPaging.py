from src.So.InterruptionHandlers.KillInterruptionHandler import KillInterruptionHandler


class KillInterruptionHandlerPaging(KillInterruptionHandler):

    def execute(self, irq):
        pidOfRunningPcb = self.kernel.pcbTable.pcbRunning.pid
        self.kernel._memoryManager.releaseFrames(pidOfRunningPcb)
        super().execute(irq)