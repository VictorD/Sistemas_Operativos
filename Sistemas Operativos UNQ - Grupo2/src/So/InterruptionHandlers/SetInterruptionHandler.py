from src.Hardware.Hardware import HARDWARE
from src.Hardware.Irq import IRQ, PAGE_FAULT_INTERRUPTION_TYPE
from src.So.InterruptionHandlers.AbstractInterruptionHandler import AbstractInterruptionHandler
from src.So.InterruptionHandlers.StrategiesOfSetting.AlreadyOnMemorySettingStrategy import \
    AlreadyOnMemorySettingStrategy


class SetInterruptionHandler(AbstractInterruptionHandler):

    def __init__(self, kernel, strat=AlreadyOnMemorySettingStrategy()):
        super().__init__(kernel)
        self._strategyOfSetting = strat

    def execute(self, irq):
        self._strategyOfSetting.execute(self, irq)

    def pageOnDemandHandle(self,irq):
        pcbRunning = self.kernel.pcbTable.pcbRunning
        if not (self.kernel._memoryManager.hasFrame(pcbRunning.pid, irq.parameters)):
            page = self.kernel._memoryManager.pageOfInstruction(irq.parameters)
            pageFaultIRQ = IRQ(PAGE_FAULT_INTERRUPTION_TYPE,[page] )
            HARDWARE.interruptVector.handle(pageFaultIRQ)
