from src.So.InterruptionHandlers.TimeOutInterruptionHandler import TimeOutInterruptionHandler


class TimeOutMFQInterruptionHandler(TimeOutInterruptionHandler):

    def schedulerAddPCB(self, aPcb):
        self.kernel.scheduler.addAndDemote(aPcb)