
class LRUStrategy():

    def getVictim(self, aMemoryTable):
        """Retorna una Tupla (pcb.Pid, PTRow)"""
        return aMemoryTable.getPageWithLowestAccessTime()
