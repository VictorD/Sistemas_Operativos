from src.Hardware.Hardware import HARDWARE
from src.So.Switch.MultilevelQueue.AbstractSwitchMultilevelQueue import AbstractSwitchMultilevelQueue
from src.So.Switch.SwitchOut import SwitchOut


class SwitchOutMultilevelQueue(SwitchOut, AbstractSwitchMultilevelQueue):

    # ---DEFINITION OF SINGLETON--- #
    __instance = None

    def __new__(cls):
        if SwitchOutMultilevelQueue.__instance is None:
            SwitchOutMultilevelQueue.__instance = object.__new__(cls)
        return SwitchOutMultilevelQueue.__instance
    # ---END OF THE DEFINITION--- #

    def addInReadyQueue(self, kernel, aPCB=None):
        HARDWARE.timer.resetTimeOutCounterIfNeed()

    def loadToRun(self, kernel, aPCB=None):
        nextPcb = kernel.scheduler.getNext()
        self.resolveTimerFor(nextPcb) # Resetea o apaga el timer segun su nivel de prioridad
        super().loadToRun(kernel, nextPcb)

    def __repr__(self):
        return "Multilevel Queue SwitchOut"