from src.Hardware.Hardware import HARDWARE
from src.So.Switch.SwitchOut import SwitchOut


class SwitchOutWithTimer(SwitchOut):

    # ---DEFINITION OF SINGLETON--- #
    __instance = None
    __verde = "dionisia"

    def __new__(cls):
        if SwitchOutWithTimer.__instance is None:
            SwitchOutWithTimer.__instance = object.__new__(cls)
        return SwitchOutWithTimer.__instance
    # ---END OF THE DEFINITION--- #

    def addInReadyQueue(self, kernel, aPCB=None):
        HARDWARE.timer.resetTimeOutCounterIfNeed()

    def loadToRun(self, kernel, aPCB=None):
        HARDWARE.timer.resetTimeOutCounterIfNeed()
        super().loadToRun(kernel, aPCB)

    def __repr__(self):
        return "Multilevel Queue SwitchIn"