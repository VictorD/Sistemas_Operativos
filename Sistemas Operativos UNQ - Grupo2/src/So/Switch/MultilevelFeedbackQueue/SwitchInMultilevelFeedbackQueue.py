from src.So.Switch.SwitchIn import SwitchIn


class SwitchInMultilevelFeedbackQueue(SwitchIn):

    # ---DEFINITION OF SINGLETON--- #
    __instance = None

    def __new__(cls):
        if SwitchInMultilevelFeedbackQueue.__instance is None:
            SwitchInMultilevelFeedbackQueue.__instance = object.__new__(cls)
        return SwitchInMultilevelFeedbackQueue.__instance
    # ---END OF THE DEFINITION--- #

    def addInReadyQueue(self, kernel, aPCB):
        pcbToReadyQueue = aPCB

        if kernel.scheduler.currentlyRunningQueue == 3:
            pcbToReadyQueue = kernel.pcbTable.pcbRunning
            kernel.dispatcher.save(pcbToReadyQueue)
            self.loadToRun(kernel, aPCB)
            kernel.scheduler.setCurrentlyRunning1()


        super().addInReadyQueue(kernel, pcbToReadyQueue)


    def __repr__(self):
        return "Multilevel Queue SwitchIn"
