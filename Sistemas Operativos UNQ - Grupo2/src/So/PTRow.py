
class PTRow():

    def __init__(self, page):
        self._page      = page
        self._frame     = None
        self._hasFrame  = False
        self._dirty     = False
        self._timeStampEntryToMemory = None
        self._timeStampRead = None
        self._secondChance = False

        # self._swapFrame    = None
        self._hasSwapFrame = False

    @classmethod
    def newRow(cls, page):
        newRow  = PTRow(page)
        return newRow

    def registryFrame(self, aFrame):
        self._frame = aFrame

    def setDirty(self,aTick):
        self._dirty     = True
        self.setTimeStampEntryToMemory(aTick)

    def isDirty(self):
        return  self._dirty

    def newAcces(self,aTick):
        self._timeStampRead = aTick

    def setSecondChance(self):
        self._secondChance = True

    def setTimeStampEntryToMemory(self,aTick):
        self._timeStampEntryToMemory = aTick

    def hasFrameLoaded(self):
        return self._hasFrame

    def hasSecondChance(self):
        return  self._secondChance

    def takeSecondChance(self):
        self._secondChance = False