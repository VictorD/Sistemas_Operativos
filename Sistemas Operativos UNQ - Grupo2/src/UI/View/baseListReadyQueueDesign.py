# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'rsc/ListReadyQueueWindow.ui'
#
# Created by: PyQt5 UI code generator 5.11
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ListedReadyQueueView(object):
    def setupUi(self, ListedReadyQueueView):
        ListedReadyQueueView.setObjectName("ListedReadyQueueView")
        ListedReadyQueueView.resize(190, 210)
        ListedReadyQueueView.setMinimumSize(QtCore.QSize(190, 210))
        ListedReadyQueueView.setMaximumSize(QtCore.QSize(190, 210))
        ListedReadyQueueView.setBaseSize(QtCore.QSize(190, 210))
        self.PcbListWidget = QtWidgets.QListWidget(ListedReadyQueueView)
        self.PcbListWidget.setGeometry(QtCore.QRect(9, 10, 91, 192))
        self.PcbListWidget.setObjectName("PcbListWidget")
        self.SeePCBPushButton = QtWidgets.QPushButton(ListedReadyQueueView)
        self.SeePCBPushButton.setEnabled(False)
        self.SeePCBPushButton.setGeometry(QtCore.QRect(109, 10, 75, 23))
        self.SeePCBPushButton.setObjectName("SeePCBPushButton")
        self.ClosePushButton = QtWidgets.QPushButton(ListedReadyQueueView)
        self.ClosePushButton.setGeometry(QtCore.QRect(109, 180, 75, 23))
        self.ClosePushButton.setObjectName("ClosePushButton")

        self.retranslateUi(ListedReadyQueueView)
        self.ClosePushButton.clicked.connect(ListedReadyQueueView.close)
        QtCore.QMetaObject.connectSlotsByName(ListedReadyQueueView)

    def retranslateUi(self, ListedReadyQueueView):
        _translate = QtCore.QCoreApplication.translate
        ListedReadyQueueView.setWindowTitle(_translate("ListedReadyQueueView", "PCB Listed"))
        self.SeePCBPushButton.setText(_translate("ListedReadyQueueView", "See PCB"))
        self.ClosePushButton.setText(_translate("ListedReadyQueueView", "Close"))

