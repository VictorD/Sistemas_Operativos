from src.UI.View.baseStateMonitorDesign import Ui_StateMonitor


class StateMonitorView(Ui_StateMonitor):

    def __init__(self, dialog,controller):
        self.__controller = controller  # Es el Controlador de la Ventana
        Ui_StateMonitor.__init__(self)    # Es el Template creado en el QTDesigner
        self.setupUi(dialog)
        self.SeeRunningPushButton.clicked.connect(self.seeRunningPCB)
        self.SeePCBPushButton.clicked.connect(self.seePCBSelected)
        self.DoaTickPushButton.clicked.connect(self.__controller.tick)
        self.DoTicksPushButton.clicked.connect(self.__controller.doTicks)
        self.PCBListPushButton.clicked.connect(self.__controller.openPCBListWindow)
        self.ReadyQueueListWidget.itemSelectionChanged.connect(self.enableDisableSeePcbButton)
        self.GanttChartPushButton.clicked.connect(self.__controller.openGanttChart)
        self.IOQueuePushButton.clicked.connect(self.__controller.openIOListWindow)
        self.MMUTablePushButton.clicked.connect(self.__controller.openMMUPageTableWindow)
        self.ListPushButton.clicked.connect(self.__controller.openListPcbReadyQueueWindow)

    def seePCBSelected(self):
        self.__controller.openPCBInspector(self.ReadyQueueListWidget.currentItem().data(1))

    def seeRunningPCB(self):
        self.__controller.openPCBInspectorForRunningPCB()

    def enableDisableSeePcbButton(self):
        self.__controller.enableDisableSeePcbButton()