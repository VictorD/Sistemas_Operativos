from src.SoBuilder.MemoryType import MemoryType
from src.SoBuilder.SchedulerType import SchedulerType
from src.SoBuilder.VictimStrategyType import VictimStrategyType
from src.UI.View.baseMainWindowDesign import Ui_MainWindow


class MainWindowView(Ui_MainWindow):

    def __init__(self, mainWindow,controller):
        self.__controller   = controller  # Es el Controlador de la Ventana
        Ui_MainWindow.__init__(self)    # Es el Template creado en el QTDesigner
        self.__mainWindow   = mainWindow
        self.setupUi(mainWindow)

        # Hago el set de las cosas
        self.setSchedulerTypes()
        self.setMemoryTypes()
        self.setVictimTypes()

        # Seteo la accion al presionar el boton continue
        self.FrameSizeErrorLabel.setHidden(True)
        self.QuantumErrorLabel.setHidden(True)
        self.ContinuePushButton.clicked.connect(self.__controller.buildAndStart)
        self.MemorySizeEdit.textChanged.connect(self.__controller.validateMemorySize)
        self.MemoryComboBox.currentTextChanged.connect(self.__controller.controllerValidateMemorySelected)
        self.FrameSizeSpinBox.valueChanged.connect(self.__controller.validateFrameSize)
        self.SchedulerComboBox.currentTextChanged.connect(self.__controller.validateSchedulerType)
        self.QuantumSpinBox.valueChanged.connect(self.__controller.validateQuantum)
        self.UserRadioButton.clicked.connect(self.__controller.validateContinueButton)
        self.UserLineEdit.textChanged.connect(self.__controller.validateContinueButton)
        self.PasswordLineEdit.textChanged.connect(self.__controller.validateContinueButton)

    def setSchedulerTypes(self):
        self.SchedulerComboBox.addItem("First Come, First Serve"    , SchedulerType.FIFO)
        self.SchedulerComboBox.addItem("Priority No Expropiative"   , SchedulerType.PRIORITYNOEXPROPIATIVE)
        self.SchedulerComboBox.addItem("Priority Expropiative"      , SchedulerType.PRIORITYEXPROPIATIVE)
        self.SchedulerComboBox.addItem("Round Robin"                , SchedulerType.ROUNDROBIN)
        self.SchedulerComboBox.addItem("Multilevel Queue"           , SchedulerType.MULTILEVELQUEUE)
        self.SchedulerComboBox.addItem("Multilevel Feedback Queue"  , SchedulerType.MULTILEVELFEEDBACKQUEUE)

    def setMemoryTypes(self):
        self.MemoryComboBox.addItem("Simple"    , MemoryType.SIMPLE)
        self.MemoryComboBox.addItem("Paging"    , MemoryType.PAGING)
        self.MemoryComboBox.addItem("On Demand" , MemoryType.ONDEMAND)

    def setVictimTypes(self):
        self.VictimComboBox.addItem("First In, Fist Out"    , VictimStrategyType.FCFS)
        self.VictimComboBox.addItem("Least Recently Used"   , VictimStrategyType.LRU)
        self.VictimComboBox.addItem("Second Chance"         , VictimStrategyType.SECONDCHANCE)

    def hide(self):
        self.__mainWindow.hide()

# if __name__ == '__main__':
#     app = QtWidgets.QApplication(sys.argv)
#     dialog = QtWidgets.QDialog()
#     prog = SOReport(dialog,)
#     dialog.show()
#     sys.exit(app.exec_())