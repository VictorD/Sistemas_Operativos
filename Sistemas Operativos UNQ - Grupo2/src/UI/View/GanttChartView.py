from src.UI.View.baseGanttChartDesign import Ui_GanttChartWindow


class GanttChartView(Ui_GanttChartWindow):

    def __init__(self, dialog,controller):
        self.__controller = controller  # Es el Controlador de la Ventana
        Ui_GanttChartWindow.__init__(self)
        self.setupUi(dialog)
        self.CalculatePushButton.clicked.connect(self.__controller.calculateAverageTime)