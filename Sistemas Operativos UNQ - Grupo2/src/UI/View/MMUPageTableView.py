from src.UI.View.basePageTableMMUDesign import Ui_MMUTable


class MMUPageTableView(Ui_MMUTable):

    def __init__(self, dialog, controller):
        self.__controller = controller  # Es el Controlador de la Ventana
        Ui_MMUTable.__init__(self)  # Es el Template creado en el QTDesigner
        self.setupUi(dialog)