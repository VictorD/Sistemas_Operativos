# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'rsc/GanttChartWindow.ui'
#
# Created by: PyQt5 UI code generator 5.11
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_GanttChartWindow(object):
    def setupUi(self, GanttChartWindow):
        GanttChartWindow.setObjectName("GanttChartWindow")
        GanttChartWindow.resize(640, 360)
        GanttChartWindow.setMinimumSize(QtCore.QSize(640, 360))
        GanttChartWindow.setMaximumSize(QtCore.QSize(640, 360))
        GanttChartWindow.setBaseSize(QtCore.QSize(640, 360))
        self.ClosePushButton = QtWidgets.QPushButton(GanttChartWindow)
        self.ClosePushButton.setGeometry(QtCore.QRect(560, 330, 75, 23))
        self.ClosePushButton.setObjectName("ClosePushButton")
        self.GanttTableWidget = QtWidgets.QTableWidget(GanttChartWindow)
        self.GanttTableWidget.setGeometry(QtCore.QRect(10, 10, 621, 311))
        self.GanttTableWidget.setSelectionMode(QtWidgets.QAbstractItemView.NoSelection)
        self.GanttTableWidget.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.GanttTableWidget.setShowGrid(False)
        self.GanttTableWidget.setObjectName("GanttTableWidget")
        self.GanttTableWidget.setColumnCount(0)
        self.GanttTableWidget.setRowCount(0)
        self.GanttTableWidget.verticalHeader().setVisible(True)
        self.AverageTimeLabel = QtWidgets.QLabel(GanttChartWindow)
        self.AverageTimeLabel.setGeometry(QtCore.QRect(140, 331, 111, 20))
        self.AverageTimeLabel.setObjectName("AverageTimeLabel")
        self.CalculatePushButton = QtWidgets.QPushButton(GanttChartWindow)
        self.CalculatePushButton.setGeometry(QtCore.QRect(10, 330, 121, 23))
        self.CalculatePushButton.setObjectName("CalculatePushButton")
        self.AverageWaitingTimeValueLabel = QtWidgets.QLabel(GanttChartWindow)
        self.AverageWaitingTimeValueLabel.setGeometry(QtCore.QRect(254, 336, 47, 13))
        self.AverageWaitingTimeValueLabel.setObjectName("AverageWaitingTimeValueLabel")

        self.retranslateUi(GanttChartWindow)
        self.ClosePushButton.clicked['bool'].connect(GanttChartWindow.close)
        QtCore.QMetaObject.connectSlotsByName(GanttChartWindow)

    def retranslateUi(self, GanttChartWindow):
        _translate = QtCore.QCoreApplication.translate
        GanttChartWindow.setWindowTitle(_translate("GanttChartWindow", "Gantt Chart"))
        self.ClosePushButton.setText(_translate("GanttChartWindow", "Close"))
        self.AverageTimeLabel.setText(_translate("GanttChartWindow", "Average Waiting Time:"))
        self.CalculatePushButton.setText(_translate("GanttChartWindow", "Calculate Waiting Time"))
        self.AverageWaitingTimeValueLabel.setText(_translate("GanttChartWindow", "N/A"))

