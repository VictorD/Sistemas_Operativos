from PyQt5 import QtWidgets, QtGui
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QTableWidgetItem

from src.UI.View.GanttChartView import GanttChartView


class GanttChartController():

    def __init__(self, aGanttChart):
        self.ganttChart     = aGanttChart
        self.window = QtWidgets.QDialog()
        self.__view = GanttChartView(self.window, self)
        self.loadInfo()
        self.window.show()

    def calculateAverageTime(self):
        if self.ganttChart.hasSnapshots():
            averageWaitingTime  = self.ganttChart.calculateGantDiagram()
            self.__view.AverageWaitingTimeValueLabel.setText(repr(averageWaitingTime))

    def loadInfo(self):
        if self.ganttChart.hasSnapshots():
            self.loadFirstColumn()
            self.loadColumns()
        else:
            self.__view.GanttTableWidget.setDisabled(True)
            self.__view.CalculatePushButton.setDisabled(True)

    def loadFirstColumn(self):
        ganttChartTable = self.__view.GanttTableWidget
        indexer         = 0
        for eachElement in range(0, self.ganttChart.quantityOfPcb):
            item = QTableWidgetItem()
            item.setText(f'PCB: {eachElement}')
            bold = QtGui.QFont()
            bold.setBold(True)
            item.setFont(bold)
            ganttChartTable.insertRow(indexer)
            ganttChartTable.setVerticalHeaderItem(indexer, item)
            indexer += 1

    def loadColumns(self):
        ganttChartTable = self.__view.GanttTableWidget
        indexer = 0
        for eachElement in self.ganttChart.snapshotList:
            ganttChartTable.insertColumn(indexer)
            header = QTableWidgetItem()
            header.setText(f'Tick {eachElement.tick}')
            header.setTextAlignment(Qt.AlignHCenter)
            bold = QtGui.QFont()
            bold.setBold(True)
            header.setFont(bold)
            ganttChartTable.setHorizontalHeaderItem(indexer, header)
            self.loadColumn(indexer, eachElement)
            indexer += 1

    def loadColumn(self, columnIndex, snapshot):
        backGroundColor = {"Ready"      : Qt.red,
                           "Running"    : Qt.green,
                           "Waiting"    : Qt.gray,
                           "Terminated" : Qt.gray}

        ganttChartTable = self.__view.GanttTableWidget
        indexer         = 0
        for eachInfo in snapshot.pcbList:
            item = QTableWidgetItem()
            item.setText(repr(eachInfo.state))
            item.setBackground(Qt.GlobalColor(backGroundColor[eachInfo.state.value]))
            item.setTextAlignment(Qt.AlignHCenter|Qt.AlignVCenter)
            ganttChartTable.setItem(indexer, columnIndex, item)
            indexer += 1