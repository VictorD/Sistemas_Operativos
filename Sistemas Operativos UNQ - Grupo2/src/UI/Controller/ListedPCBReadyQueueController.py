from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QListWidgetItem

from src.So.Pcb.Pcb import PCB
from src.UI.Controller.ProcessInspectorController import ProcessInspectorController
from src.UI.View.ListedPCBReadyQueueView import ListedPCBReadyQueueView
from PyQt5 import QtWidgets

class ListedPCBReadyQueueController():

    def __init__(self, aReadyQueue, aMemoryManager):
        self.pcbList        = aReadyQueue
        self.memoryManager  = aMemoryManager
        self.window         = QtWidgets.QDialog()
        self.__view         = ListedPCBReadyQueueView(self.window, self)
        self.setList()
        self.window.show()

    def setList(self):
        for eachPCB in self.pcbList:
            item = QListWidgetItem()
            item.setText(repr(eachPCB))
            item.setData(Qt.ItemIsSelectable, eachPCB)
            self.__view.PcbListWidget.addItem(item)

    def openPCBInspector(self, aPCB):
        ProcessInspectorController(aPCB, self.memoryManager)

    def enableButton(self):
        if self.__view.PcbListWidget.currentItem().data(1).__class__ == PCB:
            self.__view.SeePCBPushButton.setEnabled(True)
