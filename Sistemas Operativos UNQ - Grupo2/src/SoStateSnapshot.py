import copy
from src.Hardware.Hardware import HARDWARE

class SoStateSnapshot():

    def __init__(self):
        self.__kernel             = None
        self.__hardware           = None
        self.__gantchart          = None
        self.__statisticsCollector= None

    @classmethod
    def takeSnapshot(cls, aKernel, aGantChart, aStatisticsCollector):
        newSOSnapshot   = SoStateSnapshot()
        newSOSnapshot.__kernel              = copy.deepcopy(aKernel)
        newSOSnapshot.__hardware            = copy.deepcopy(HARDWARE)
        newSOSnapshot.__gantchart           = copy.deepcopy(aGantChart)
        newSOSnapshot.__statisticsCollector = copy.deepcopy(aStatisticsCollector)
        return newSOSnapshot

    @property
    def kernel(self):
        return self.__kernel

    @property
    def hardware(self):
        return self.__hardware

    @property
    def gantchart(self):
        return self.__gantchart

    @property
    def statisticsCollector(self):
        return self.__statisticsCollector