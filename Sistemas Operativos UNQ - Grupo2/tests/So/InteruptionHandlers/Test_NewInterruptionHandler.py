from unittest import TestCase

from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.Hardware.Irq import NEW_INTERRUPTION_TYPE, IRQ
from src.So.Kernel import Kernel
from src.So.InterruptionHandlers.NewInterruptionHandler import NewInterruptionHandler
from src.So.Pcb.PCBPriority import PCBPriority
from src.So.Pcb.PcbState import PcbState
from src.So.Program import Program


class TestNewInterruptionHandler(TestCase):

    def setUp(self):
        HARDWARE.setup(50)
        self.kernel                = Kernel()
        self.program               = Program("It's a Virus!.exe", [ASM.CPU(1)])
        HARDWARE.harddisk.newDirectory("C:/virus")
        HARDWARE.harddisk.saveProgram(self.program, "C:/virus")
        self.program1              = Program("Really? another virus!.exe", [ASM.CPU(2)])
        HARDWARE.harddisk.newDirectory("C:/fake")
        HARDWARE.harddisk.saveProgram(self.program1, "C:/fake")
        self.newIHSut              = NewInterruptionHandler(self.kernel)
        self.irqNew                = IRQ(NEW_INTERRUPTION_TYPE, ["C:/virus/It's a Virus!.exe", PCBPriority.MEDIUM])
        self.irqNew1               = IRQ(NEW_INTERRUPTION_TYPE, ["C:/fake/Really? another virus!.exe", PCBPriority.MEDIUM])

    def test_should_a_program_be_executed_and_there_is_already_none_running_it_goes_to_running(self):
        # Setup

        # Exercise
        self.newIHSut.execute(self.irqNew)

        # Test
        self.assertFalse(self.kernel.pcbTable.hasNotRunningPCB())
        self.assertEqual(0, self.kernel.pcbTable.pcbRunning.pid)
        self.assertEqual(PcbState.RUNNING, self.kernel.pcbTable.pcbRunning.state)
        self.assertTrue(self.kernel.scheduler.readyQueue == [])

    def test_should_two_programs_be_executed_and_there_is_none_running_the_first_goes_to_running_and_the_other_to_the_Ready_Queue(self):
        # Setup

        # Exercise
        self.newIHSut.execute(self.irqNew)
        self.newIHSut.execute(self.irqNew1)

        # Test
        self.assertFalse(self.kernel.pcbTable.hasNotRunningPCB())
        self.assertEqual(0, self.kernel.pcbTable.pcbRunning.pid)
        self.assertEqual(PcbState.RUNNING, self.kernel.pcbTable.pcbRunning.state)
        self.assertFalse(self.kernel.scheduler.readyQueue == [])
        self.assertEqual(2, len(self.kernel.pcbTable._pcbTable))
        self.assertEqual(1, len(self.kernel.scheduler.readyQueue))
        self.assertEqual(1, self.kernel.scheduler.readyQueue[0].pid)
        self.assertEqual(PcbState.READY, self.kernel.scheduler.readyQueue[0].state)