from unittest import TestCase
from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.Hardware.Irq import IRQ, TIMEOUT_INTERRUPTION_TYPE
from src.So.InterruptionHandlers.TimeOutInterruptionHandler import TimeOutInterruptionHandler
from src.So.Kernel import Kernel
from src.So.Program import Program


class TestTimeOutInterruptionHandler(TestCase):

    def setUp(self):
        HARDWARE.setup(50)
        self.kernel     = Kernel()
        self.timeOutSut = TimeOutInterruptionHandler(self.kernel)
        self.timeOutIRQ = IRQ(TIMEOUT_INTERRUPTION_TYPE)
        self.program    = Program("It's a Virus!.dog", [ASM.CPU(1), ASM.IO()])
        HARDWARE.harddisk.newDirectory("C:/virus")
        HARDWARE.harddisk.saveProgram(self.program, "C:/virus")
        self.program    = Program("It's DogFace Time!.dog", [ASM.CPU(3), ASM.IO()])
        HARDWARE.harddisk.newDirectory("C:/Dogmmander")
        HARDWARE.harddisk.saveProgram(self.program, "C:/Dogmmander")

    def test_when_a_timeout_call_is_executed_with_an_empty_readyQueue_the_CPU_has_again_the_PCB_that_was_running(self):
        # Setup
        self.kernel.execute("C:/virus/It's a Virus!.dog")
        HARDWARE.cpu._pc = 1

        # Exercise
        self.timeOutSut.execute(self.timeOutIRQ)

        # Test
        self.assertEqual(False, self.kernel.isCPUIdle())
        self.assertIsNotNone(self.kernel.pcbTable.pcbRunning)
        self.assertEqual(1, HARDWARE.cpu.pc)
        self.assertEqual(1, self.kernel.pcbTable.pcbRunning.pc)

    def test_when_a_timeout_call_is_executed_with_an_readyQueue_with_one_pcb_the_CPU_has_that_PCB_and_that_what_was_running_go_to_the_readyqueue(self):
        # Setup
        self.kernel.execute("C:/virus/It's a Virus!.dog")
        self.kernel.execute("C:/Dogmmander/It's DogFace Time!.dog")
        HARDWARE.cpu._pc = 1

        # Exercise
        self.timeOutSut.execute(self.timeOutIRQ)

        # Test
        self.assertIsNotNone(self.kernel.pcbTable.pcbRunning)
        self.assertEqual(1, self.kernel.pcbTable.pcbRunning.pid)
        self.assertEqual(1, len(self.kernel.scheduler.readyQueue))
        self.assertEqual(0, self.kernel.scheduler.readyQueue[0].pid)
