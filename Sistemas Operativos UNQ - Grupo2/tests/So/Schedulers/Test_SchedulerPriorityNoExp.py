from unittest import TestCase

from src.So.Pcb.PCBPriority import PCBPriority
from src.So.Pcb.Pcb import PCB
from src.So.Scheduler.SchedulerPriorityNoExp import SchedulerPriorityNoExp


class TestSchedulerPriorityNoExp(TestCase):

    def setUp(self):
        self.schedulerPNE = SchedulerPriorityNoExp()
        self.pcb    = PCB(1, "C:/huesohueso/bitcoinminner.dog")
        self.pcb2   = PCB(2, "C:/huesohueso/troyano.dog")
        self.pcb3   = PCB(3, "C:/huesohueso/keylogger.dog")


    def test_should_a_process_be_handed_to_the_scheduler_and_there_is_a_process_with_more_priority_in_the_queue_it_goes_behind(self):
        # Setup
        self.pcb.priority  = PCBPriority.HIGHEST
        self.schedulerPNE.add(self.pcb)

        # Exercise
        self.schedulerPNE.add(self.pcb2)

        # Assert
        self.assertEqual(self.pcb, self.schedulerPNE.getNext())
        self.assertEqual(self.pcb2, self.schedulerPNE.getNext())

    def test_should_a_process_be_handed_to_the_scheduler_and_there_is_a_process_with_less_priority_in_the_queue_it_goes_first(self):
        # Setup
        self.pcb.priority  = PCBPriority.LOWEST
        self.schedulerPNE.add(self.pcb)

        # Exercise
        self.schedulerPNE.add(self.pcb2)

        # Assert
        self.assertEqual(self.pcb2, self.schedulerPNE.getNext())
        self.assertEqual(self.pcb, self.schedulerPNE.getNext())

    def test_should_a_process_be_handed_to_the_scheduler_and_there_is_a_process_with_equal_priority_but_less_instructions_in_the_queue_it_goes_behind(self):
        # Setup
        self.pcb.priority   = PCBPriority.HIGH
        self.pcb.size       = 2
        self.schedulerPNE.add(self.pcb)

        # Exercise
        self.pcb2.priority  = PCBPriority.HIGH
        self.pcb2.size = 5
        self.schedulerPNE.add(self.pcb2)

        # Assert
        self.assertEqual(self.pcb, self.schedulerPNE.getNext())
        self.assertEqual(self.pcb2, self.schedulerPNE.getNext())

    def test_should_a_process_be_handed_to_the_scheduler_and_there_is_a_process_with_equal_priority_and_instructions_in_the_queue_it_goes_behind(self):
        # Setup
        self.pcb.priority   = PCBPriority.HIGH
        self.pcb.size       = 2
        self.schedulerPNE.add(self.pcb)

        # Exercise
        self.pcb2._priority = PCBPriority.HIGH
        self.pcb2.size = 2
        self.schedulerPNE.add(self.pcb2)

        # Assert
        self.assertEqual(self.pcb, self.schedulerPNE.getNext())
        self.assertEqual(self.pcb2, self.schedulerPNE.getNext())

    def test_should_a_process_be_handed_to_the_scheduler_and_in_the_queue_are_one_process_with_more_priority_and_other_with_a_lowest_priority_it_goes_between_they(self):
        # Setup
        self.pcb.priority   = PCBPriority.HIGHEST
        self.schedulerPNE.add(self.pcb)
        self.pcb2.priority  = PCBPriority.LOWEST
        self.schedulerPNE.add(self.pcb2)

        # Exercise
        self.schedulerPNE.add(self.pcb3)

        # Assert
        self.assertEqual(self.pcb, self.schedulerPNE.getNext())
        self.assertEqual(self.pcb3, self.schedulerPNE.getNext())
        self.assertEqual(self.pcb2, self.schedulerPNE.getNext())
