from unittest import TestCase

from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.Hardware.MMU.MMUOnDemand import MMUOnDemand
from src.So.Dispatcher.Dispatcher import Dispatcher
from src.So.Dispatcher.DispatcherOnDemand import DispatcherOnDemand
from src.So.MemoryManager.MemoryManagerOnDemand import MemoryManagerOnDemand
from src.So.Page import Page
from src.So.PageTable import PageTable
from src.So.Pcb.Pcb import PCB
from src.So.Program import Program


class Test_DispatcherOnDemand(TestCase):

    def setUp(self):
        HARDWARE.setup(60)
        self.memoryManager = MemoryManagerOnDemand(4, 40)

        self.programTest1 = Program("Test.dog", [ASM.CPU(12)])
        HARDWARE.harddisk.newDirectory("C:/perrugrama")
        HARDWARE.harddisk.saveProgram(self.programTest1, "C:/perrugrama")
        self.pcbProgram1 = PCB(0, "C:/perrugrama/Test.dog")

        self.pft = PageTable()
        self.page1 = Page()
        self.page1._number = 0
        self.page2 = Page()
        self.page2._number = 1
        self.page3 = Page()
        self.page3._number = 2
        self.page4 = Page()
        self.page4._number = 3
        self.pft.assocPages([self.page1,self.page2,self.page3,self.page4])

        self.dispatcherSUT          = DispatcherOnDemand(self.memoryManager)

        self.mmuOnDemand = MMUOnDemand(4, HARDWARE.memory)
        HARDWARE._mmu = self.mmuOnDemand

    def test_WhenAProgramRunningGoesOutOfCPUTheDispatcherHasTOUpadteTheNewValuesOfThePageTableOfThePCBWithTheOnesSavedInTheMMU(self):
        #Exersice

        # modificar algo en la tabla, y despues el load

        self.memoryManager.savePCBAndPageTable(self.pcbProgram1.pid, self.pft )
        self.dispatcherSUT.load(self.pcbProgram1)

        tableInMemoryManager = self.memoryManager.getPageTableOf(self.pcbProgram1.pid)
        ## Se testea que la tabla en el memory manager este como esperamos antes del cambio
        self.assertIsNone(tableInMemoryManager.frameOf(0))


        self.mmuOnDemand._currentProgramPaginationTable.setFrameToPage(0,10)

        self.dispatcherSUT.save(self.pcbProgram1)

        tableInMemoryManager = self.memoryManager.getPageTableOf(self.pcbProgram1.pid)
        ## Se testea que la tabla en el memory manager Tenga los cambios que se hicieron en el mmu despues de ser salvada
        #Test
        self.assertIsNotNone(tableInMemoryManager.frameOf(0))



    # def test_when_the_loader_is_asked_to_load_a_program_and_the_memory_Manager_doesnt_has_free_frames_raise_an_exeption(self):
    #  la idea es que aca cuando no te quedan frames para asginar, la excepcion se handlee avisando que libere un frame
    #  (en el test del handler hay que fijarse que haga bien la seleccion de victima) y ponga la pagina asignada en swap

    #  Setup
    #     outOfMemoryHandler = OutOfMemoryInterruptionHandler(Kernel())
    #     HARDWARE.interruptVector.register(OUT_OF_MEMORY_INTERRUPTION_TYPE, outOfMemoryHandler)
    #     self.memoryManager._freeFrames = []
    #     self.memoryManager._freeMemory = 0
    #
    #     # Exercise
    #     self.assertRaises(OutOfMemoryException, self.loaderSUT.loadProgram, self.pcbProgram2)