from unittest import TestCase

from src.Hardware.Asm import ASM
from src.Hardware.Hardware import HARDWARE
from src.So.Block import Block
from src.So.MemoryManager.MemoryManagerSegmentation import MemoryManagerOnSegmentation
from src.So.Segment import Segment
from src.So.SegmentTable import SegmentTable


class TestMemoryManagerOnSegmentation(TestCase):

    def setUp(self):
        HARDWARE.setup(50)
        self.mermoryManagerSut  = MemoryManagerOnSegmentation(120)

        self.segment1 = Segment([ASM.CPU(2), ASM.IO(), ASM.CPU(3), ASM.IO(), ASM.CPU(2), ASM.EXIT(1)])
        self.segment1._sid = 1
        self.segment2 = Segment([ASM.CPU(2), ASM.IO(), ASM.CPU(3), ASM.IO(), ASM.CPU(2), ASM.EXIT(1)])
        self.segment2._sid = 2
        self.segment3 = Segment([ASM.CPU(2), ASM.IO(), ASM.CPU(3), ASM.IO(), ASM.CPU(2), ASM.EXIT(1)])
        self.segment3._sid = 3

        self.block1 = Block(0,49)
        self.block2 = Block(75,99)

        self.mermoryManagerSut._freeBlocks                = [self.block1,self.block2]

        self.segmentTablePid0 = SegmentTable()
        self.segmentTablePid0.setSegment(self.segment1)
        self.segmentTablePid0.setSegment(self.segment2)
        self.segmentTablePid0._assocPid = 0

    def test_a_Memory_manager_with_a_Memory_of_size_50_and_and_2_Free_block_if_Size_of_50_and_size_25_has_75_cells_free(self):
        # Test
        self.assertEqual(75, self.mermoryManagerSut.freeMemory())
        self.assertEqual(2, len(self.mermoryManagerSut._freeBlocks))
        self.assertTrue(self.mermoryManagerSut._segmentTables._table == {})


    def test_when_the_memory_manager_is_asked_for_a_block_for_some_instructions_it_returns_the_first_fit_From_its_empty_blocks (self):
        # Setup
        # Exercise
        block       = self.mermoryManagerSut.getFreeBlock(10)
        # Test
        self.assertEqual(0, block.init)
        self.assertEqual(9, block.end)
        self.assertEqual(2, len(self.mermoryManagerSut._freeBlocks))
        self.assertEqual(65, self.mermoryManagerSut.freeMemory())


    def test_when_the_memory_manager_is_asked_for_the_segment_table_of_a_pid_it_returns_The_corresponding_segment_table(self):
        # Setup
        block = self.mermoryManagerSut.getFreeBlock(len(self.segment1.instructions))
        self.segmentTablePid0.setBlockToSegment(1,block)
        #Exercise
        self.mermoryManagerSut.savePidSegmentTable(0, self.segmentTablePid0)
        # Test
        self.assertEqual(self.segmentTablePid0, self.mermoryManagerSut.getSegmentTableOf(0))


    #def test_when_the_memory_manager_is_given_a_pid_and_Asked_to_release_a_Block_The_Block_is_freed_again(self):
        # Setup



    #def test_when_the_memory_manager_is_given_a_pid_and_Asked_to_release_the_page_table_the_frames_are_Free_Again(self):
        # Setup


    #def test_when_the_memory_manager_is_asked_for_a_frame_and_dont_have_more_select_a_victim_and_goes_to_swap(self):
        # Setup
