from unittest import TestCase

from src.So.MemoryTable import MemoryTable
from src.So.Page import Page
from src.So.PageTable import PageTable
from src.So.victimStrategy.FIFOStrategy import FIFOStrategy


class TestFIFOStrategy(TestCase):

    def setUp(self):
        self.FifoStrategySut= FIFOStrategy()
        self.memoryTable    = MemoryTable()
        self.pageTablePid0  = self.setupPageTableOfPid0()
        self.pageTablePid1  = self.setupPageTableOfPid1()
        self.pageTablePid2  = self.setupPageTableOfPid2()
        self.memoryTable.loadPCBAndPageTable(0, self.pageTablePid0)
        self.memoryTable.loadPCBAndPageTable(1, self.pageTablePid1)
        self.memoryTable.loadPCBAndPageTable(2, self.pageTablePid2)

    def test_getVictim(self):
        # Setup
        # Exercise
        victimFifo = self.FifoStrategySut.getVictim(self.memoryTable)
        # Test
        self.assertEqual(1, victimFifo[0])
        self.assertEqual(2, victimFifo[1]._frame)

    ##[-----Setup Methods-----]##
    def setupPageTableOfPid2(self):
        pageTablePid2 = PageTable()
        page1Pid2 = Page()
        page1Pid2._number = 0
        pageTablePid2.assocPages([page1Pid2])
        return pageTablePid2

    def setupPageTableOfPid1(self):
        pageTablePid1       = PageTable()
        page1Pid1           = Page()
        page1Pid1._number   = 0
        page2Pid1           = Page()
        page2Pid1._number   = 1
        pageTablePid1.assocPages([page1Pid1, page2Pid1])
        pageTablePid1.assocFrames([2])
        pageTablePid1.setDirty(0, 1)
        return pageTablePid1

    def setupPageTableOfPid0(self):
        pageTablePid0       = PageTable()
        page1Pid0          = Page()
        page1Pid0._number  = 0
        page2Pid0          = Page()
        page2Pid0._number  = 1
        page3Pid0          = Page()
        page3Pid0._number  = 2
        page4Pid0          = Page()
        page4Pid0._number  = 3
        pageTablePid0.assocPages([page1Pid0, page2Pid0, page3Pid0, page4Pid0])
        pageTablePid0.assocFrames([4, 0])
        pageTablePid0.setDirty(0, 5)
        pageTablePid0.setDirty(1, 9)
        return pageTablePid0